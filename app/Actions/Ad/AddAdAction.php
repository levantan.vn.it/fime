<?php

namespace App\Actions\Ad;
use App\Contracts\AdsRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class AddAdAction extends Action {
    protected $ad_repository;

    public function __construct(AdsRepositoryInterface $ad_repository) {
        $this->ad_repository = $ad_repository;
    }

    public function run($data) {
        try {
            $ad = $this->ad_repository->create($data);
            return $ad;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
