<?php

namespace App\Actions\Ad;
use App\Contracts\AdsRepositoryInterface;
use App\Actions\Action;

class GetAdByIDAction extends Action {
    protected $ad_repository;

    public function __construct(AdsRepositoryInterface $ad_repository) {
        $this->ad_repository = $ad_repository;
    }

    public function run($id) {
        $banner = $this->ad_repository->find($id);
       return $banner;
    }
}
