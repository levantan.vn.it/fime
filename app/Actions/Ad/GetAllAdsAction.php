<?php

namespace App\Actions\Ad;
use App\Contracts\AdsRepositoryInterface;
use App\Actions\Action;

class GetAllAdsAction extends Action {
    protected $ads_repository;

    public function __construct(AdsRepositoryInterface $ads_repository) {
        $this->ads_repository = $ads_repository;
    }

    public function run() {
        $ads = $this->ads_repository->all();
        return $ads;
    }
}
