<?php

namespace App\Actions\Ad;
use App\Contracts\AdsRepositoryInterface;
use App\Actions\Action;
use App\Criterias\Ad\GetAvailableAdsCriteria;

class GetAvailableAdsAction extends Action {
    protected $ads_repository;

    public function __construct(AdsRepositoryInterface $ads_repository) {
        $this->ads_repository = $ads_repository;
    }

    public function run() {
        $ads = $this->ads_repository->getByCriteria(new GetAvailableAdsCriteria());
        return $ads;
    }
}
