<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/17/2019
 * Time: 10:58 AM
 */

namespace App\Actions\Auth;


use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;
use Carbon\Carbon;

class Login extends Action
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository) {
        $this->userRepository = $userRepository;
    }
    /**
     * @param $request
     * @return array
     */
    public function run($request)
    {
        $credentials = ['email' => $request['email'], 'password' => $request['password']];

        if (!$token = auth()->attempt($credentials)) {
            return ['error' => 'Unauthorized', 'errorCode' => 1];
        }

        if (auth()->user()->deleted_at == null) {
            $this->userRepository->update([
                'last_login_dt' => Carbon::now()->toDateString()
            ], auth()->user()->user_no);
            return [
                'access_token' => $token,
                'user' => auth()->user(),
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
        } else {
            auth()->logout();
            return ['error' => 'Inactive account', 'errorCode' => 2];
        }
    }
}