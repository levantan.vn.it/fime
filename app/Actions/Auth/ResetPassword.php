<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/18/2019
 * Time: 10:24 AM
 */

namespace App\Actions\Auth;


use App\Actions\Action;
use App\Contracts\PasswordResetRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class ResetPassword extends Action
{
    private $passwordResetRepository;
    private $userRepository;

    public function __construct(PasswordResetRepositoryInterface $passwordResetRepository,
                                UserRepositoryInterface $userRepository) {
        $this->passwordResetRepository = $passwordResetRepository;
        $this->userRepository = $userRepository;
    }


    /**
     * @param $request
     * @return array
     */
    public function run($request){
        try {
            $token = $request['token'];
            $is_valid_token = $this->passwordResetRepository->findWhere(['token' => $token]);
            if (count($is_valid_token) > 0) {
                $expired_at = $is_valid_token[0]->expired_at;
                $now = Carbon::now();
                // Compare now with expired_at to determine that token is valid or invalid
                if ($now->gt($expired_at)) {
                    return [
                        'error' => 'Token was expired'
                    ];
                } else {
                    // Hash new password to store
                    $newPassword = Hash::make($request['password']);
                    $email = $is_valid_token[0]->email;
                    // Find user by email to update password
                    $user = $this->userRepository->findWhere(['email' => $email]);
                    if (count($user) > 0) {
                        $id = $user[0]->user_no;
                        $this->userRepository->update(['password' => $newPassword], $id);
                        // Token is used, delete it.
                        $this->passwordResetRepository->delete($is_valid_token[0]->id);
                        return [
                            'done' => 1,
                        ];
                    } else {
                        return [
                            'error' => 'Email and token are not compatible.'
                        ];
                    }
                }
            } else {
                return [
                    'error' => 'Token is invalid'
                ];
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}