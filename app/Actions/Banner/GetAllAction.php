<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Banner;
use App\Contracts\BannerRepositoryInterface;
use App\Actions\Action;

class GetAllAction extends Action {
    protected $banner_repository;

    public function __construct(BannerRepositoryInterface $banner_repository) {
        $this->banner_repository = $banner_repository;
    }

    public function run() {
        $banners = $this->banner_repository->all();
       return $banners;
    }
}
