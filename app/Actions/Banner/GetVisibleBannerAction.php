<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Banner;
use App\Actions\Action;
use App\Contracts\BannerRepositoryInterface;
use Carbon\Carbon;

class GetVisibleBannerAction extends Action {
    protected $banner_repository;

    public function __construct(BannerRepositoryInterface $banner_repository) {
        $this->banner_repository = $banner_repository;
    }

    public function run() {
        $banners = $this->banner_repository->scopeQuery(function ($query) {
            $query = $query->where('is_disabled', '=', 0);
            $query = $query->where('period_from', '<=', Carbon::now('UTC'));
            $query = $query->where('period_to', '>=', Carbon::now('UTC'));
            $query = $query->orderBy('id');
            return $query;
        })->all();
       return $banners;
    }
}
