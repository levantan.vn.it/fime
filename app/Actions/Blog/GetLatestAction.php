<?php

namespace App\Actions\Blog;
use App\Actions\Action;
use App\Contracts\BlogRepositoryInterface;
use App\Criterias\Blog\GetLatestBlogCriteria;

class GetLatestAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($request) {
        try {
            $blogs = $this->repository->pushCriteria(new GetLatestBlogCriteria())->paginate(10);
            return $blogs;
        } catch (\Exception $e) {
            \Log::error($e);
            return [];
        }
    }
}
