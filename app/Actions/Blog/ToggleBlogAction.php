<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Blog;
use App\Contracts\BlogRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class ToggleBlogAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($ids, $toggle) {
        try {
            $blogs = $this->repository->findWhereIn('id', $ids);

            foreach ($blogs as $blog) {
                $this->repository->update([
                    'is_disabled' => $toggle
                ], $blog->id);
            }
            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
