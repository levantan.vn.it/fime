<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Blog;
use App\Contracts\BlogRepositoryInterface;
use App\Actions\Action;
use Mockery\Exception;

class UpdateBlogAction extends Action {
    protected $repository;

    public function __construct(BlogRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($data) {
        try {
            $blog = $this->repository->update([
                'title' => $data['title'],
                'content' => $data['content'],
                'short_description' => $data['short_description'],
                'is_disabled' => $data['is_disabled'],
                'show_on_main' => $data['show_on_main'],
                'resource_type' => $data['resource_type'],
                'url' => $data['url'],
            ], $data['id']);
            return $blog;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
