<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Brand;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\BrandRepositoryInterface;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class AddBrandAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($data) {
        try {
            $last_item = $this->repository->orderBy('sort_ordr', 'desc')->findWhere([
                'code_group' => Constant::$BRAND_GROUP_CODE
            ])->first();

            $code = $last_item->sort_ordr + 1;

            $code = str_pad($code, 3, '0', STR_PAD_LEFT);

            $code = Constant::$BRAND_GROUP_CODE . $code;

            $brand = $this->repository->create([
                'code' => $code,
                'code_nm' => $data['code_nm'],
                'code_group' => Constant::$BRAND_GROUP_CODE,
                'sort_ordr' => $last_item->sort_ordr + 1,
                'use_at' => 'Y'
            ]);

            return $brand;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
