<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Brand;
use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Criterias\Brand\GetAllBrandCriteria;

class GetAllAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run() {
        $brands = $this->repository->getByCriteria(new GetAllBrandCriteria());
        return $brands;
    }
}
