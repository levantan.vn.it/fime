<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Brand;
use App\Contracts\BrandRepositoryInterface;
use App\Actions\Action;

class GetByAction extends Action {
    protected $repository;

    public function __construct(BrandRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id) {
        $brand = $this->repository->find($id);
       return $brand;
    }
}
