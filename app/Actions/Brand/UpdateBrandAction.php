<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Brand;
use App\Actions\Action;
use App\Contracts\BrandRepositoryInterface;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class UpdateBrandAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($data) {
        try {
            $brand = $this->repository->update([
                'code_nm' => $data['code_nm']
            ], $data['code']);
            return $brand;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
