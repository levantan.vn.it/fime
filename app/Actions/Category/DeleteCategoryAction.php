<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Category;

use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use Mockery\Exception;

class DeleteCategoryAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run($id) {
        try {
            $result = $this->repository->delete($id);
            return $result;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
