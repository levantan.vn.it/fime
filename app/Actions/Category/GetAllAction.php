<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\Category;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\CodeRepositoryInterface;

class GetAllAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run() {
        $categories = $this->repository->findWhere([
            'code_group' => Constant::$CATEGORY_GROUP_CODE,
            'use_at' => 'Y'
        ]);
       return $categories;
    }
}
