<?php

namespace App\Actions\Comment;
use App\Actions\Notification\CreateNotificationAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class CreateCommentAction extends CreateNotificationAction {
    protected $comment_repository;
    protected $review_comment_repository;

    public function __construct(CommentRepositoryInterface $comment_repository,
                                ReviewCommentRepositoryInterface $review_comment_repository,
                                NotificationRepositoryInterface $notification_repository,
                                ReviewRepositoryInterface $review_repository,
                                TryRepositoryInterface $try_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                CodeRepositoryInterface $codeRepository) {
        $this->comment_repository = $comment_repository;
        $this->review_comment_repository = $review_comment_repository;
        parent::__construct($notification_repository, $review_repository, $try_repository, $user_point_repository, $codeRepository);
    }


    public function run($data) {
        try {
            $type = $data['object_type'];
            $current_user_id = auth()->id();
            $user = Auth::user();
            $expsr_at = $user->allow_comment ? 'Y' : 'N';
            if($type == 'try') {
                $query = [
                    'anwr_writng_dt' => Carbon::now()->toDateTimeString(),
                    'anwr_sn' => 1,
                    'cntnts_no' => $data['object_id'],
                    'anwr_cn' => $data['content'],
                    'delete_at' => 'N',
                    'user_no' => $current_user_id,
                    'parent_id' => isset($data['parent_id']) ? $data['parent_id']: null,
                    'expsr_at' => $expsr_at
                ];

                $comment = $this->comment_repository->create($query);
                $this->comment('try', $data['object_id'], $current_user_id, false);
            } else {
                $query = [
                    'anwr_writng_dt' => Carbon::now()->toDateTimeString(),
                    'anwr_sn' => 1,
                    'review_no' => $data['object_id'],
                    'anwr_cn' => $data['content'],
                    'delete_at' => 'N',
                    'user_no' => $current_user_id,
                    'parent_id' => isset($data['parent_id']) ? $data['parent_id']: null,
                    'expsr_at' => $expsr_at
                ];

                $comment = $this->review_comment_repository->create($query);
                $this->comment('review', $data['object_id'], $current_user_id, false);

                // Create notification
                if ($user->allow_comment) {
                    $review = $this->review_repository->find($data['object_id']);
                    $author_id = $review->user_no;
                    $type = 'comment';
                    if (isset($data['is_reply'])) {
                        $type = $data['is_reply'] ? 'reply' : 'comment';
                    }
                    $this->createCommentNotification($data['object_id'], $data['object_type'], $author_id, $type);
                }
            }

            // Add point
//            $this->commentReview($comment->user_no, $comment->review_no, false);

            // Reformat return data
            $data = [];
            $data['id'] = $comment->id;
            $data['content'] = $comment->anwr_cn;
            $data['created_at'] = $comment->anwr_writng_dt;
            $data['parent_id'] = $comment->parent_id;
            $data['count'] = 0;
            $data['user_no'] = $comment->user_no;
            $data['author_slug'] = Auth::user()->slug;
            return $data;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
