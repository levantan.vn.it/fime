<?php

namespace App\Actions\Comment;
use App\Actions\Action;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Criterias\Comment\GetCommentCountByReviewIdCriteria;
use App\Criterias\Comment\GetCommentCountByTryIdCriteria;
use App\Criterias\Comment\GetCommentsByReviewIdCriteria;
use App\Criterias\Comment\GetCommentsByTryIdCriteria;

class GetCommentByObjectIdAction extends Action {
    protected $comment_repository;
    protected $review_comment_repository;

    public function __construct(CommentRepositoryInterface $comment_repository,
                                ReviewCommentRepositoryInterface $review_comment_repository) {
        $this->comment_repository = $comment_repository;
        $this->review_comment_repository = $review_comment_repository;
    }

    public function run($data) {
        $id = $data['object_id'];
        $type = $data['object_type'];
        $page = $data['page'];
        $last_id = INF;
        if (isset($data['last_id'])) {
            $last_id = $data['last_id'];
        }
        $parent_id = null;
        $offset = 5;
        if (isset($data['parent_id'])) {
            $parent_id = $data['parent_id'];
        }
        try {
            if (!$parent_id) {
                if($type == 'try') {
                    $count_comments = $this->comment_repository->getByCriteria(new GetCommentCountByTryIdCriteria($id, $parent_id));
                    $comments = $this->comment_repository->getByCriteria(new GetCommentsByTryIdCriteria($id, $page, $last_id, $parent_id));

                    foreach ($comments as $comment) {
                        $comment->replies = $this->comment_repository->getByCriteria(new GetCommentsByTryIdCriteria($id, 0, $last_id, $comment->id));
                        $count_replies = $this->comment_repository->getByCriteria(new GetCommentCountByTryIdCriteria($id, $comment->id));

                        $remaining_replies = ($count_replies[0]->count - (($page + 1) * $offset)) > 0 ? $count_replies[0]->count - ($page + 1) * $offset : 0;
                        $comment->count = $remaining_replies;
                    }
                } else {
                    $count_comments = $this->review_comment_repository->getByCriteria(new GetCommentCountByReviewIdCriteria($id, $parent_id));
                    $comments = $this->review_comment_repository->getByCriteria(new GetCommentsByReviewIdCriteria($id, $page, $last_id, $parent_id));

                    foreach ($comments as $comment) {
                        $comment->replies = $this->review_comment_repository->getByCriteria(new GetCommentsByReviewIdCriteria($id, 0, $last_id, $comment->id));
                        $count_replies = $this->review_comment_repository->getByCriteria(new GetCommentCountByReviewIdCriteria($id, $comment->id));

                        $remaining_replies = ($count_replies[0]->count - (($page + 1) * $offset)) > 0 ? $count_replies[0]->count - ($page + 1) * $offset : 0;
                        $comment->count = $remaining_replies;
                    }
                }

                $remaining_comments = ($count_comments[0]->count - ($page + 1) * $offset) > 0 ? $count_comments[0]->count - ($page + 1) * $offset : 0;
                return [
                    'count' => $remaining_comments,
                    'comments' => $comments
                ];

            } else {
                if($type == 'try') {
                    $count_replies = $this->comment_repository->getByCriteria(new GetCommentCountByTryIdCriteria($id, $parent_id));
                    $replies = $this->comment_repository->getByCriteria(new GetCommentsByTryIdCriteria($id, $page, $last_id, $parent_id));
                    $remaining_replies = ($count_replies[0]->count - ($page + 1) * $offset) > 0 ? $count_replies[0]->count - ($page + 1) * $offset : 0;
                } else {
                    $count_replies = $this->review_comment_repository->getByCriteria(new GetCommentCountByReviewIdCriteria($id, $parent_id));
                    $replies = $this->review_comment_repository->getByCriteria(new GetCommentsByReviewIdCriteria($id, $page, $last_id, $parent_id));
                    $remaining_replies = ($count_replies[0]->count - ($page + 1) * $offset) > 0 ? $count_replies[0]->count - ($page + 1) * $offset : 0;
                }

                return [
                    'count' => $remaining_replies,
                    'comments' => $replies
                ];
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
