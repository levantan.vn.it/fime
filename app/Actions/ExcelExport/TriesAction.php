<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/1/2019
 * Time: 2:03 PM
 */

namespace App\Actions\ExcelExport;


use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\UserTry\GetNumberOfTriesByTryIdsCriteria;
use Carbon\Carbon;

class TriesAction extends Action
{
    protected $try_repository;
    protected $code_repository;
    protected $user_try_repository;

    /**
     * TriesAction constructor.
     * @param TryRepositoryInterface $try_repository
     * @param CodeRepositoryInterface $code_repository
     * @param UserTryRepositoryInterface $user_try_repository
     */
    public function __construct(TryRepositoryInterface $try_repository, CodeRepositoryInterface $code_repository, UserTryRepositoryInterface $user_try_repository)
    {
        $this->try_repository = $try_repository;
        $this->code_repository = $code_repository;
        $this->user_try_repository = $user_try_repository;
    }

    public function run($params)
    {
        $tries = $this->try_repository->scopeQuery(function ($query) use ($params) {
            $query = $query->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no');
            $query = $query->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_GOODS.goods_cl_code');

            $query = $query->where('TOM_CNTNTS_WDTB.delete_at', 'N');

            if (isset($params['is_disabled']) && $params['is_disabled'] != "null") {
                if ($params['is_disabled'] == 'Y') {
                    $query = $query->where('event_bgnde', '<', Carbon::now());
                    $query = $query->where('event_endde', '>', Carbon::now());
                } else {
                    $query = $query->where('event_endde', '<', Carbon::now());
                }
            }

            if (isset($params['brand_id']) && $params['brand_id'] != "null") {
                $query = $query->where('brnd_code', $params['brand_id']);
            }

            if (isset($params['category_id']) && $params['category_id'] != "null") {
                $query = $query->where('goods_cl_code', $params['category_id']);
            }

            if (isset($params['name']) && $params['name'] != "null") {
                $query = $query->where('sj', 'like', '%' . $params['name'] . '%');
            }

            if (isset($params['type']) && $params['type'] != "null") {
                $query = $query->where('event_knd_code', '=', $params['type']);
            }

            if (isset($params['from']) && $params['from'] != "null") {
                $query = $query->where('event_bgnde', '>=', Carbon::parse($params['from']));
            }

            if (isset($params['to']) && $params['to'] != "null") {
                $query = $query->where('event_bgnde', '<', Carbon::parse($params['to'])->addDay(1));
            }

            if (isset($params['is_event']) && $params['is_event'] != "null") {
                if ($params['is_event'] == 1) {
                    $query = $query->where('is_try_event', '=', 1);
                }
            } else {
                $query = $query->where('is_try_event', '=', 0);
            }

            $query = $query->select(
                'TCT_GOODS.cntnts_no AS id',
                'TCT_GOODS.brnd_code AS brnd_code',
                'TOM_CNTNTS_WDTB.sj AS product_name',
                'TCT_GOODS.brnd_code AS brand_code',
                'TCT_GOODS.modl_nombr AS model_number',
                'TCT_GOODS.link_url AS product_url',
                'TCT_GOODS.goods_cl_code AS goods_cl_code',
                'TCT_GOODS.event_knd_code',
                'TCT_GOODS.goods_pc as price',
                'TCT_GOODS.event_bgnde as start_date',
                'TCT_GOODS.event_endde as end_date',
                'TCT_GOODS.quantity_to_qualify as joining',
                'TCT_GOODS.dlvy_bgnde as delivery_start_date',
                'TCT_GOODS.dlvy_endde as delivery_end_date',
                'TCT_GOODS.event_trgter_co as number_of_participant'
            );

            return $query->orderBy('id', 'asc')->orderBy('TOM_CNTNTS_WDTB.regist_dt', 'desc');
        })->all();

        $brand_ids = $tries->pluck('brnd_code');

        $brands = $this->code_repository->findWhereIn('code', $brand_ids->toArray())->keyBy('code');

        $categories = $this->code_repository->findWhere([
            'code_group' => Constant::$CATEGORY_GROUP_CODE,
            'use_at' => 'Y'
        ])->keyBy('code');


        $try_ids = $tries->pluck('id')->toArray();

        $number_of_tries = $this->user_try_repository->getByCriteria(new GetNumberOfTriesByTryIdsCriteria($try_ids))->keyBy('cntnts_no');

//        return $number_of_tries;

        $rs = [];

        foreach ($tries as $item) {
            $data = [
                'id' => $item->id,
                'product_name' => $item->product_name,
                'brand_code' => $item->brand_code,
                'brand_name' => isset($brands[$item->brnd_code]) ? $brands[$item->brnd_code]['code_nm'] : '',
                'category' => isset($categories[$item->goods_cl_code]) ? $categories[$item->goods_cl_code]['code_nm'] : '',
                'model_number' => $item->model_number,
                'product_url' => $item->product_url,
                'event_knd_code' => $item->event_knd_code == 398001 ? 'Paid' : 'Free',
                'price' => $item->event_knd_code == 398001 ? (isset($item->price) ? $item->price : '0')  : '0',
                'start_date' => isset($item->start_date) ? Carbon::parse($item->start_date)->format('Y-m-d') : null,
                'end_date' => isset($item->end_date) ? Carbon::parse($item->end_date)->format('Y-m-d') : null,
                'number_of_participant' => $item->number_of_participant,
                'delivery_start_date' => isset($item->delivery_start_date) ? Carbon::parse($item->delivery_start_date)->format('Y-m-d') : null,
                'delivery_end_date' => isset($item->delivery_end_date) ? Carbon::parse($item->delivery_end_date)->format('Y-m-d') : null,
                'status' => $item->number_of_participant . '/' . (isset($number_of_tries[$item->id]) ? $number_of_tries[$item->id]->number_of_tries : 0)
            ];
            array_push($rs, $data);
        }

        return $rs;
    }

}
