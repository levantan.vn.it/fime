<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 11:58 AM
 */

namespace App\Actions\ExcelExport;


use App\Actions\Action;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\Review\GetNumberOfReviewsByUserIdsCriteria;
use App\Criterias\TryFree\GetTryDetailCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByUserIdCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\UserTry\GetAllCriteria;
use App\Criterias\UserTry\GetNumberOfSelectedTriesByUserIdsCriteria;
use Carbon\Carbon;

class WinnerAction extends Action
{
    protected $repository;
    protected $try_repository;
    protected $user_repository;
    protected $user_like_repository;
    protected $follow_repository;
    protected $review_repository;

    public function __construct(UserTryRepositoryInterface $repository,
                                UserRepositoryInterface $user_repository,
                                UserLikeRepositoryInterface $user_like_repository,
                                ReviewRepositoryInterface $review_repository,
                                UserFollowRepositoryInterface $follow_repository,
                                TryRepositoryInterface $try_repository)
    {
        $this->repository = $repository;
        $this->user_like_repository = $user_like_repository;
        $this->user_repository = $user_repository;
        $this->try_repository = $try_repository;
        $this->review_repository = $review_repository;
        $this->follow_repository = $follow_repository;
    }

    public function run($request)
    {
        $user_tries = $this->repository->getByCriteria(new GetAllCriteria($request['try_id'], $request['name']));
        $user_ids = $user_tries->pluck('user_no')->toArray();

        $reviews = $this->review_repository->getByCriteria(new GetNumberOfReviewsByUserIdsCriteria($user_ids))->keyBy('user_id');

        $likes = $this->user_repository->getByCriteria(new GetNumberOfLikesByUserIdCriteria($user_ids))->keyBy('user_id');

        $selected_tries = $this->repository->getByCriteria(new GetNumberOfSelectedTriesByUserIdsCriteria($user_ids))->keyBy('user_id');

        $followers = $this->follow_repository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($user_tries as $user_try) {
            $user_try->number_of_followers = 0;
            $user_try->number_of_reviews = 0;
            $user_try->number_of_likes = 0;
            $user_try->number_of_selected = 0;

            if (isset($followers[$user_try->user_no])) {
                $user_try->number_of_followers = $followers[$user_try->user_no]->number_of_followers;
            }

            if (isset($reviews[$user_try->user_no])) {
                $user_try->number_of_reviews = $reviews[$user_try->user_no]->number_of_reviews;
            }

            if (isset($likes[$user_try->user_no])) {
                $user_try->number_of_likes = $likes[$user_try->user_no]->number_of_likes;
            }

            if (isset($selected_tries[$user_try->user_no])) {
                $user_try->number_of_selected = $selected_tries[$user_try->user_no]->number_of_tries;
            }
        }

        $try = $this->try_repository->getByCriteria(new GetTryDetailCriteria($request['try_id']))[0];

        $rs = [];
        foreach ($user_tries as $user_try) {
            $data = [
                'user_no' => $user_try->user_no,
                'reg_name' => $user_try->reg_name,
                'email' => $user_try->email,
                'cellphone' => $user_try->cellphone,
                'home_addr1' => $user_try->home_addr1,
                'created_at' => isset($user_try->created_at) ? Carbon::parse($user_try->created_at)->format('Y-m-d') : null,
                'reqst_dt' => isset($user_try->reqst_dt) ? Carbon::parse($user_try->reqst_dt)->format('Y-m-d') : null,
                'number_of_selected' => $user_try->number_of_selected . '',
                'number_of_likes' => $user_try->number_of_likes . '',
                'number_of_followers' => $user_try->number_of_followers . '',
                'number_of_reviews' => $user_try->number_of_reviews . '',
                'is_select' => isset($user_try->slctn_at) ? ($user_try->slctn_at === 'Y' ? 'Yes' : 'No') : 'No',
                'is_delivery' => isset($user_try->dlvy_at) ? ($user_try->dlvy_at === 'Y' ? 'Yes' : 'No') : null,
                'delivery_date' => isset($user_try->dlvy_dt) ? Carbon::parse($user_try->dlvy_dt)->format('Y-m-d') : null,
            ];

            array_push($rs, $data);
        }

        return ['user_tries' => $rs, 'try' => $try];
    }
}
