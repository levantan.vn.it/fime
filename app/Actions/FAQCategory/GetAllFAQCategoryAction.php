<?php

namespace App\Actions\FAQCategory;
use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Criterias\FAQ\GetFAQCategoryOrderBySortCriteria;

class GetAllFAQCategoryAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run() {
        $categories = $this->repository->getByCriteria(new GetFAQCategoryOrderBySortCriteria());
        return $categories;
    }
}
