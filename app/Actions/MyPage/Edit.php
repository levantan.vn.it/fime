<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/20/2019
 * Time: 3:17 PM
 */

namespace App\Actions\MyPage;


use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Edit extends Action
{
    protected $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    public function run($data)
    {
        $user = $data['user'];

        if ($user['pic'] !== Auth::user()->avatar) {
            $file_data = $user['pic'];

            @list($type, $file_data) = explode(';base64,', $file_data);
            if (!empty($file_data)) {
                @list(, $extension) = explode('/', $type);

                $file_name = $this->generateRandomString() . '.' . $extension;

                \Storage::disk('data')->put('images/profile/' . $file_name, base64_decode($file_data));

                $user['pic'] = '/data/images/profile/' . $file_name;
            }
        }

        $rs = $this->userRepository->update([
            'home_addr1' => $user['home_addr1'],
            'pic' => $user['pic'],
            'email' => $user['email'],
            'cellphone' => $user['cellphone'],
            'self_intro' => $user['self_intro'],
            'updated_at' => Carbon::now()
        ], Auth::user()->user_no);


        return $rs;
    }

    private function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function is_base64_encoded($data)
    {
        if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
