<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/20/2019
 * Time: 3:17 PM
 */

namespace App\Actions\MyPage;


use App\Actions\Action;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;

class Follow extends Action
{
    protected $userFollowRepository;
    protected $userRepository;
    protected $reviewRepository;
    protected $userLikeRepository;
    protected $userTryRepository;
    protected $notification_repository;

    public function __construct(
        UserFollowRepositoryInterface $userFollowRepository,
        UserRepositoryInterface $userRepository,
        ReviewRepositoryInterface $reviewRepository,
        UserLikeRepositoryInterface $userLikeRepository,
        UserTryRepositoryInterface $userTryRepository,
        NotificationRepositoryInterface $notificationRepository
    )
    {
        $this->userFollowRepository = $userFollowRepository;
        $this->userRepository = $userRepository;
        $this->reviewRepository = $reviewRepository;
        $this->userLikeRepository = $userLikeRepository;
        $this->userTryRepository = $userTryRepository;
        $this->notification_repository = $notificationRepository;
    }

    public function run($data)
    {
        $userId = auth()->id();

        return $userId;

        $userLikes = $this->userFollowRepository->updateOrCreate(
            [
                'user_no' => $userId,
                'fllwr_user_no' => $data['id']
            ],
            [
                'followed' => (int)$data['followed'] === 0 ? 1 : 0
            ]
        );


//        $followed = $data['followed'] === 0 ? false : true;
//        $this->createLikeNotification($data, false, $followed);

        return $userLikes;
    }
}
