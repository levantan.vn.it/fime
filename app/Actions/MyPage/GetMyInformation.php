<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/15/2019
 * Time: 10:50 AM
 */

namespace App\Actions\MyPage;


use App\Actions\Action;
use App\Contracts\HashtagRepositoryInterface;
use App\Contracts\ReviewHashTagsRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\Review\GetNumberOfReviewsByUserIdsCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByUserIdCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;
use App\Criterias\UserTry\GetNumberOfTriesByUserIdsCriteria;

class GetMyInformation extends Action
{
    protected $userRepository;
    protected $reviewRepository;
    protected $followRepository;
    protected $userLikeRepository;
    protected $userTryRepository;
    protected $hashtagRepository;
    protected $reviewHashTagsRepository;

    /**
     * GetMyInformation constructor.
     * @param UserRepositoryInterface $userRepository
     * @param ReviewRepositoryInterface $reviewRepository
     * @param UserLikeRepositoryInterface $userLikeRepository
     * @param UserTryRepositoryInterface $userTryRepository
     * @param UserFollowRepositoryInterface $followRepository
     * @param ReviewHashTagsRepositoryInterface $reviewHashTagsRepository
     * @param HashtagRepositoryInterface $hashtagRepository
     */
    public function __construct(UserRepositoryInterface $userRepository,
                                ReviewRepositoryInterface $reviewRepository,
                                UserLikeRepositoryInterface $userLikeRepository,
                                UserTryRepositoryInterface $userTryRepository,
                                UserFollowRepositoryInterface $followRepository,
                                ReviewHashTagsRepositoryInterface $reviewHashTagsRepository,
                                HashtagRepositoryInterface $hashtagRepository)
    {
        $this->userRepository = $userRepository;
        $this->reviewRepository = $reviewRepository;
        $this->followRepository = $followRepository;
        $this->userLikeRepository = $userLikeRepository;
        $this->userTryRepository = $userTryRepository;
        $this->reviewHashTagsRepository = $reviewHashTagsRepository;
        $this->hashtagRepository = $hashtagRepository;
    }

    public function run($request)
    {
        $slug = $request->slug;

        $user = $this->userRepository->findWhere(['slug' => $slug, 'drmncy_at' => 'N'])->first();

        if ($user == null)
            return null;


        $followers = $this->followRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria([$user['user_no']])
        )->keyBy('user_id');

        $followings = $this->followRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria([$user['user_no']])
        )->keyBy('user_id');

        $number_of_reviews = $this->reviewRepository->getByCriteria(
            new GetNumberOfReviewsByUserIdsCriteria([$user['user_no']]))->keyBy('user_id');

        $likes = $this->userRepository->getByCriteria(
            new GetNumberOfLikesByUserIdCriteria([$user['user_no']]))->keyBy('user_id');

        $tries = $this->userTryRepository->getByCriteria(
            new GetNumberOfTriesByUserIdsCriteria([$user['user_no']]))->keyBy('user_no');

        /*
         * get hashtag of user
         */
        $reviewsIds = $this->reviewRepository->findByField('user_no', $user['user_no'], ['review_no'])->pluck('review_no')->toArray();
        $hashTags = [];
        if (count($reviewsIds)) {
            $hashtagIds = $this->reviewHashTagsRepository->findWhereIn('review_no', $reviewsIds, ['hash_seq'])->pluck('hash_seq')->toArray();
            $hashTags = $this->hashtagRepository->orderBy('hash_seq', 'desc')->findWhereIn('hash_seq', $hashtagIds, ['hash_tag']);
        }
        $user->hashTags = $hashTags;


        $user['number_of_reviews'] = 0;
        $user['number_of_followers'] = 0;
        $user['number_of_followings'] = 0;
        $user['number_of_likes'] = 0;
        $user['number_of_try_free'] = 0;

        if (isset($number_of_reviews[$user['user_no']])) {
            $user['number_of_reviews'] = $number_of_reviews[$user['user_no']]->number_of_reviews;
        }

        if (isset($likes[$user['user_no']])) {
            $user['number_of_likes'] = $likes[$user['user_no']]->number_of_likes;
        }

        if (isset($tries[$user['user_no']])) {
            $user['number_of_try_free'] = $tries[$user['user_no']]->number_of_tries;
        }

        if (isset($followers[$user['user_no']])) {
            $user['number_of_followers'] = $followers[$user['user_no']]->number_of_followers;
        }

        if (isset($followings[$user['user_no']])) {
            $user['number_of_followings'] = $followings[$user['user_no']]->number_of_followings;
        }


        return $user;
    }
}
