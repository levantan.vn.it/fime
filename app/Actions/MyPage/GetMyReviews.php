<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/15/2019
 * Time: 5:11 PM
 */

namespace App\Actions\MyPage;


use App\Actions\Action;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Files\GetMainImageOfReviewsCriteria;
use App\Criterias\Review\GetReviewsByUserId;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByReviewIdsCriteria;
use App\Criterias\UserLike\GetReviewLikeInfoCriteria;
use App\Criterias\UserLike\GetReviewLikesByUserIdCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;

class GetMyReviews extends Action
{
    protected $userRepository;
    protected $reviewRepository;
    protected $userLikeRepository;
    protected $reviewFileRepository;
    protected $userFollowRepository;

    public function __construct(UserRepositoryInterface $userRepository,
                                ReviewRepositoryInterface $reviewRepository,
                                ReviewLikeRepositoryInterface $userLikeRepository,
                                ReviewFilesRepositoryInterface $reviewFileRepository,
                                UserFollowRepositoryInterface $userFollowRepository)
    {
        $this->userRepository = $userRepository;
        $this->reviewRepository = $reviewRepository;
        $this->userLikeRepository = $userLikeRepository;
        $this->reviewFileRepository = $reviewFileRepository;
        $this->userFollowRepository = $userFollowRepository;
    }

    public function run($request)
    {
        if (isset($request['userId']))
            $userId = $request['userId'];
        else return [];
        
        $reviews = $this->reviewRepository->pushCriteria(new GetReviewsByUserId($userId))->paginate(12);


        $this->getLikesNumber($reviews);
        $this->getListUserLike($reviews);

        return $reviews;
    }


    protected function getLikesNumber($reviews) {
        try {
            $review_ids = $reviews->pluck('review_no');

            $current_user_id = auth()->id();
            $likes = [];

            if($current_user_id) {
                $likes = $this->userLikeRepository->getByCriteria(new GetReviewLikesByUserIdCriteria($review_ids, $current_user_id))
                    ->keyBy('review_no');
            }

            $review_ids = $reviews->pluck('review_no')->toArray();

            $reviewsMainImage = $this->reviewFileRepository->getByCriteria(new GetMainImageOfReviewsCriteria($review_ids, true))->keyBy('review_no');

            $list_like_number = $this->userLikeRepository->getByCriteria(new GetNumberOfLikesByReviewIdsCriteria($review_ids));

            $list_like_number = collect($list_like_number)->keyBy('object_id');

            foreach ($reviews as $review) {
                if (isset($list_like_number[$review->review_no])) {
                    $review->total_like = $list_like_number[$review->review_no]->like_number;
                } else {
                    $review->total_like = 0;
                }

                $review->is_liked = 0;
                if(isset($likes[$review->review_no])) {
                    $review->is_liked = 1;
                }

                if(isset($reviewsMainImage[$review->review_no]))
                    $review->main_image = $reviewsMainImage[$review->review_no];
            }
            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    protected function getListUserLike($reviews)
    {
        try {
            foreach ($reviews as $review) {
                $list_user_liked = $this->userLikeRepository->getByCriteria(new GetReviewLikeInfoCriteria($review->review_no));
                $fimers = $this->getCurrentUserFollowing($list_user_liked);
                $review->users_liked = $fimers;
            }

            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->userFollowRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');


        $followers = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->id])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }
}
