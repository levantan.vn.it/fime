<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/20/2019
 * Time: 9:54 AM
 */

namespace App\Actions\MyPage;

use App\Actions\TryFree\GetTriesAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\TryFree\GetTriesOfUserCriteria;

class GetMyTries extends GetTriesAction
{
    protected   $user_try_repository;
    protected   $codeRepository;
    public function __construct(
        TryRepositoryInterface $tryRepository,
        UserTryRepositoryInterface $userTriesRepository,
        UserRepositoryInterface $userRepository,
        UserLikeRepositoryInterface $userLikeRepository,
        CommentRepositoryInterface $commentRepository,
        CodeRepositoryInterface $codeRepository,
        UserFollowRepositoryInterface $followRepository
    )
    {
        $this->user_try_repository = $userTriesRepository;
        $this->codeRepository = $codeRepository;
        parent::__construct($tryRepository, $commentRepository,
            $userLikeRepository, $userTriesRepository, $userRepository, $followRepository);
    }

    public function run($request)
    {
        if (isset($request['userId']))
            $userId = $request['userId'];

        else return [];

        $tries = $this->user_try_repository->pushCriteria(new GetTriesOfUserCriteria($userId))->paginate(12);

        $this->decorateData($tries);

        $code_ids = $tries->pluck('dlvy_cmpny_code')->toArray();
        $status_ids = $tries->pluck('dlvy_mth_code')->toArray();

        $code_ids = array_merge($code_ids, $status_ids);

        $codes = $this->codeRepository->findWhereIn('code', $code_ids)->keyBy('code');

        foreach ($tries as $item) {
            if(isset($item->dlvy_cmpny_code) && isset($codes[$item->dlvy_cmpny_code])) {
                $item->dlvy_cmpny_code = $codes[$item->dlvy_cmpny_code]->code_nm;
            }

            if(isset($item->dlvy_mth_code) && isset($codes[$item->dlvy_mth_code])) {
                $item->dlvy_mth_code = $codes[$item->dlvy_mth_code]->code_nm;
            }
        }

        return $tries;
    }

}
