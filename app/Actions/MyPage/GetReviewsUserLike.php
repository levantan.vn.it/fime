<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 9:46 AM
 */

namespace App\Actions\MyPage;


use App\Actions\Action;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface as ReviewsLikes;
use App\Contracts\UserLikeRepositoryInterface as TriesLikes;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Files\GetMainImageOfReviewsCriteria;
use App\Criterias\Review\GetReviewsThatUserLiked;
use App\Criterias\TryFree\GetFilesByListIdCriteria;
use App\Criterias\TryFree\GetTriesThatUserLikedCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetLikeNumberOfTryByListIdCriteria;
use App\Criterias\UserLike\GetLikesOfCurrentUserCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByReviewIdsCriteria;
use App\Criterias\UserLike\GetReviewLikeInfoCriteria;
use App\Criterias\UserLike\GetReviewLikesByUserIdCriteria;
use App\Criterias\UserLike\GetUserLikeInfoCriteria;
use App\Criterias\UserLike\GetUserLikesCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;

class GetReviewsUserLike extends Action
{
    protected $userLikesRepository;
    protected $userTriesLikesRepository;
    protected $reviewRepository;
    protected $tryRepository;
    protected $userRepository;
    protected $reviewLikeRepository;
    protected $reviewFileRepository;
    protected $userFollowRepository;

    public function __construct(ReviewsLikes $userLikesRepository,
                                TriesLikes $userTriesLikesRepository,
                                TryRepositoryInterface $tryRepository,
                                UserRepositoryInterface $userRepository,
                                ReviewLikeRepositoryInterface $reviewLikeRepository,
                                ReviewFilesRepositoryInterface $reviewFileRepository,
                                ReviewRepositoryInterface $reviewRepository,
                                UserFollowRepositoryInterface $userFollowRepository)
    {
        $this->userLikesRepository = $userLikesRepository;
        $this->userTriesLikesRepository = $userTriesLikesRepository;
        $this->reviewLikeRepository = $reviewLikeRepository;
        $this->reviewRepository = $reviewRepository;
        $this->tryRepository = $tryRepository;
        $this->userRepository = $userRepository;
        $this->reviewFileRepository = $reviewFileRepository;
        $this->userFollowRepository = $userFollowRepository;
    }

    protected function getFiles($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $files = $this->tryRepository->getByCriteria(new GetFilesByListIdCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->file = [];
            if (isset($files[$try->cntnts_no])) {
                $try->file = $files[$try->cntnts_no];
            }
        }

        return $tries;
    }


    public function run($request)
    {
        if (isset($request['userId']))
            $userId = $request['userId'];
        else return [];

        $objects = $this->userRepository->pushCriteria(new GetUserLikesCriteria($userId))->paginate(10);

        $reviewIds = $objects->filter(function ($obj) {
            return $obj->type == 'review';
        })->pluck('id')->toArray();

        $tryIds = $objects->filter(function ($obj) {
            return $obj->type == 'try';
        })->pluck('id')->toArray();

        $reviews = $this->reviewRepository->getByCriteria(new GetReviewsThatUserLiked($reviewIds));

        $tries = $this->tryRepository->getByCriteria(new GetTriesThatUserLikedCriteria($tryIds));

        $tries = $this->getFiles($tries);

        $tries =  $tries->map(function ($obj) {
            $obj->is_try = true;
            return $obj;
        });

        $reviewsMainImage = $this->reviewFileRepository->getByCriteria(new GetMainImageOfReviewsCriteria($reviewIds, true))->keyBy('review_no');

        $current_user_id = auth()->id();
        $review_likes = [];
        $total_review_likes = [];
        $try_likes = [];
        $total_try_likes = [];
        if($current_user_id) {
            $total_review_likes = $this->reviewLikeRepository->getByCriteria(new GetNumberOfLikesByReviewIdsCriteria($reviewIds))->keyBy('object_id');

            $review_likes = $this->reviewLikeRepository->getByCriteria(new GetReviewLikesByUserIdCriteria($reviewIds, $current_user_id))
                ->keyBy('review_no');

            $total_try_likes = $this->userLikesRepository->getByCriteria(new GetLikeNumberOfTryByListIdCriteria($tryIds))->keyBy('cntnts_no');

            $try_likes = $this->userLikesRepository->getByCriteria(new GetLikesOfCurrentUserCriteria($tryIds, $current_user_id))
                ->keyBy('cntnts_no');
        }

        $reviewsUsersLiked = $this->getReviewsListUserLike($reviewIds);
        $triesUsersLiked = $this->getTriesListUserLike($tryIds);

        foreach ($reviews as $review){
            if(!empty($reviewsUsersLiked[$review->review_no])){
                $review->users_liked = $reviewsUsersLiked[$review->review_no];
            }
        }

        foreach ($tries as $try){
            if(!empty($triesUsersLiked[$try->cntnts_no])){
                $try->users_liked = $triesUsersLiked[$try->cntnts_no];
            }
        }

        $reviews = $reviews->merge($tries)->toArray();
        $reviews = collect($reviews)->sortBy('created_at');

        $reviews =  $reviews->map(function ($obj) use($try_likes, $review_likes, $total_review_likes, $total_try_likes, $reviewsMainImage) {
            $obj['is_liked'] = false;
            $obj['total_like'] = false;
            $obj['updt_dt'] = $obj['created_at'];

            if(!empty($obj['is_try'])) {
                if(isset($try_likes[$obj['cntnts_no']])){
                    $obj['is_liked'] = true;
                }

                if(isset($total_try_likes[$obj['cntnts_no']])){
                    $obj['total_like'] = $total_try_likes[$obj['cntnts_no']]->like_number;
                }

            } else {
                if(isset($review_likes[$obj['review_no']])){
                    $obj['is_liked'] = true;
                }
                if(isset($total_review_likes[$obj['review_no']])){
                    $obj['total_like'] = $total_review_likes[$obj['review_no']]->like_number;
                }

                if(isset($reviewsMainImage[$obj['review_no']]))
                    $obj['main_image'] = $reviewsMainImage[$obj['review_no']];
            }
            return $obj;
        });

        $data = $objects->toArray();
        $data['data'] = $reviews->values()->all();
        return $data;
    }

    protected function getReviewsListUserLike($reviewIds)
    {
        $data = [];
        foreach ($reviewIds as $reviewId) {
            $list_user_liked = $this->reviewLikeRepository->getByCriteria(new GetReviewLikeInfoCriteria($reviewId));
            $fimers = $this->getCurrentUserFollowing($list_user_liked);
            $data[$reviewId] = $fimers;
        }

        return $data;
    }

    protected function getTriesListUserLike($tryIds)
    {
        $data = [];
        foreach ($tryIds as $tryId) {
            $user_liked = $this->userTriesLikesRepository->getByCriteria(new GetUserLikeInfoCriteria($tryId));
            $fimers = $this->getCurrentUserFollowing($user_liked);
            $data[$tryId] = $fimers;
        }
        return $data;
    }

    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->userFollowRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');


        $followers = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->id])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }
}
