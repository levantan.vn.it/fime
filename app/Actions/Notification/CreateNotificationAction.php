<?php

namespace App\Actions\Notification;

use App\Actions\UserPoint\AddPointsToUserAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use Carbon\Carbon;
use Mockery\Exception;

class CreateNotificationAction extends AddPointsToUserAction
{
    protected $notification_repository;
    protected $review_repository;
    protected $try_repository;

    /**
     * @return string
     */
    protected function getGUID()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid =
                substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12);
            return $uuid;
        }
    }

    /**
     * @param $object_id
     * @param $object_type
     * @param $author_id
     * @param bool $is_new
     * @param bool $is_disable
     * @return bool
     */
    protected function createLikeNotification($object_id, $object_type, $author_id, $is_new = false, $is_disable = false)
    {
        try {
            // If user like their review ,we will not generate notification
            if ($author_id == auth()->id()) {
                return false;
            }
            $data = [];
            $data['type'] =  'like';
            $data['user_id'] = $author_id;
            $data['object_id'] = $object_id;
            $data['object_type'] = $object_type;
            $data['reference_user_id'] = auth()->id();
            $data['guid'] = $this->getGUID();
            $data['posted_at'] = Carbon::now();
            $data['is_disabled'] = $is_disable;
            if ($is_new) {
                $this->notification_repository->create($data);
            } else {
                $notification = $this->notification_repository->findWhere(['user_id' => $data['user_id'],
                    'reference_user_id' => $data['reference_user_id'],
                    'type' => $data['type'],
                    'object_type' => $data['object_type'],
                    'object_id' => $data['object_id']
                ]);
                if (count($notification) > 0) {
                    $this->notification_repository->update([
                        'is_disabled' => $is_disable,
                        'is_seen' => 0,
                        'posted_at' => Carbon::now()
                    ], $notification[0]->id);
                } else {
                    $this->notification_repository->create($data);
                }
            }
            return true;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $followed_user_id
     * @param bool $is_new
     * @param bool $is_disable
     * @return bool
     */
    protected function createFollowNotification($followed_user_id, $is_new = false, $is_disable = false) {
        try {
            if ($followed_user_id != auth()->id()) {
                $data = [];
                $data['type'] = 'follow';
                $data['user_id'] = $followed_user_id;
                $data['object_id'] = 0;
                $data['object_type'] = 'user';
                $data['reference_user_id'] = auth()->id();
                $data['guid'] = $this->getGUID();
                $data['posted_at'] = Carbon::now();
                $data['is_disabled'] = $is_disable;
                if ($is_new) {
                    $this->notification_repository->create($data);
                } else {
                    $notification = $this->notification_repository->findWhere(['user_id' => $data['user_id'],
                        'reference_user_id' => $data['reference_user_id'],
                        'type' => $data['type'],
                        'object_type' => $data['object_type'],
                        'object_id' => $data['object_id']
                    ]);
                    if (count($notification) > 0) {
                        $this->notification_repository->update([
                            'is_disabled' => $is_disable,
                            'is_seen' => 0,
                            'posted_at' => Carbon::now()
                        ], $notification[0]->id);
                    } else {
                        $this->notification_repository->create($data);
                    }
                }
                return true;
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }


    /**
     * @param $object_id
     * @param $object_type
     * @param $author_id
     * @param $type
     */
    protected function createCommentNotification($object_id, $object_type, $author_id, $type) {
        try {
            if ($author_id != auth()->id()) {
                $data = [];
                $data['type'] = $type;
                $data['user_id'] = $author_id;
                $data['object_id'] = $object_id;
                $data['object_type'] = $object_type;
                $data['reference_user_id'] = auth()->id();
                $data['guid'] = $this->getGUID();
                $data['posted_at'] = Carbon::now();
                $data['is_disabled'] = 0;
                $this->notification_repository->create($data);
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $try_id
     * @param $is_disable
     * @return int
     */
    protected function createTryAppliedNotification($try_id, $is_disable) {
        try {
            $try = $this->try_repository->find($try_id);
            // If try begin is over, skip this notification
            if ($try->event_bgnde < Carbon::now()) {
                return 0;
            }
            // Else add a notification with posted_at is open time
            $data = [];
            $data['type'] = 'try_applied';
            $data['title'] = 'try_open';
            $data['user_id'] = auth()->id();
            $data['object_id'] = $try_id;
            $data['object_type'] = 'try';
            $data['reference_user_id'] = null;
            $data['guid'] = $this->getGUID();
            $data['posted_at'] = $try->event_bgnde;
            $data['is_disabled'] = 0;
            $notification = $this->notification_repository->findWhere([
                'object_id' => $try_id,
                'type' => 'try_applied',
                'user_id' => auth()->id(),
                'object_type' => 'try'
            ]);
            if (count($notification) > 0) {
                $this->notification_repository->update([
                    'is_disabled' => $is_disable,
                    'is_seen' => 0,
                    'posted_at' => $try->event_bgnde
                ], $notification[0]->id);
            } else {
                $this->notification_repository->create($data);
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function createDeliveryNotification($try_id, $user_no, $posted_at) {
        try {
            // If try begin is over, skip this notification
            // Else add a notification with posted_at is open time
            $data = [];
            $data['type'] = 'delivery';
            $data['title'] = 'try_delivery';
            $data['user_id'] = $user_no;
            $data['object_id'] = $try_id;
            $data['object_type'] = 'try';
            $data['reference_user_id'] = null;
            $data['guid'] = $this->getGUID();
            $data['posted_at'] = Carbon::parse($posted_at, 'UTC');
            $data['is_disabled'] = 0;
            $notification = $this->notification_repository->findWhere([
                'object_id' => $try_id,
                'type' => 'delivery',
                'user_id' => $user_no,
                'object_type' => 'try'
            ]);
            if (count($notification) > 0) {
                $this->notification_repository->update([
                    'posted_at' => Carbon::parse($posted_at, 'UTC')
                ], $notification[0]->id);
            } else {
                $this->notification_repository->create($data);
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }


    /**
     * CreateNotificationAction constructor.
     * @param NotificationRepositoryInterface $notification_repository
     * @param ReviewRepositoryInterface $review_repository
     * @param TryRepositoryInterface $try_repository
     * @param UserPointRepositoryInterface $user_point_repository
     * @param CodeRepositoryInterface $codeRepository
     */
    public function __construct(NotificationRepositoryInterface $notification_repository,
                                ReviewRepositoryInterface $review_repository,
                                TryRepositoryInterface $try_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                CodeRepositoryInterface $codeRepository)
    {
        $this->notification_repository = $notification_repository;
        $this->review_repository = $review_repository;
        $this->try_repository = $try_repository;
        parent::__construct($user_point_repository, $codeRepository);
    }

}
