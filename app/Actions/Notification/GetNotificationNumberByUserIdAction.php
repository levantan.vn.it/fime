<?php

namespace App\Actions\Notification;

use App\Actions\Action;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Criterias\Notification\GetNotificationNumberByUserIdCriteria;

class GetNotificationNumberByUserIdAction extends Action
{
    protected $notification_repository;
    protected $review_repository;

    public function __construct(NotificationRepositoryInterface $notification_repository,
                                ReviewRepositoryInterface $review_repository)
    {
        $this->notification_repository = $notification_repository;
        $this->review_repository = $review_repository;
    }

    public function run()
    {
        $current_user_id = auth()->id();
        $notifications = $this->notification_repository->getByCriteria(new GetNotificationNumberByUserIdCriteria($current_user_id));
        return $notifications[0]->count;
    }
}
