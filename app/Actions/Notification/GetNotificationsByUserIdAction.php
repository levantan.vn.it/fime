<?php

namespace App\Actions\Notification;

use App\Actions\Action;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryExtRepositoryInterface;
use App\Criterias\Notification\GetNotificationsByUserIdCriteria;
use App\Criterias\Notification\GetNotificationsCriteria;
use App\Criterias\Notification\GetNotificationUserCriteria;

class GetNotificationsByUserIdAction extends Action
{
    protected $notification_repository;
    protected $review_repository;
    protected $try_ex_repository;

    public function __construct(NotificationRepositoryInterface $notification_repository,
                                ReviewRepositoryInterface $review_repository,
                                TryExtRepositoryInterface $try_ex_repository)
    {
        $this->notification_repository = $notification_repository;
        $this->review_repository = $review_repository;
        $this->try_ex_repository = $try_ex_repository;
    }

    public function run($data)
    {
        $page = $data['page'];
        $current_user_id = auth()->id();
        $list_notification_ids = $this->notification_repository->getByCriteria(new GetNotificationsByUserIdCriteria($current_user_id, $page));
        $notification_ids = [];
        $list_review_id = [];
        foreach ($list_notification_ids as $list_notification_id) {
            array_push($notification_ids, $list_notification_id->id);
        }
        $notifications = $this->notification_repository->getByCriteria(new GetNotificationsCriteria($notification_ids));
        $list_nonsystem_notification_ids = [];
        foreach ($notifications as $notification) {
            if ($notification->is_system == 0) {
                array_push($list_nonsystem_notification_ids, $notification->id);
                if ($notification->object_type == 'review') {
                    array_push($list_review_id, $notification->object_id);
                }
            }
        }
        $nonsystem_notifications = $this->notification_repository->getByCriteria(new GetNotificationUserCriteria($list_nonsystem_notification_ids));
        $review_notifications = $this->review_repository->findWhereIn('review_no', $list_review_id);
        foreach ($notifications as $index => $notification) {
            foreach ($nonsystem_notifications as $nonsystem_notification) {
                if ($notification->id == $nonsystem_notification->id) {
                    $notifications[$index]->title = $nonsystem_notification->user_name;
                    $notifications[$index]->user_avatar = $nonsystem_notification->user_avatar;
                    if ($notification->object_type == 'user') {
                        $notifications[$index]->url = '/usr/' . $nonsystem_notification->user_slug;
                    }
                }
            }
            foreach ($review_notifications as $review_notification) {
                if ($notification->object_id == $review_notification->review_no &&
                    $notification->object_type == 'review') {
                    $notifications[$index]->url = '/reviews/detail/' . $review_notification->slug;
                }
            }
        }
        $this->getProductNameOfTryAppliedNotification($notifications);
        return $notifications;
    }

    protected function getProductNameOfTryAppliedNotification($notifications) {
        $notification_ids = [];
        // Get try ids
        foreach ($notifications as $notification) {
            if ($notification->type == 'try_applied' || $notification->type == 'delivery') {
                array_push($notification_ids, $notification->object_id);
            }
        }
        // Reassign try name
        foreach ($notifications as $notification) {
            if ($notification->type == 'try_applied') {
                array_push($notification_ids, $notification->object_id);
            }
        }
        $tries = $this->try_ex_repository->findWhereIn('cntnts_no', $notification_ids)->keyBy('cntnts_no');

        foreach ($notifications as $notification) {
            if ($notification->type == 'try_applied' && isset($tries[$notification->object_id])) {
                $notification->product_name = $tries[$notification->object_id]->sj;
                $notification->url = '/tries/' . $tries[$notification->object_id]->slug;
            }
            if ($notification->type == 'delivery' && isset($tries[$notification->object_id])) {
                $notification->product_name = $tries[$notification->object_id]->sj;
                $notification->url = '/tries/' . $tries[$notification->object_id]->slug;
            }
        }
        return $notifications;
    }
}
