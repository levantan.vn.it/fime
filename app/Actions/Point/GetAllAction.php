<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/5/2019
 * Time: 4:02 PM
 */

namespace App\Actions\Point;


use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;

class GetAllAction extends Action
{
    private $codeRepository;

    public function __construct(CodeRepositoryInterface $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    public function run()
    {
        return $this->codeRepository->findByField('code_group', 405);
    }
}
