<?php

namespace App\Actions\Review;

use App\Actions\Constant;
use App\Actions\UserPoint\AddPointsToUserAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\FilesRepositoryInterface;
use App\Contracts\HashtagRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewHashTagsRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\UserTry\GetWinnersByTryIdCriteria;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class CreateReviewAction extends AddPointsToUserAction
{
    protected $review_repository;
    protected $file_repository;
    protected $review_file_repository;
    protected $hashtag_repository;
    protected $user_repository;
    protected $reviewHashTagsRepository;
    protected $user_try_repository;

    public function __construct(UserRepositoryInterface $user_repository,
                                ReviewRepositoryInterface $review_repository,
                                HashtagRepositoryInterface $hashtag_repository,
                                FilesRepositoryInterface $file_repository,
                                ReviewFilesRepositoryInterface $review_file_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                ReviewHashTagsRepositoryInterface $reviewHashTagsRepository,
                                UserTryRepositoryInterface $user_try_repository,
                                CodeRepositoryInterface $codeRepository)
    {
        $this->user_repository = $user_repository;
        $this->review_repository = $review_repository;
        $this->file_repository = $file_repository;
        $this->review_file_repository = $review_file_repository;
        $this->hashtag_repository = $hashtag_repository;
        $this->reviewHashTagsRepository = $reviewHashTagsRepository;
        $this->user_try_repository = $user_try_repository;
        parent::__construct($user_point_repository, $codeRepository);
    }

    public function run($data)
    {
        try {
            $review = [];
            $images = $data['images'];
            $review['cntnts_no'] = isset($data['cntnts_no']) ? $data['cntnts_no']: null;
            if ($review['cntnts_no']) {
                $winners = $this->user_try_repository->getByCriteria(new GetWinnersByTryIdCriteria($review['cntnts_no']));
                if (count($winners) == 0) {
                    $review['cntnts_no'] = null;
                } else {
                    $review['cntnts_no'] = null;
                    foreach ($winners as $winner) {
                        if ($winner->user_no == auth()->id()) {
                            $review['cntnts_no'] = $data['cntnts_no'];
                            break;
                        }
                    }
                }
            }
            $review['goods_cl_code'] = $data['goods_cl_code'];
            $review['goods_nm'] = $data['goods_nm'];
            $review['review_short'] = $data['review_short'];
            $review['review_dc'] = $data['review_dc'];
            $review['user_no'] = auth()->id();
            $review['writng_dt'] = Carbon::now();
            $review['delete_at'] = 'N';

            // Check this user can review
            $user = Auth::user();
            $expsr_at = $user->allow_comment ? 'Y' : 'N';
            $review['expsr_at'] = $expsr_at;

            // Get last id
            $last = $this->review_repository->scopeQuery(function ($query) {
                return $query->orderBy('review_no', 'desc')
                    ->take(1);
            })->all()->first();

            $review_no = $last->review_no + 1;
            $review['review_no'] = $review_no;

            $new_review = $this->review_repository->create($review);
            $list_images = [];
            if (!empty($images)) {
                foreach ($images as $index => $image) {
                    $new_image = $this->review_file_repository->create([
                        'file_se_code' => Constant::$FILE_SE_CODE,
                        'file_sn' => $index + 1,
                        'orginl_file_nm' => $image['name'],
                        'stre_file_nm' => $image['name'],
                        'thumb_file_nm' => $image['thumb_name'],
                        'thumb_file_nm2' => $image['name'],
                        'file_cours' => $image['url'],
                        'se_code' => Constant::$OTHER_SE_CODE,
                        'review_no' => $new_review->review_no
                    ]);
                    array_push($list_images, $new_image);
                }
            }
            $new_review->images = $list_images;

            $hashtags = [];
            preg_match_all('/#([A-Za-z-_!?.@%^&*$0-9]+)/', $review['review_dc'], $hashtags);

            if (count($hashtags) > 1) {
                $hashtags = array_unique($hashtags[0]);

                foreach ($hashtags as $hashtag) {
                    $hashtag = strtoupper($hashtag);

                    $existing_tag = $this->hashtag_repository->findWhere([
                        'hash_tag' => $hashtag
                    ])->first();

                    if ($existing_tag) {
                        $total = $this->review_repository->count([
                            ['review_dc', 'like', '%' . $hashtag . '%']
                        ]);

                        $this->hashtag_repository->update([
                            'hash_cnt' => $total
                        ], $existing_tag->hash_seq);
                        $this->updateReviewHashTagTable($new_review->review_no, $existing_tag->hash_seq);
                    } else {
                        $hashtagRecord = $this->hashtag_repository->create([
                            'hash_cnt' => 1,
                            'hash_tag' => $hashtag,
                            'hash_dt' => Carbon::now()
                        ]);
                        $this->updateReviewHashTagTable($new_review->review_no, $hashtagRecord->hash_seq);
                    }
                }
            }

            // Add point
            if(isset($data['cntnts_no']))
                $this->tryReviewWinner(auth()->id(), $data['cntnts_no'], $new_review->review_no, false);
            else
                $this->writeReview(auth()->id(), $new_review->review_no, false);

            $this->user_repository->update([
                  'reviews' => $user->reviews + 1
              ], $user->user_no);

            return $new_review;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function updateReviewHashTagTable($review_no, $hash_seq)
    {
        $count = $this->reviewHashTagsRepository->count([
            'review_no' => $review_no,
            'hash_seq' => $hash_seq
        ]);

        if ($count === 0) {
            $this->reviewHashTagsRepository->create([
                'review_no' => $review_no,
                'hash_seq' => $hash_seq
            ]);
        }
    }
}
