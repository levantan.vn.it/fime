<?php

namespace App\Actions\Review;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Criterias\Comment\GetNumberOfCommentsByReviewIdsCriteria;
use App\Criterias\Files\GetMainImageOfReviewsCriteria;
use App\Criterias\TryFree\GetTryNameByIdsCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByReviewIdsCriteria;
use App\Repositories\ReviewRepository;
use Carbon\Carbon;

class GetAllReviewAction extends Action
{
    protected $review_repository;
    protected $commentRepository;
    protected $reviewLikeRepository;
    private $review_files_repository;
    private $try_repository;

    public function __construct(ReviewRepository $review_repository,
                                ReviewLikeRepositoryInterface $reviewLikeRepository,
                                ReviewFilesRepositoryInterface $review_files_repository,
                                ReviewCommentRepositoryInterface $commentRepository,
                                TryRepositoryInterface $try_repository)
    {
        $this->review_repository = $review_repository;
        $this->reviewLikeRepository = $reviewLikeRepository;
        $this->commentRepository = $commentRepository;
        $this->review_files_repository = $review_files_repository;
        $this->try_repository = $try_repository;
    }

    public function run($params)
    {
        $reviews = $this->review_repository->scopeQuery(function ($query) use ($params) {
            $query = $query->leftJoin('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_REVIEW.goods_cl_code')
                ->leftJoin('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
                ->leftJoin('TCT_MAIN_CONTS', function ($join) {
                    $join->on('TCT_MAIN_CONTS.ref_no', '=', 'TCT_REVIEW.review_no')
                        ->where('TCT_MAIN_CONTS.cont_type', Constant::$POPULAR_CONT_TYPE_CODE)
                        ->where('TCT_MAIN_CONTS.cont_std', Constant::$POPULAR_CONT_STD_CODE);
                })
                ->where('TCT_REVIEW.delete_at', 'N');

            if (isset($params['category_id']) && $params['category_id'] != "null") {
                $query = $query->where('goods_cl_code', $params['category_id']);
            }

            if (isset($params['is_disabled']) && $params['is_disabled'] != "null") {
                $query = $query->where('TCT_REVIEW.expsr_at', $params['is_disabled']);
            }

            if (isset($params['name']) && $params['name'] != "null") {
                $query = $query->where('goods_nm', 'like', '%' . $params['name'] . '%');
            }

            if (isset($params['type']) && $params['type'] != "null") {
                if ($params['type'] == 1) {
                    $query = $query->where('TCT_MAIN_CONTS.conts_seq', '<>', null);
                } else {
                    $query = $query->where('TCT_MAIN_CONTS.conts_seq', '=', null);
                }
            }

            if (isset($params['from']) && $params['from'] != "null") {
                $query = $query->where('writng_dt', '>=', Carbon::parse($params['from']));
            }

            if (isset($params['to']) && $params['to'] != "null") {
                $query = $query->where('writng_dt', '<', Carbon::parse($params['to'])->addDay(1));
            }

            if (isset($params['tryId']) && $params['tryId'] != "null") {
                $query = $query->where('cntnts_no', '=', $params['tryId']);
            }

            return $query->select('TCT_REVIEW.cntnts_no',
                'TCT_REVIEW.writng_dt',
                'TCT_REVIEW.delete_at',
                'TCT_REVIEW.expsr_at',
                'TCT_REVIEW.review_no',
                'TCT_REVIEW.user_no',
                'TCT_REVIEW.goods_cl_code',
                'TCT_REVIEW.goods_nm',
                'TCT_REVIEW.slug',
                'TDM_USER.reg_name', 'TSM_CODE.code_nm', 'TCT_MAIN_CONTS.conts_seq')
                ->orderBy($params['column'], $params['sort'])->orderBy('TCT_REVIEW.writng_dt', 'desc');
        })->paginate(10);

        $this->injectGoodName($reviews);

        $data = $reviews->toArray();

        $reviewsData = $this->decorateData($data['data']);
        $data['data'] = $reviewsData;
        return $data;
    }

    public function injectGoodName(& $reviews) {
        $review_ids = [];
        foreach ($reviews as $review) {
            if (($review->goods_nm == '' || $review->goods_nm == null) && $review->cntnts_no) {
                array_push($review_ids, $review->cntnts_no);
            }
        }

        $data = $this->try_repository->getByCriteria(new GetTryNameByIdsCriteria($review_ids));
        $data = $data->keyBy('cntnts_no');
        foreach ($reviews as $review) {
            if (isset($data[$review->cntnts_no]) ) {
                $review->goods_nm = $data[$review->cntnts_no]->sj;
            }
        }
    }

    public function decorateData($reviews)
    {
        $this->getLikesNumber($reviews);
        $this->getCommentNumberOfReview($reviews);
        $this->getFiles($reviews);
        return $reviews;
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getLikesNumber(&$reviews)
    {
        try {
            $review_ids = array_values(array_map(function ($review) {
                return $review['review_no'];
            }, $reviews));

            $list_like_number = $this->reviewLikeRepository->getByCriteria(new GetNumberOfLikesByReviewIdsCriteria($review_ids));
            $list_number_follow = collect($list_like_number)->keyBy('object_id');

            foreach ($reviews as $key => $review) {
                if (isset($list_number_follow[$review['review_no']])) {
                    $reviews[$key]['like_number'] = $list_number_follow[$review['review_no']]->like_number;
                } else {
                    $reviews[$key]['like_number'] = 0;
                }
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    protected function getFiles(&$reviews)
    {
        try {
            $review_ids = array_values(array_map(function ($review) {
                return $review['review_no'];
            }, $reviews));
            $reviewsMainImage = $this->review_files_repository->getByCriteria(new GetMainImageOfReviewsCriteria($review_ids, true))->keyBy('review_no');

            foreach ($reviews as $key => $review) {
                if(isset($reviewsMainImage[$review['review_no']])) {
                    $reviews[$key]['main_image'] = $reviewsMainImage[$review['review_no']];
                } else {
                    $reviews[$key]['main_image'] = ['file_cours' => null];
                }
            }
            return $reviews;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getCommentNumberOfReview(&$reviews)
    {
        try {
            $review_ids = array_values(array_map(function ($review) {
                return $review['review_no'];
            }, $reviews));

            $list_comment_number = $this->commentRepository->getByCriteria(new GetNumberOfCommentsByReviewIdsCriteria($review_ids));
            $list_comment_number = collect($list_comment_number)->keyBy('review_no');

            foreach ($reviews as $key => $review) {
                if (isset($list_comment_number[$review['review_no']])) {
                    $reviews[$key]['comment_number'] = $list_comment_number[$review['review_no']]->comment_number;
                } else {
                    $reviews[$key]['comment_number'] = 0;
                }
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}

