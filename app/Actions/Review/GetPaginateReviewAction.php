<?php

namespace App\Actions\Review;

use App\Contracts\HashtagRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewHashTagsRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Review\GetReviewAndAuthorPagingCriteria;
use App\Criterias\Review\GetReviewsByHashtagCriteria;
use Carbon\Carbon;

class GetPaginateReviewAction extends GetReviewByTypeAction
{
    private $user_repository;
    private $reviewHashTagsRepository;
    private $hashtagRepository;

    /**
     * GetPaginateReviewAction constructor.
     * @param ReviewRepositoryInterface $review_repository
     * @param UserFollowRepositoryInterface $user_follow_repository
     * @param ReviewLikeRepositoryInterface $user_like_repository
     * @param ReviewCommentRepositoryInterface $comment_repository
     * @param UserRepositoryInterface $user_repository
     * @param ReviewFilesRepositoryInterface $review_files_repository
     * @param ReviewHashTagsRepositoryInterface $reviewHashTagsRepository
     * @param HashtagRepositoryInterface $hashtagRepository
     */
    public function __construct(
        ReviewRepositoryInterface $review_repository,
        UserFollowRepositoryInterface $user_follow_repository,
        ReviewLikeRepositoryInterface $user_like_repository,
        ReviewCommentRepositoryInterface $comment_repository,
        UserRepositoryInterface $user_repository,
        ReviewFilesRepositoryInterface $review_files_repository,
        ReviewHashTagsRepositoryInterface $reviewHashTagsRepository,
        HashtagRepositoryInterface $hashtagRepository)
    {
        $this->user_repository = $user_repository;
        $this->reviewHashTagsRepository = $reviewHashTagsRepository;
        $this->hashtagRepository = $hashtagRepository;
        parent::__construct($review_repository, $user_follow_repository,
            $user_like_repository, $comment_repository, $review_files_repository);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function run($request)
    {

        try {
            $current_user_id = auth()->id();

            $reviewIds = [];
            if ($current_user_id) {
                $this->user_repository->update([
                    'review_click_dt' => Carbon::now()
                ], $current_user_id);
            }

            if (isset($request['tag']) && $request['tag'] != null && $request['tag'] != '') {
                $hashTags = $this->hashtagRepository->findWhere([
                    ['hash_tag', 'like', '%' . strtoupper($request['tag']) . '%']
                ]);
                $hashTagIds = $hashTags->pluck('hash_seq')->toArray();
                $reviewIds = $this->reviewHashTagsRepository->findWhereIn('hash_seq', $hashTagIds)->pluck('review_no')->toArray();
            }
            $reviews = $this->review_repository->pushCriteria(new GetReviewAndAuthorPagingCriteria($request['slug'], $request['searchValue'], $reviewIds))->paginate(12);

            $reviews = $this->decorateData($reviews);

            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
