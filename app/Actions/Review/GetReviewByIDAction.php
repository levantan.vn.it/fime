<?php

namespace App\Actions\Review;
use App\Actions\Action;
use App\Contracts\ReviewRepositoryInterface;

class GetReviewByIDAction extends Action {
    protected $review_repository;

    public function __construct(ReviewRepositoryInterface $review_repository) {
        $this->review_repository = $review_repository;
    }

    public function run($id) {
        try {
            $review = $this->review_repository->find($id);
            return $review;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
