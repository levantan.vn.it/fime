<?php

namespace App\Actions\Review;
use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\MainContRepositoryInterface;
use App\Repositories\ReviewRepository;
use Mockery\Exception;

class TogglePopularReviewAction extends Action {
    protected $review_repository;
    protected $main_review_repository;

    public function __construct(ReviewRepository $review_repository,
                                MainContRepositoryInterface $main_review_repository) {
        $this->review_repository = $review_repository;
        $this->main_review_repository = $main_review_repository;
    }

    public function run($ids, $toggle) {
        try {
            $reviews = $this->review_repository->findWhereIn('review_no', $ids);

            $mains = $this->main_review_repository->scopeQuery(function ($query) use ($ids){
                $query = $query
                    ->where('cont_type', Constant::$POPULAR_CONT_TYPE_CODE)
                    ->where('cont_std', Constant::$POPULAR_CONT_STD_CODE)
                    ->orderBy('cont_ord', 'desc');

                return $query;
            })->all();

            $last_item = $mains->first();

            $last_order = 1;
            if($last_item) {
                $last_order = $last_item->cont_ord + 1;
            }

            $mains = $mains->keyBy('ref_no');

            foreach ($reviews as $review) {
                if($toggle == true) {
                    if(!isset($mains[$review->review_no])) {
                        $this->main_review_repository->create([
                            'ref_no' => $review->review_no,
                            'cont_type' => Constant::$POPULAR_CONT_TYPE_CODE,
                            'cont_std' => Constant::$POPULAR_CONT_STD_CODE,
                            'cont_ord' => $last_order,
                            'cont_kind' => '502001'
                        ]);
                    }
                } else {
                    $this->main_review_repository->deleteWhere([
                        'ref_no' => $review->review_no,
                        'cont_type' => Constant::$POPULAR_CONT_TYPE_CODE,
                        'cont_std' => Constant::$POPULAR_CONT_STD_CODE,
                    ]);
                }
            }

            return true;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
