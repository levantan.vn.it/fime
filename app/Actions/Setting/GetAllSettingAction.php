<?php

namespace App\Actions\Setting;
use App\Actions\Action;
use App\Contracts\SettingRepositoryInterface;

class GetAllSettingAction extends Action {
    protected $setting_repository;

    public function __construct(SettingRepositoryInterface $setting_repository) {
        $this->setting_repository = $setting_repository;
    }

    public function run() {
        $settings = $this->setting_repository->all();
       return $settings;
    }
}
