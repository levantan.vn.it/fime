<?php

namespace App\Actions\SystemNotification;
use App\Actions\Action;
use App\Contracts\SystemNotificationRepositoryInterface;
use Mockery\Exception;

class UpdateSystemNotificationAction extends Action {
    protected $system_notification_repository;

    public function __construct(SystemNotificationRepositoryInterface $system_notification_repository) {
        $this->system_notification_repository = $system_notification_repository;
    }

    public function run($data) {
        try {
            $notification = $this->system_notification_repository->update([
                'title' => $data['title'],
                'content' => $data['content'],
                'url' => $data['url']
            ], $data['id']);
            return $notification;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
