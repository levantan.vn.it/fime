<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 4:37 PM
 */

namespace App\Actions\Tip;


use App\Actions\Action;
use App\Contracts\TipsRepositoryInterface;

class GetAllTipsAction extends Action {
    protected $tipsRepository;

    public function __construct(TipsRepositoryInterface $tipsRepository) {
        $this->tipsRepository = $tipsRepository;
    }

    public function run() {
        $tips = $this->tipsRepository->orderBy('ti_id', 'desc')->findByField('display_yn', 'Y');
        return $tips;
    }
}
