<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 4:43 PM
 */

namespace App\Actions\Tip;


use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TipsMappingRepositoryInterface;
use App\Contracts\TipsRepositoryInterface;
use App\Contracts\TipViewsRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\Comment\GetCommentNumberByTryIdsCriteria;
use App\Criterias\Comment\GetNumberOfCommentsByReviewIdsCriteria;
use App\Criterias\Files\GetReviewFilesCriteria;
use App\Criterias\Review\GetReviewsByIdsCriteria;
use App\Criterias\TryFree\GetFilesByListIdCriteria;
use App\Criterias\TryFree\GetTriesByIdsCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetLikeNumberOfTryByListIdCriteria;
use App\Criterias\UserLike\GetLikesOfCurrentUserCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByReviewIdsCriteria;
use App\Criterias\UserLike\GetReviewLikeInfoCriteria;
use App\Criterias\UserLike\GetReviewLikesByUserIdCriteria;
use App\Criterias\UserLike\GetUserLikeInfoCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;
use App\Criterias\UserTry\GetApplyOfCurrentUserCriteria;
use App\Criterias\UserTry\GetNumberOfTriesByTryIdsCriteria;

class GetByIDAction extends Action
{
    protected $tipsRepository;
    protected $tipViewsRepository;
    protected $tipsMappingRepository;
    protected $tryRepository;
    protected $user_like_repository;
    protected $comment_repository;
    protected $user_repository;
    protected $user_try_repository;
    protected $userFollowRepository;
    protected $codeRepository;
    protected $review_repository;
    protected $review_files_repository;
    protected $reviewCommentRepository;
    protected $reviewLikeRepository;

    public function __construct(
        TipsRepositoryInterface $tipsRepository,
        TipsMappingRepositoryInterface $tipsMappingRepository,
        TipViewsRepositoryInterface $tipViewsRepository,
        TryRepositoryInterface $tryRepository,
        CommentRepositoryInterface $comment_repository,
        UserLikeRepositoryInterface $user_like_repository,
        UserTryRepositoryInterface $user_try_repository,
        UserRepositoryInterface $user_repository,
        UserFollowRepositoryInterface $userFollowRepository,
        CodeRepositoryInterface $codeRepository,
        ReviewRepositoryInterface $review_repository,
        ReviewFilesRepositoryInterface $review_files_repository,
        ReviewCommentRepositoryInterface $reviewCommentRepository,
        ReviewLikeRepositoryInterface $reviewLikeRepository
    )
    {
        $this->tipsRepository = $tipsRepository;
        $this->tipViewsRepository = $tipViewsRepository;
        $this->tipsMappingRepository = $tipsMappingRepository;
        $this->tryRepository = $tryRepository;
        $this->user_like_repository = $user_like_repository;
        $this->user_repository = $user_repository;
        $this->comment_repository = $comment_repository;
        $this->user_try_repository = $user_try_repository;
        $this->userFollowRepository = $userFollowRepository;
        $this->codeRepository = $codeRepository;
        $this->review_repository = $review_repository;
        $this->review_files_repository = $review_files_repository;
        $this->reviewCommentRepository = $reviewCommentRepository;
        $this->reviewLikeRepository = $reviewLikeRepository;
    }

    public function run($id)
    {
        $tips = $this->tipsRepository->find($id);
        $tipViews = $this->tipViewsRepository->findByField('ti_id', $id)->toArray();

        $views = 0;
        if (count($tipViews)) {
            $views = (int)$tipViews[0]['views'] + 1;
            $this->tipViewsRepository->update([
                'views' => $views
            ], $id);
        } else {
            $views = 1;
            $this->tipViewsRepository->create([
                'ti_id' => $id,
                'views' => $views
            ]);
        }

        $tipsMapping = $this->tipsMappingRepository->findByField('ti_id', $id)->keyBy('content_id');

        $tryIds = [];
        $reviewIds = [];


        $fimerIds = [];

        if ($tips->hot_fimer1 && $tips->hot_fimer1 != '')
            $fimerIds[] = $tips->hot_fimer1;
        if ($tips->hot_fimer2 && $tips->hot_fimer2 != '')
            $fimerIds[] = $tips->hot_fimer2;
        if ($tips->hot_fimer3 && $tips->hot_fimer3 != '')
            $fimerIds[] = $tips->hot_fimer3;
        if ($tips->hot_fimer4 && $tips->hot_fimer4 != '')
            $fimerIds[] = $tips->hot_fimer4;
        if ($tips->hot_fimer5 && $tips->hot_fimer5 != '')
            $fimerIds[] = $tips->hot_fimer5;
        if ($tips->hot_fimer6 && $tips->hot_fimer6 != '')
            $fimerIds[] = $tips->hot_fimer6;

        foreach ($tipsMapping as $item) {
            if ($item->content_gb === 'T')
                array_push($tryIds, $item->content_id);
            if ($item->content_gb === 'R')
                array_push($reviewIds, $item->content_id);
        }


        $tries = $this->getTries($tryIds);
        $reviews = $this->getReviews($reviewIds);
        $hotFimers = $this->getHotFimers($fimerIds);


        $tips->views = $views;
        $tips->tries = $tries;
        $tips->reviews = $reviews;
        $tips->hotFimers = $hotFimers;
        return $tips;
    }

    public function getTries($tryIds)
    {
        $tries = $this->tryRepository->getByCriteria(new GetTriesByIdsCriteria($tryIds));
        $this->decorateTriesData($tries);
        return $tries;
    }

    public function decorateTriesData($tries)
    {
        $this->getTriesTotalApply($tries);
        $this->getNumberLikesOfTry($tries);
        $this->getNumberCommentsOfTry($tries);
        $this->getTriesListUserLike($tries);
        $this->checkCurrentUserLikedTries($tries);
        $this->checkJoinTry($tries);
        $this->getFilesTries($tries);
    }

    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->userFollowRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');


        $followers = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->id])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }

    protected function getFilesTries($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $files = $this->tryRepository->getByCriteria(new GetFilesByListIdCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->file = [];
            if (isset($files[$try->cntnts_no])) {
                $try->file = $files[$try->cntnts_no];
            }
        }

        return $tries;
    }

    protected function getTriesTotalApply($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $number_of_tries = $this->user_try_repository->getByCriteria(new GetNumberOfTriesByTryIdsCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->total_apply = 0;
            if (isset($number_of_tries[$try->cntnts_no])) {
                $try->total_apply = $number_of_tries[$try->cntnts_no]->number_of_tries;
            }
        }

    }

    protected function getNumberLikesOfTry($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        $list_try_likes = $this->user_like_repository->getByCriteria(new GetLikeNumberOfTryByListIdCriteria($try_ids));

        $list_try_likes = collect($list_try_likes)->keyBy('cntnts_no');

        foreach ($tries as $index => $try) {
            if (isset($list_try_likes[$try->cntnts_no])) {
                $tries[$index]->likes = $list_try_likes[$try->cntnts_no]->like_number;
            } else {
                $tries[$index]->likes = 0;
            }
        }

        return $tries;
    }

    protected function getNumberCommentsOfTry($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        $list_try_comments = $this->comment_repository->getByCriteria(new GetCommentNumberByTryIdsCriteria($try_ids));

        $list_try_comments = collect($list_try_comments)->keyBy('cntnts_no');

        foreach ($tries as $index => $try) {
            if (isset($list_try_comments[$try->cntnts_no])) {
                $tries[$index]->comments = $list_try_comments[$try->cntnts_no]->comment_number;
            } else {
                $tries[$index]->comments = 0;
            }
        }

        return $tries;
    }

    protected function getTriesListUserLike($tries)
    {
        foreach ($tries as $index => $try) {
            $list_user_liked = $this->user_like_repository->getByCriteria(new GetUserLikeInfoCriteria($try->cntnts_no));
            $fimers = $this->getCurrentUserFollowing($list_user_liked);
            $tries[$index]->users_liked = $fimers;
        }

        return $tries;
    }

    protected function checkCurrentUserLikedTries($tries)
    {
        $current_user_id = auth()->id();
        $list_try_id = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id)
            return null;

        $list_user_liked = $this->user_like_repository->getByCriteria(new GetLikesOfCurrentUserCriteria($list_try_id, $current_user_id));
        $list_user_liked = $list_user_liked->keyBy('cntnts_no');

        foreach ($tries as $item) {
            if (isset($list_user_liked[$item->cntnts_no])) {
                $item->is_liked = 1;
            } else {
                $item->is_liked = 0;
            }
        }
        return $tries;
    }

    protected function checkJoinTry($tries)
    {
        $current_user_id = auth()->id();
        $list_try_id = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id)
            return null;

        $list_user_apply = $this->user_try_repository->getByCriteria(new GetApplyOfCurrentUserCriteria($list_try_id, $current_user_id));
        $list_user_apply = $list_user_apply->keyBy('cntnts_no');

        foreach ($tries as $index => $item) {
            if (isset($list_user_apply[$item->cntnts_no])) {
                $tries[$index]->is_joined = 1;
            } else {
                $tries[$index]->is_joined = 0;
            }
        }
        return $tries;
    }

    public function getReviews($reviewIds)
    {
        $reviews = $this->review_repository->getByCriteria(new GetReviewsByIdsCriteria($reviewIds));
        $this->decorateReviewsData($reviews);
        return $reviews;
    }

    protected function decorateReviewsData($reviews)
    {
        $this->getFollowInfo($reviews);
        $this->checkCurrentUserFollowingReviewAuthor($reviews);
        $this->getLikesNumber($reviews);
        $this->checkCurrentUserLikedReview($reviews);
        $this->getCommentNumberOfReview($reviews);
        $this->getListUserLike($reviews);
        $this->getFiles($reviews);
        return $reviews;
    }

    protected function getListUserLike($reviews)
    {
        try {
            foreach ($reviews as $review) {
                $list_user_liked = $this->reviewLikeRepository->getByCriteria(new GetReviewLikeInfoCriteria($review->review_no));
                $fimers = $this->getCurrentUserFollowingReview($list_user_liked);
                $review->users_liked = $fimers;
            }

            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    protected function getCurrentUserFollowingReview($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->userFollowRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');


        $followers = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->id])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getFollowInfo($reviews)
    {
        try {
            $user_ids = $reviews->pluck('user_no')->toArray();

            $list_number_follow = $this->userFollowRepository->getByCriteria(new GetNumberOfFollowersByUserIdsCriteria($user_ids));

            $list_number_follow = collect($list_number_follow)->keyBy('user_id');

            foreach ($reviews as $index => $review) {
                if (isset($list_number_follow[$review->user_no])) {
                    $review->author_follows = $list_number_follow[$review->user_no]->number_of_followers;
                } else {
                    $review->author_follows = 0;
                }
            }

            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function checkCurrentUserFollowingReviewAuthor($reviews)
    {
        $current_user_id = auth()->id();

        $user_ids = $reviews->pluck('user_no')->toArray();

        $user_followings = $this->userFollowRepository->getByCriteria(new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        foreach ($reviews as $index => $review) {
            $review->followed = 0;
            if (isset($user_followings[$review->user_no])) {
                $review->followed = 1;
            }
        }
        return $reviews;
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getLikesNumber($reviews)
    {
        try {
            $review_ids = $reviews->pluck('review_no')->toArray();

            $list_like_number = $this->reviewLikeRepository->getByCriteria(new GetNumberOfLikesByReviewIdsCriteria($review_ids));

            $list_number_follow = collect($list_like_number)->keyBy('object_id');

            foreach ($reviews as $review) {
                if (isset($list_number_follow[$review->review_no])) {
                    $review->like_number = $list_number_follow[$review->review_no]->like_number;
                } else {
                    $review->like_number = 0;
                }
            }
            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function checkCurrentUserLikedReview($reviews)
    {
        $current_user_id = auth()->id();

        $review_ids = $reviews->pluck('review_no')->toArray();

        $list_user_liked = $this->reviewLikeRepository->getByCriteria(new GetReviewLikesByUserIdCriteria($review_ids, $current_user_id));

        $list_user_liked = $list_user_liked->keyBy('review_no');

        foreach ($reviews as $review) {
            if (isset($list_user_liked[$review->review_no])) {
                $review->is_liked = 1;
            } else {
                $review->is_liked = 0;
            }
        }

        return $reviews;
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getCommentNumberOfReview($reviews)
    {
        try {
            $review_ids = $reviews->pluck('review_no')->toArray();

            $list_comment_number = $this->reviewCommentRepository->getByCriteria(new GetNumberOfCommentsByReviewIdsCriteria($review_ids));

            $list_comment_number = collect($list_comment_number)->keyBy('review_no');

            foreach ($reviews as $review) {
                if (isset($list_comment_number[$review->review_no])) {
                    $review->comment_number = $list_comment_number[$review->review_no]->comment_number;
                } else {
                    $review->comment_number = 0;
                }
            }
            return $reviews;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $reviews
     * @return mixed
     */
    protected function getFiles($reviews)
    {
        try {
            foreach ($reviews as $review) {
                $files = $this->review_files_repository->getByCriteria(new GetReviewFilesCriteria($review->review_no));
                $review->files = $files;
            }
            return $reviews;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getHotFimers($fimerIds)
    {
        $users = $this->user_repository->findWhereIn('user_no', $fimerIds);

        $this->getCurrentUserFollowingFimers($users);

        return $users;
    }

    protected function getCurrentUserFollowingFimers($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->userFollowRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        $followers = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->user_no])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }

        return $fimers;
    }

}
