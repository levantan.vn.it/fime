<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/5/2019
 * Time: 9:28 AM
 */

namespace App\Actions\Tip;


use App\Actions\Action;
use App\Contracts\TipsRepositoryInterface;
use App\Contracts\TipViewsRepositoryInterface;

class GetTopViewAction extends Action
{
    protected $tipsRepository;
    protected $tipViewsRepository;

    public function __construct(
        TipsRepositoryInterface $tipsRepository,
        TipViewsRepositoryInterface $tipViewsRepository
    )
    {
        $this->tipViewsRepository = $tipViewsRepository;
        $this->tipsRepository = $tipsRepository;
    }

    public function run()
    {
        $tips = $this->tipsRepository->scopeQuery(function ($query) {
            $query = $query->join('tip_views', 'tip_views.ti_id', '=', 'TCT_TIPS.ti_id');
            $query = $query->where('display_yn', 'Y');
            $query = $query->where('display_yn', 'Y');
            $query = $query->where('display_yn', 'Y');
            $query = $query->orderBy('tip_views.views', 'desc');
            return $query->take(5);
        })->all();
        return $tips;
    }
}
