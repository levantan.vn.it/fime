<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\TryExtRepositoryInterface;
use App\Contracts\TryFilesRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\Comment\GetCommentNumberByTryIdsCriteria;
use App\Criterias\TryFree\GetFilesOfTryCriteria;
use App\Criterias\TryFree\GetImageDescOfTryCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetLikeNumberOfTryByListIdCriteria;
use App\Criterias\UserLike\GetUserLikeInfoCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;
use App\Criterias\UserTry\GetApplyOfCurrentUserCriteria;
use App\Criterias\UserTry\GetNumberOfTriesByTryIdsCriteria;
use App\Criterias\UserTry\GetWinnersByTryIdCriteria;
use Mockery\Exception;

class GetBySlugAction extends Action {
    protected $repository;
    protected $user_like_repository;
    protected $user_try_repository;
    protected $comment_repository;
    protected $follow_repository;
    protected $ext_repository;
    protected $try_files_repository;
    protected   $codeRepository;

    public function __construct(TryRepositoryInterface $repository,
                                TryExtRepositoryInterface $ext_repository,
                                CommentRepositoryInterface $comment_repository,
                                UserFollowRepositoryInterface $follow_repository,
                                UserTryRepositoryInterface $user_try_repository,
                                UserLikeRepositoryInterface $user_like_repository,
                                TryFilesRepositoryInterface $try_files_repository,
                                CodeRepositoryInterface $codeRepository
    ) {
        $this->repository = $repository;
        $this->ext_repository = $ext_repository;
        $this->user_like_repository = $user_like_repository;
        $this->comment_repository = $comment_repository;
        $this->user_try_repository = $user_try_repository;
        $this->follow_repository = $follow_repository;
        $this->try_files_repository = $try_files_repository;
        $this->codeRepository = $codeRepository;
    }

    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->follow_repository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        $followers = $this->follow_repository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->follow_repository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->id])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }

    public function run($slug) {
        try {
            $try_ext = $this->ext_repository->findWhere(['slug' => $slug, 'expsr_at' => 'Y', 'delete_at' => 'N'])->first();
            if($try_ext) {
                $try = $this->repository->find($try_ext->cntnts_no);

                if ($try_ext == null || $try == null)
                    return null;

                $try = $this->repository->find($try_ext->cntnts_no);
                $try->files = $this->repository->getByCriteria(new GetFilesOfTryCriteria($try_ext->cntnts_no));
                $try->imgDesc = $this->repository->getByCriteria(new GetImageDescOfTryCriteria($try_ext->cntnts_no));

                $try->cntnts_nm = $try_ext->sj;

                $this->getListUserLike($try);
                $this->getNumberCommentsOfTry($try);
                $this->getTotalApply($try);
                $this->getWinner($try);
                return $try;
            }
            return null;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    protected function getTotalApply($try){
        $number_of_tries = $this->user_try_repository->getByCriteria(new GetNumberOfTriesByTryIdsCriteria([$try->cntnts_no]))->first();
        $try->total_apply = 0;

        if($number_of_tries != null) {
            $try->total_apply = $number_of_tries->number_of_tries;
        }
    }

    protected function getWinner($try){
        $winners = $this->user_try_repository->getByCriteria(new GetWinnersByTryIdCriteria($try->cntnts_no));
        $try->is_winner = 0;
        $try->delivery_info = ['dlvy_dt' => null];
        if (count($winners) == 0) {
            $try->delivery_info = ['dlvy_dt' => null];
        } else {
            foreach ($winners as $winner) {
                if ($winner->user_no == auth()->id()) {
                    $try->is_winner = 1;
                    $company_name = $this->codeRepository->findWhere(['code' => $winner->dlvy_cmpny_code]);
                    if (count($company_name) > 0) {
                        $winner->company_name = $company_name[0]->code_nm;
                    }
                    $status = $this->codeRepository->findWhere(['code' => $winner->dlvy_mth_code]);
                    if (count($status) > 0) {
                        $winner->status = $status[0]->code_nm;
                    }
                    $try->delivery_info = $winner;
                    break;
                }
            }
        }
        $this->getFollowInfo($winners);
        $this->checkCurrentUserFollowingReviewAuthor($winners);
        $try->winners = $winners;
    }

    protected function getFollowInfo($winners) {
        try {
            $user_ids = $winners->pluck('user_no')->toArray();

            $number_follows = $this->follow_repository->getByCriteria(new GetNumberOfFollowersByUserIdsCriteria($user_ids));

            $number_follows = collect($number_follows)->keyBy('user_id');

            foreach ($winners as $winner) {
                if (isset($number_follows[$winner->user_no])) {
                    $winner->follows = $number_follows[$winner->user_no]->number_of_followers;
                } else {
                    $winner->follows = 0;
                }
            }

            return $winners;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    protected function checkCurrentUserFollowingReviewAuthor($winners) {
        $current_user_id = auth()->id();

        $user_ids = $winners->pluck('user_no')->toArray();

        $user_followings = $this->follow_repository->getByCriteria(new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        foreach ($winners as $winner) {
            $winner->followed = 0;
            if(isset($user_followings[$winner->user_no])) {
                $winner->followed = 1;
            }
        }
        return $winners;
    }

    protected function getListUserLike($try) {
        $current_user_id = auth()->id();

        $list_user_liked = $this->user_like_repository->getByCriteria(new GetUserLikeInfoCriteria($try->cntnts_no));
        $fimers = $this->getCurrentUserFollowing($list_user_liked);
        $try->is_liked = false;

        if($current_user_id != null) {
            $is_liked = $this->user_like_repository->findWhere([
                ['cntnts_no', '=', $try->cntnts_no],
                ['user_no', '=', $current_user_id]
            ])->first();

            if($is_liked) {
                $try->is_liked = true;
            }
        }

        $try->users_liked = $fimers;

        $list_try_likes = $this->user_like_repository->getByCriteria(new GetLikeNumberOfTryByListIdCriteria([$try->cntnts_no]));
        $try->likes = 0;
        if(count($list_try_likes) > 0) {
            $try->likes = $list_try_likes[0]->like_number;
        }

        $try->is_joined = false;
        $list_user_apply = $this->user_try_repository->getByCriteria(new GetApplyOfCurrentUserCriteria([$try->cntnts_no], $current_user_id));
        if(count($list_user_apply) > 0) {
            $try->is_joined = true;
        }
    }

    protected function getNumberCommentsOfTry($try) {
        $list_try_comments = $this->comment_repository->getByCriteria(new GetCommentNumberByTryIdsCriteria([$try->cntnts_no]));

        $comments = 0;
        if(count($list_try_comments) > 0) {
            $comments = $list_try_comments[0]->comment_number;
        }

        $try->comments = $comments;
    }
}
