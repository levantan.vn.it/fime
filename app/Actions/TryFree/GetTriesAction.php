<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\Comment\GetCommentNumberByTryIdsCriteria;
use App\Criterias\TryFree\GetFilesByListIdCriteria;
use App\Criterias\TryFree\GetTriesCriteria;
use App\Criterias\TryFree\GetWinnersInfoCriteria;
use App\Criterias\UserLike\GetLikeNumberOfTryByListIdCriteria;
use App\Criterias\UserLike\GetLikesOfCurrentUserCriteria;
use App\Criterias\UserLike\GetUserLikeInfoCriteria;
use App\Criterias\UserTry\GetApplyOfCurrentUserCriteria;
use App\Criterias\UserTry\GetNumberOfTriesByTryIdsCriteria;
use Carbon\Carbon;

class GetTriesAction extends Action
{
    protected $repository;
    private $user_like_repository;
    private $comment_repository;
    private $user_repository;
    protected $user_try_repository;
    protected $followRepository;

    public function __construct(TryRepositoryInterface $repository,
                                CommentRepositoryInterface $comment_repository,
                                UserLikeRepositoryInterface $user_like_repository,
                                UserTryRepositoryInterface $user_try_repository,
                                UserRepositoryInterface $user_repository,
                                UserFollowRepositoryInterface $followRepository)
    {
        $this->repository = $repository;
        $this->user_like_repository = $user_like_repository;
        $this->user_repository = $user_repository;
        $this->comment_repository = $comment_repository;
        $this->user_try_repository = $user_try_repository;
        $this->followRepository = $followRepository;
    }

    public function run($params)
    {
        $current_user_id = auth()->id();

        if ($current_user_id) {
            $this->user_repository->update([
                'try_click_dt' => Carbon::now()
            ], $current_user_id);
        }

        $period = isset($params['period']) ? $params['period'] : 0;
        $tries = $this->repository->getByCriteria(new GetTriesCriteria($period));

        $this->decorateData($tries);

        return $tries;
    }

    public function decorateData($tries)
    {
        $this->getTotalApply($tries);
        $this->getNumberLikesOfTry($tries);
        $this->getNumberCommentsOfTry($tries);
        $this->getListUserLike($tries);
        $this->checkCurrentUserLiked($tries);
        $this->checkJoinTry($tries);
        $this->checkWinner($tries);
        $this->getFiles($tries);
    }

    protected function getFiles($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $files = $this->repository->getByCriteria(new GetFilesByListIdCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->file = [];
            if (isset($files[$try->cntnts_no])) {
                $try->file = $files[$try->cntnts_no];
            }
        }

        return $tries;
    }

    protected function getTotalApply($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();
        $number_of_tries = $this->user_try_repository->getByCriteria(new GetNumberOfTriesByTryIdsCriteria($try_ids))->keyBy('cntnts_no');

        foreach ($tries as $try) {
            $try->total_apply = 0;
            if (isset($number_of_tries[$try->cntnts_no])) {
                $try->total_apply = $number_of_tries[$try->cntnts_no]->number_of_tries;
            }
        }
        return $tries;
    }

    protected function getNumberLikesOfTry($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        $list_try_likes = $this->user_like_repository->getByCriteria(new GetLikeNumberOfTryByListIdCriteria($try_ids));

        $list_try_likes = collect($list_try_likes)->keyBy('cntnts_no');

        foreach ($tries as $index => $try) {
            if (isset($list_try_likes[$try->cntnts_no])) {
                $tries[$index]->likes = $list_try_likes[$try->cntnts_no]->like_number;
            } else {
                $tries[$index]->likes = 0;
            }
        }

        return $tries;
    }

    protected function getNumberCommentsOfTry($tries)
    {
        $try_ids = $tries->pluck('cntnts_no')->toArray();

        $list_try_comments = $this->comment_repository->getByCriteria(new GetCommentNumberByTryIdsCriteria($try_ids));

        $list_try_comments = collect($list_try_comments)->keyBy('cntnts_no');

        foreach ($tries as $index => $try) {
            if (isset($list_try_comments[$try->cntnts_no])) {
                $tries[$index]->comments = $list_try_comments[$try->cntnts_no]->comment_number;
            } else {
                $tries[$index]->comments = 0;
            }
        }

        return $tries;
    }

    protected function getListUserLike($tries)
    {
        foreach ($tries as $index => $try) {
            $list_user_liked = $this->user_like_repository->getByCriteria(new GetUserLikeInfoCriteria($try->cntnts_no));
//            $fimers = $this->getCurrentUserFollowing($list_user_liked);
            $tries[$index]->users_liked = $list_user_liked;
        }

        return $tries;
    }

    protected function checkCurrentUserLiked($tries)
    {
        $current_user_id = auth()->id();
        $list_try_id = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id)
            return null;

        $list_user_liked = $this->user_like_repository->getByCriteria(new GetLikesOfCurrentUserCriteria($list_try_id, $current_user_id));
        $list_user_liked = $list_user_liked->keyBy('cntnts_no');

        foreach ($tries as $item) {
            if (isset($list_user_liked[$item->cntnts_no])) {
                $item->is_liked = 1;
            } else {
                $item->is_liked = 0;
            }
        }
        return $tries;
    }

    protected function checkJoinTry($tries)
    {
        $current_user_id = auth()->id();
        $list_try_id = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id)
            return null;

        $list_user_apply = $this->user_try_repository->getByCriteria(new GetApplyOfCurrentUserCriteria($list_try_id, $current_user_id));
        $list_user_apply = $list_user_apply->keyBy('cntnts_no');

        foreach ($tries as $index => $item) {
            if (isset($list_user_apply[$item->cntnts_no])) {
                $tries[$index]->is_joined = 1;
            } else {
                $tries[$index]->is_joined = 0;
            }
        }
        return $tries;
    }

    protected function checkWinner($tries)
    {
        $current_user_id = auth()->id();
        $list_try_id = $tries->pluck('cntnts_no')->toArray();

        if (!$current_user_id) {
            foreach ($tries as $index => $item) {
                $tries[$index]->slctn_dt = null;
                $tries[$index]->dlvy_dt = null;
            }
            return null;
        }

        $list_winners = $this->user_try_repository->getByCriteria(new GetWinnersInfoCriteria($list_try_id, $current_user_id));
        $list_winners = $list_winners->keyBy('cntnts_no');

        foreach ($tries as $index => $item) {
            if (isset($list_winners[$item->cntnts_no])) {
                $tries[$index]->slctn_dt = $list_winners[$item->cntnts_no]->slctn_dt;
                $tries[$index]->dlvy_dt = $list_winners[$item->cntnts_no]->dlvy_dt;
            } else {
                $tries[$index]->slctn_dt = null;
                $tries[$index]->dlvy_dt = null;
            }
        }
        return $tries;
    }
}
