<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\TryFree;

use App\Actions\Action;
use App\Actions\Constant;
use App\Contracts\TryExtRepositoryInterface;
use App\Contracts\TryFilesRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Criterias\TryFree\GetImageDescOfTryCriteria;
use App\Criterias\TryFree\GetTryByIdCriteria;
use Carbon\Carbon;
use Mockery\Exception;

class UpdateTryAction extends Action
{
    protected $repository;
    protected $try_ext_repository;
    protected $try_files_repository;

    public function __construct(TryRepositoryInterface $repository,
                                TryExtRepositoryInterface $try_ext_repository,
                                TryFilesRepositoryInterface $try_files_repository
    )
    {
        $this->repository = $repository;
        $this->try_ext_repository = $try_ext_repository;
        $this->try_files_repository = $try_files_repository;
    }

    public function run($data)
    {
        try {
            $query = [
                'goods_cl_code' => isset($data['goods_cl_code']) ? $data['goods_cl_code'] : null,
                'time_color_code' => isset($data['time_color_code']) ? $data['time_color_code'] : null,
                'brnd_code' => isset($data['brnd_code']) ? $data['brnd_code'] : null,
                'modl_nombr' => isset($data['modl_nombr']) ? $data['modl_nombr'] : null,
                'link_url' => isset($data['link_url']) ? $data['link_url'] : null,
                'event_knd_code' => isset($data['event_knd_code']) ? $data['event_knd_code'] : null,
                'goods_pc' => isset($data['goods_pc']) ? $data['goods_pc'] : null,
                'event_pc' => isset($data['event_pc']) ? $data['event_pc'] : null,
                'event_trgter_co' => isset($data['event_trgter_co']) ? $data['event_trgter_co'] : null,
                'resource_type' => isset($data['resource_type']) ? $data['resource_type'] : null,
                'goods_dc' => isset($data['goods_dc']) ? $data['goods_dc'] : null,
                'short_desc' => isset($data['short_desc']) ? $data['short_desc'] : null,
                'event_bgnde' => isset($data['event_bgnde']) ? Carbon::parse($data['event_bgnde']) : null,
                'event_endde' => isset($data['event_endde']) ? Carbon::parse($data['event_endde']) : null,
                'dlvy_bgnde' => isset($data['dlvy_bgnde_format']) ? Carbon::parse($data['dlvy_bgnde_format'])->format('Ymd') : null,
                'dlvy_endde' => isset($data['dlvy_endde_format']) ? Carbon::parse($data['dlvy_endde_format'])->format('Ymd') : null,
                'is_try_event' => isset($data['is_try_event']) ? (int)$data['is_try_event'] : 0,
                'quantity_to_qualify' => isset($data['quantity_to_qualify']) ? $data['quantity_to_qualify'] : 0,
                'try_event_type' => isset($data['try_event_type']) ? $data['try_event_type'] : null,
                'hash_tag' => isset($data['hash_tag']) ? $data['hash_tag'] : null,
                'goods_txt' => isset($data['goods_txt']) ? $data['goods_txt'] : null,
            ];

            $result = $this->repository->update($query, $data['cntnts_no']);

            $this->try_ext_repository->update([
                'sj' => isset($data['cntnts_nm']) ? $data['cntnts_nm'] : null,
                'expsr_at' => isset($data['expsr_at']) ? $data['expsr_at'] : null,
            ], $data['cntnts_no']);

            if (!empty($data['images'])) {
                $this->try_files_repository->deleteWhere([
                    'cntnts_no' => $data['cntnts_no']
                ]);
                foreach ($data['images'] as $index => $image) {
                    $se_code = Constant::$MAIN_SE_CODE;
                    if ($index != 0) {
                        $se_code = Constant::$OTHER_SE_CODE;
                    }
                    $this->try_files_repository->create(
                        [
                            'cntnts_no' => $data['cntnts_no'],
                            'file_se_code' => Constant::$FILE_SE_CODE,
                            'file_sn' => $index + 1,
                            'orginl_file_nm' => $image['name'],
                            'stre_file_nm' => $image['name'],
                            'file_cours' => $image['url'],
                            'se_code' => $se_code,
                        ]);
                }

                if (!empty($data['img_desc'])){
                    foreach ($data['img_desc'] as $index => $image) {
                        $this->try_files_repository->create(
                            [
                                'cntnts_no' => $data['cntnts_no'],
                                'file_se_code' => Constant::$DESC_SE_CODE,
                                'file_sn' => $index,
                                'orginl_file_nm' => $image['name'],
                                'stre_file_nm' => $image['name'],
                                'file_cours' => $image['url'],
                                'se_code' => Constant::$DESC_SE_CODE,
                            ]);
                    }
                }
            } else {
                $this->try_files_repository->deleteWhere([
                    'cntnts_no' => $data['cntnts_no']
                ]);
            }



            $images = $this->repository->getByCriteria(new GetTryByIdCriteria($result->cntnts_no));
            $imgDesc = $this->repository->getByCriteria(new GetImageDescOfTryCriteria($result->cntnts_no));
            $result->images = $images;
            $result->imgDesc = $imgDesc;

            return $result;
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
