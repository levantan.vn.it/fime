<?php

namespace App\Actions\UserFollow;

use App\Actions\Notification\CreateNotificationAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use Carbon\Carbon;

class ToggleFollowUserAction extends CreateNotificationAction
{
    protected $user_follow_repository;
    protected $user_repository;
    protected $notification_repository;

    public function __construct(UserFollowRepositoryInterface $user_follow_repository,
                                UserRepositoryInterface $user_repository,
                                NotificationRepositoryInterface $notification_repository,
                                ReviewRepositoryInterface $review_repository,
                                TryRepositoryInterface $try_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                CodeRepositoryInterface $codeRepository
)
    {
        $this->user_follow_repository = $user_follow_repository;
        $this->user_repository = $user_repository;
        $this->notification_repository = $notification_repository;
        parent::__construct($notification_repository, $review_repository, $try_repository, $user_point_repository, $codeRepository);
    }

    /**
     * @param $request
     * @return array|bool
     */
    public function run($request)
    {
        try {
            $data = [];
            $data['fllwr_user_no'] = $request['followed_user_id'];
            $data['user_no'] = auth()->id();

            if ($data['fllwr_user_no'] == $data['user_no']) {
                return false;
            }

            $follows = $this->user_follow_repository->findWhere([
                'fllwr_user_no' => $data['user_no'],
                'user_no' => $data['fllwr_user_no'],
            ]);

            if (count($follows) > 0) {
                $this->user_follow_repository->deleteWhere([
                    'fllwr_user_no' => $data['user_no'],
                    'user_no' => $data['fllwr_user_no'],
                ]);

                $followed = false;
                // Disable notification
                $this->createFollowNotification($data['fllwr_user_no'], false, true);
            } else {
                $this->user_follow_repository->create([
                    'fllwr_user_no' => $data['user_no'],
                    'regist_dt' => Carbon::now(),
                    'user_no' => $data['fllwr_user_no'],
                ]);

                $followed = true;
                // Enable notification
                $this->createFollowNotification($data['fllwr_user_no'], false, false);
            }


            $follows = $this->user_follow_repository->getByCriteria(new GetNumberOfFollowersByUserIdsCriteria([$data['fllwr_user_no']]));

            $total = 0;
            if (count($follows) > 0) {
                $total = $follows[0]->number_of_followers;
            }

            return ['total' => $total, 'followed' => $followed];

        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
