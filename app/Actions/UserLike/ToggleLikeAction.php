<?php

namespace App\Actions\UserLike;
use App\Actions\Notification\CreateNotificationAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\UserLike\GetLikeNumberOfTryByListIdCriteria;
use App\Criterias\UserLike\GetNumberOfLikesByReviewIdsCriteria;
use App\Criterias\UserLike\GetReviewLikeInfoCriteria;
use App\Criterias\UserLike\GetUserLikeInfoCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;
use Carbon\Carbon;

class ToggleLikeAction extends CreateNotificationAction {
    protected $user_like_repository;
    protected $review_like_repository;
    protected $userFollowRepository;

    public function __construct(UserLikeRepositoryInterface $user_like_repository,
                                ReviewLikeRepositoryInterface $review_like_repository,
                                NotificationRepositoryInterface $notification_repository,
                                ReviewRepositoryInterface $review_repository,
                                TryRepositoryInterface $try_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                UserFollowRepositoryInterface $userFollowRepository,
                                CodeRepositoryInterface $codeRepository) {
        $this->user_like_repository = $user_like_repository;
        $this->review_like_repository = $review_like_repository;
        $this->userFollowRepository = $userFollowRepository;
        parent::__construct($notification_repository, $review_repository, $try_repository, $user_point_repository, $codeRepository);
    }

    public function run($request) {
        $data = [];
        $data['object_id'] = $request['object_id'];
        $data['object_type'] = $request['object_type'];

        $user_id = auth()->id();

        if($data['object_type'] == 'try') {
            $likes = $this->user_like_repository->findWhere([
                'cntnts_no' => $data['object_id'],
                'user_no' => $user_id
            ]);

            if (count($likes) > 0) {
                $this->user_like_repository->deleteWhere([
                    'cntnts_no' => $data['object_id'],
                    'user_no' => $user_id
                ]);

                // Disable notification
                $this->createTryAppliedNotification($data['object_id'], 1);
                $is_liking = false;
            } else {
                $this->user_like_repository->create([
                    'cntnts_no' => $data['object_id'],
                    'recomend_dt' => Carbon::now(),
                    'user_no' => $user_id
                ]);

                // Add notification
                $this->createTryAppliedNotification($data['object_id'], 0);

                $is_liking = true;
            }
            // Add points
            $this->likeTry($user_id, $data['object_id'], $is_liking);
        } else {
            $likes = $this->review_like_repository->findWhere([
                'review_no' => $data['object_id'],
                'user_no' => $user_id
            ]);

            $review = $this->review_repository->find($data['object_id']);
            $author_id = $review->user_no;

            if (count($likes) > 0) {
                $this->review_like_repository->deleteWhere([
                    'review_no' => $data['object_id'],
                    'user_no' => $user_id
                ]);

                // Disable notification
                $this->createLikeNotification($data['object_id'], $data['object_type'], $author_id, false, true);

                $is_liking = false;
            } else {
                $this->review_like_repository->create([
                    'review_no' => $data['object_id'],
                    'recomend_dt' => Carbon::now(),
                    'user_no' => $user_id
                ]);

                // Add notification
                $this->createLikeNotification($data['object_id'], $data['object_type'], $author_id, false, false);

                $is_liking = true;
            }
            // Add points
            $this->likeReview($user_id, $data['object_id'], $is_liking);
        }

        if($data['object_type'] == 'try') {
            $users = $this->user_like_repository->getByCriteria(new GetUserLikeInfoCriteria($data['object_id']));
            $likes = $this->user_like_repository->getByCriteria(new GetLikeNumberOfTryByListIdCriteria([$data['object_id']]));
        } else {
            $users = $this->review_like_repository->getByCriteria(new GetReviewLikeInfoCriteria($data['object_id']));
            $likes = $this->review_like_repository->getByCriteria(new GetNumberOfLikesByReviewIdsCriteria([$data['object_id']]));
        }

        $total = 0;
        if(count($likes) > 0) {
            $total = $likes[0]->like_number;
        }

        $this->getCurrentUserFollowing($users);

        return ['users' => $users, 'total' => $total, 'is_liking' => $is_liking];
    }

    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->userFollowRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');


        $followers = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->userFollowRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->id])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }
}
