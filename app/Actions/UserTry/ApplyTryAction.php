<?php

namespace App\Actions\UserTry;

use App\Actions\UserPoint\AddPointsToUserAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Criterias\UserTry\GetNumberOfTriesByTryIdsCriteria;
use Carbon\Carbon;

class ApplyTryAction extends AddPointsToUserAction
{
    protected $repository;
    protected $try_repository;
    protected $user_repository;

    public function __construct(UserTryRepositoryInterface $repository,
                                UserRepositoryInterface $user_repository,
                                TryRepositoryInterface $try_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                CodeRepositoryInterface $codeRepository)
    {
        $this->repository = $repository;
        $this->user_repository = $user_repository;
        $this->try_repository = $try_repository;

        parent::__construct($user_point_repository, $codeRepository);
    }

    public function run($request)
    {
        $user_id = auth()->id();
        $try_id = $request['try_id'];

        $apply = $this->repository->findWhere([
            'user_no' => $user_id,
            'cntnts_no' => $try_id
        ]);

        if (count($apply) > 0) {
            return false;
        } else {
            $now = Carbon::now('UTC');

            $try = $this->try_repository->findWhere([
                'cntnts_no' => $try_id,
                ['event_bgnde', '<=', $now],
                ['event_endde', '>=', $now]
            ])->first();

            if ($try) {
                $user = $this->user_repository->find($user_id);

                $this->repository->create([
                    'user_no' => $user_id,
                    'cntnts_no' => $try_id,
                    'dlvy_addr' => $user->home_addr1,
                    'reqst_dt' => Carbon::now()->toDateString(),
                    'slctn_at' => 'N'
                ]);

                $this->tryApplied($user_id, $try_id);

                $total = 0;
                $number_of_tries = $this->repository->getByCriteria(new GetNumberOfTriesByTryIdsCriteria([$try_id]));
                if (count($number_of_tries) > 0) {
                    $total = $number_of_tries[0]->number_of_tries;
                }

                return $total;
            }

            return false;
        }
    }
}
