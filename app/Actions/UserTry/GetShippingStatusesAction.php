<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\UserTry;
use App\Actions\Constant;
use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;

class GetShippingStatusesAction extends Action {
    protected $repository;

    public function __construct(CodeRepositoryInterface $repository) {
        $this->repository = $repository;
    }

    public function run() {
        $shippings = $this->repository->findWhere([
            'code_group' => Constant::$SHIPPING_STATUS_GROUP_CODE,
            'use_at' => 'Y'
        ]);

        return $shippings;
    }
}
