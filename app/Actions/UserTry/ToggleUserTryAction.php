<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17/01/2019
 * Time: 10:12 AM
 */

namespace App\Actions\UserTry;

use App\Actions\UserPoint\AddPointsToUserAction;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ToggleUserTryAction extends AddPointsToUserAction
{
    protected $repository;
    protected $user_try_repository;
    protected $user_repository;
    protected $reviewHashTagsRepository;

    public function __construct(TryRepositoryInterface $repository,
                                UserRepositoryInterface $user_repository,
                                UserTryRepositoryInterface $user_try_repository,
                                UserPointRepositoryInterface $user_point_repository,
                                CodeRepositoryInterface $codeRepository)
    {
        $this->repository = $repository;
        $this->user_repository = $user_repository;
        $this->user_try_repository = $user_try_repository;

        parent::__construct($user_point_repository, $codeRepository);
    }

    public function run($cntnts_no, $user_no)
    {
        try {
            // If users are not admin, kick them out
            if (auth()->user()->role_id != 1) {
                return false;
            }

            $user_try = $this->user_try_repository->findWhere(['user_no' => $user_no, 'cntnts_no' => $cntnts_no])[0];

            $total = $this->user_try_repository->count([
                'cntnts_no' => $user_try->cntnts_no,
                'slctn_at' => 'Y'
            ]);

            $try = $this->repository->find($user_try->cntnts_no);

            if ($user_try->slctn_at != 'N') {
                //Handle un-select
//                $this->user_try_repository->update([
//                    'slctn_at' => 'N'
//                ], ['cntnts_no' => $cntnts_no, 'user_no'=>$user_no]);

                DB::table('TCT_DRWT')
                    ->where('cntnts_no', $cntnts_no)
                    ->where('user_no', $user_no)
                    ->update([
                        'slctn_at' => 'N',
                        'slctn_dt' => null,
                        'dlvy_dt' => null,
                        'dlvy_cmpny_code' => null,
                        'dlvy_mth_code' => null,
                        'invc_no' => null,
                        'memo' => null
                    ]);

                $this->tryWinner($user_no, $cntnts_no, false);

                $user_selected = $this->user_try_repository->count([
                    'user_no' => $user_try->user_no,
                ]);

                $selected_at = null;

                $last_selected = $this->user_try_repository->orderBy('slctn_at', 'desc')->findWhere([
                    'user_no' => $user_try->user_no,
                ])->first();

                if ($last_selected != null) {
                    $selected_at = Carbon::parse($last_selected->selected_at);
                }

                $this->user_repository->update([
                    'recent_selected_at' => $selected_at
                ], $user_try->user_no);

                return ['recent' => $selected_at ? $selected_at->toDateString() : null, 'number_of_selected' => $user_selected, 'is_selected' => false];
            } else {
                //Handle select
                if ($try->event_trgter_co > 0) {
                    if ($try->event_trgter_co <= $total) {
                        return false;
                    }

                    $selected_at = Carbon::now();

//                    $this->user_try_repository->update([
//                        'slctn_at' => $selected_at
//                    ], ['cntnts_no' => $cntnts_no, 'user_no'=>$user_no]);


                    DB::table('TCT_DRWT')
                        ->where('cntnts_no', $cntnts_no)
                        ->where('user_no', $user_no)
                        ->update(['slctn_at' => 'Y', 'slctn_dt' => Carbon::now()]);

                    $this->tryWinner($user_no, $cntnts_no, true);

                    $user_selected = $this->user_try_repository->count([
                        'user_no' => $user_try->user_no,
                    ]);

                    $this->user_repository->update([
                        'recent_selected_at' => $selected_at
                    ], $user_try->user_no);

                    return ['recent' => $selected_at->toDateString(), 'number_of_selected' => $user_selected, 'is_selected' => true];
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }
}
