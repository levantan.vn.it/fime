<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/12/2019
 * Time: 10:04 AM
 */

namespace App\Actions\Users;


use App\Actions\Action;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Files\GetMainImageOfReviewsCriteria;
use App\Criterias\Review\GetNumberOfReviewsByUserIdsCriteria;
use App\Criterias\Review\GetReviewsByUserId;
use App\Criterias\User\GetHotFimersOrderByReviewCountCriteria;
use App\Criterias\User\GetNewFimersOrderByReviewCountCriteria;
use App\Criterias\UserFollow\GetFollowingOfCurrentUserCriteria;
use App\Criterias\Users\GetNumberOfFollowersByUserIdsCriteria;
use App\Criterias\Users\GetNumberOfFollowingsByUserIdsCriteria;

class Fimers extends Action
{
    protected $userRepository;
    protected $reviewRepository;
    protected $followRepository;
    protected $reviewFileRepository;

    public function __construct(UserRepositoryInterface $userRepository, ReviewRepositoryInterface $reviewRepository,
                                UserFollowRepositoryInterface $followRepository, ReviewFilesRepositoryInterface $reviewFileRepository)
    {
        $this->userRepository = $userRepository;
        $this->reviewRepository = $reviewRepository;
        $this->followRepository = $followRepository;
        $this->reviewFileRepository = $reviewFileRepository;
    }

    public function run($type, $limit = 20, $offset = 0)
    {
        $fimers = $this->getFimers($type, $limit, $offset);
        $this->getReviewsOfFimer($fimers);
        $this->getCurrentUserFollowing($fimers);

        return $fimers;
    }

    private function getFimers($type, $limit, $offset)
    {
        try {
            switch ($type) {
                case 'new_member':
                    $fimers = $this->userRepository->pushCriteria(new GetNewFimersOrderByReviewCountCriteria())->paginate(12);
                    break;
                case 'monthly':
                    $fimers = $this->userRepository->pushCriteria(new GetHotFimersOrderByReviewCountCriteria(30))->paginate(12);
                    break;
                case 'weekly':
                    $fimers = $this->userRepository->pushCriteria(new GetHotFimersOrderByReviewCountCriteria(7))->paginate(12);
                    break;
                case 'all':
                default:
                    $fimers = $this->userRepository->pushCriteria(new GetHotFimersOrderByReviewCountCriteria(356))->paginate(12);
                    break;
            }


            return $fimers;
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * @param $fimers
     */
    private function getReviewsOfFimer($fimers)
    {
        $fimerIds = $fimers->pluck('user_no')->toArray();

        $number_of_reviews = $this->reviewRepository->getByCriteria(new GetNumberOfReviewsByUserIdsCriteria($fimerIds))->keyBy('user_id');

        foreach ($fimers as $key => $fimer) {
            $fimer->number_of_reviews = 0;
            if (isset($number_of_reviews[$fimer->user_no])) {
                $fimer->number_of_reviews = $number_of_reviews[$fimer->user_no]->number_of_reviews;
            }

            $reviews = $this->reviewRepository->getByCriteria(new GetReviewsByUserId($fimer->user_no, 5));

            $review_ids = $reviews->pluck('review_no')->toArray();
            $reviewsMainImage = $this->reviewFileRepository->getByCriteria(new GetMainImageOfReviewsCriteria($review_ids, true))->keyBy('review_no');

            foreach ($reviews as $review) {
                $review->is_liked = 0;
                if (isset($likes[$review->review_no])) {
                    $review->is_liked = 1;
                }
                if (isset($reviewsMainImage[$review->review_no]))
                    $review->main_image = $reviewsMainImage[$review->review_no];
            }

            $fimer->reviews = $reviews;
        }
    }

    protected function getCurrentUserFollowing($fimers)
    {
        $current_user_id = auth()->id();

        $user_ids = $fimers->pluck('user_no')->toArray();

        $user_followings = $this->followRepository->getByCriteria(
            new GetFollowingOfCurrentUserCriteria($user_ids, $current_user_id))->keyBy('followed_user_id');

        $followers = $this->followRepository->getByCriteria(
            new GetNumberOfFollowersByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        $followings = $this->followRepository->getByCriteria(
            new GetNumberOfFollowingsByUserIdsCriteria($user_ids)
        )->keyBy('user_id');

        foreach ($fimers as $fimer) {
            $fimer->followed = 0;
            $fimer->number_of_followers = 0;
            $fimer->number_of_followings = 0;

            if (isset($followers[$fimer->user_no])) {
                $fimer->number_of_followers = $followers[$fimer->user_no]->number_of_followers;
            }

            if (isset($followings[$fimer->user_no])) {
                $fimer->number_of_followings = $followings[$fimer->user_no]->number_of_followings;
            }

            if (isset($user_followings[$fimer->user_no])) {
                $fimer->followed = 1;
            }
        }
        return $fimers;
    }
}
