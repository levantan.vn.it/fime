<?php

namespace App\Actions\Users;

use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;

class GetByIdsAction extends Action {
    protected $user_repository;

    public function __construct(UserRepositoryInterface $user_repository) {
        $this->user_repository = $user_repository;
    }

    public function run($ids) {
        $users = $this->user_repository->findWhereIn('user_no', $ids);
        return $users;
    }
}
