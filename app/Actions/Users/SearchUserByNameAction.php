<?php

namespace App\Actions\Users;

use App\Actions\Action;
use App\Contracts\UserRepositoryInterface;

class SearchUserByNameAction extends Action {
    protected $user_repository;

    public function __construct(UserRepositoryInterface $user_repository) {
        $this->user_repository = $user_repository;
    }

    public function run($term) {
        $users = $this->user_repository->scopeQuery(function($query) use ($term){
            return $query->where('reg_name', 'like', '%' . $term . '%')
                    ->where('drmncy_at', 'N')
                    ->where('delete_at', 'N')
                    ->orderBy('reg_name','asc');
        })->paginate(10);
       return $users;
    }
}
