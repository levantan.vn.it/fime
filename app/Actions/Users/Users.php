<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/21/2019
 * Time: 1:32 PM
 */

namespace App\Actions\Users;


use App\Actions\Action;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\RoleRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Criterias\Users\GetListUsersCriteria;

class Users extends Action
{
    protected $userRepository;
    protected $roleRepository;
    protected $userPointRepository;
    protected $codeRepository;

    public function __construct(UserRepositoryInterface $userRepository,
                                RoleRepositoryInterface $roleRepository,
                                UserPointRepositoryInterface $userPointRepository,
                                CodeRepositoryInterface $codeRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->userPointRepository = $userPointRepository;
        $this->codeRepository = $codeRepository;
    }

    public function getList($request)
    {
        try {
            $searchType = isset($request['searchType']) ? $request['searchType'] : '';
            $searchValue = isset($request['searchValue']) ? $request['searchValue'] : '';
            $allowComment = isset($request['allowComment']) ? $request['allowComment'] : 1;
            $allowReview = isset($request['allowReview']) ? $request['allowReview'] : 1;
            $isActive = isset($request['isActive']) ? ($request['isActive'] === 'true' ? 'N' : 'Y') : 'N';
            $isDelete = isset($request['isDelete']) ? ($request['isDelete'] === 'true' ? 'Y' : 'N') : 'N';
            $role = isset($request['role']) ? $request['role'] : 'fimer';

            $role_id = $this->roleRepository->findByField('name', $role)->first();

            $users = $this->userRepository->pushCriteria(new GetListUsersCriteria($searchType, $searchValue, $allowComment, $allowReview, $isActive, $isDelete, $role_id->id))->paginate(10);

            $this->getUsersPoint($users);

            return $users;
        } catch (\Exception $e) {
            return [];
        }

    }

    public function getUsersPoint($users){
        $user_ids = $users->pluck('user_no')->toArray();

        $points = $this->userPointRepository->scopeQuery(function ($query) use ($user_ids){
            $query = $query->select(\DB::raw('sum(accml_pntt) as total_point'), 'user_no');
            $query = $query->join('TSM_CODE', 'TCT_PNTT.pntt_accml_code', '=', 'TSM_CODE.code');
            $query = $query->whereIn('user_no', $user_ids);
            $query = $query->where('TSM_CODE.code_group', 405);
            $query = $query->where('TSM_CODE.use_at', 'Y');
            $query = $query->groupBy('user_no');
            return $query;
        });

        $points = $points->all()->keyBy('user_no');
        foreach ($users as $user) {
            if (isset($points[$user->user_no])) {
                $user->total_point = $points[$user->user_no]->total_point;
            } else {
                $user->total_point = 0;
            }
        }
    }

}
