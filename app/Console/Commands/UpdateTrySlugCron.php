<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class UpdateTrySlugCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateTrySlugCron:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update slug for TOM_CNTNTS_WDTB table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $tries = DB::table('TOM_CNTNTS_WDTB')->select('cntnts_no', 'sj')->where(function ($query) {
                $query->whereNull('slug')->orWhere('slug', '');
            })->get();
            foreach ($tries as $try) {
                $slug = $this->createSlug($try->sj, $try->cntnts_no);
                DB::table('TOM_CNTNTS_WDTB')->where('cntnts_no', $try->cntnts_no)->update([
                    'slug' => $slug
                ]);
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function createSlug($title, $id = 0)
    {
        // Normalize the title
        $slug = str_slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 999999; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return DB::table('TOM_CNTNTS_WDTB')->select('slug')->where('slug', 'like', $slug . '%')
            ->where('CNTNTS_NO', '<>', $id)
            ->get();
    }
}
