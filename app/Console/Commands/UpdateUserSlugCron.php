<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class UpdateUserSlugCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateUserSlugCron:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update slug for TDM_USER table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $users = DB::table('TDM_USER')->select('user_no', 'reg_name')->where(function ($query) {
                $query->whereNull('slug')->orWhere('slug', '');
            })->get();

            foreach ($users as $user) {
                $slug = $this->createSlug($user->reg_name, $user->user_no);
                DB::table('TDM_USER')->where('user_no', $user->user_no)->update([
                    'slug' => $slug
                ]);
            }
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function createSlug($title, $id = '')
    {
        // Normalize the title
        $slug = str_slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 999999; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugs($slug, $id = '')
    {
        return DB::table('TDM_USER')->select('slug')->where('slug', 'like', $slug . '%')
            ->where('user_no', '<>', $id)
            ->get();
    }
}
