<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class updateNumberOfReviewsOfUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateNumberOfReviewsOfUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command updateNumberOfReviewsOfUsers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reviews = DB::table('TCT_REVIEW')->select('user_no', DB::raw('count(user_no) as number_of_reviews'))->groupBy('user_no')->get();

        foreach ($reviews as $review){
            DB::table('TDM_USER')->where('user_no', $review->user_no)->update([
                'reviews' => $review->number_of_reviews
            ]);
        }
    }
}
