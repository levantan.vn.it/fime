<?php

namespace App\Contracts;

/**
 * Interface RepositoryInterface.
 *
 */
interface RepositoryInterface
{
    public function count($wheres);
}