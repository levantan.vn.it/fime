<?php

namespace App\Contracts;

/**
 * Interface SettingRepositoryInterface.
 *
 */
interface SettingRepositoryInterface extends RepositoryInterface
{
}