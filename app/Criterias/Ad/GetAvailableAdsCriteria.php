<?php
namespace App\Criterias\Ad;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAvailableAdsCriteria implements CriteriaInterface
{

    public function __construct()
    {
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('ads.*')
            ->where('ads.show_on_frontend', '=', 1)
            ->orderBy('ads.order', 'asc');
        return $model;
    }
}