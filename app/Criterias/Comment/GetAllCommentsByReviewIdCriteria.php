<?php
namespace App\Criterias\Comment;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAllCommentsByReviewIdCriteria implements CriteriaInterface
{
    private $review_id;

    public function __construct($review_id)
    {
        $this->review_id = $review_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW_ANWR.id', 'TCT_REVIEW_ANWR.expsr_at', 'anwr_cn as content', 'anwr_writng_dt as created_at', 'TDM_USER.reg_name as author', 'TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW_ANWR.user_no')
            ->where('TCT_REVIEW_ANWR.review_no', '=', $this->review_id)
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW_ANWR.delete_at', 'N')
            ->orderBy('TCT_REVIEW_ANWR.anwr_writng_dt', 'asc');
        return $model;
    }
}