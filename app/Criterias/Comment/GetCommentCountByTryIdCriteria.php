<?php
namespace App\Criterias\Comment;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetCommentCountByTryIdCriteria implements CriteriaInterface
{
    private $object_id;
    private $parent_id;

    public function __construct($object_id, $parent_id = null)
    {
        $this->object_id = $object_id;
        $this->parent_id = $parent_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(DB::raw('count(*) as count'))
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_GOODS_ANWR.user_no')
            ->where('TCT_GOODS_ANWR.cntnts_no', '=', $this->object_id)
            ->where('TCT_GOODS_ANWR.parent_id', '=', $this->parent_id)
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_GOODS_ANWR.delete_at', 'N')
            ->where('TCT_GOODS_ANWR.expsr_at', 'Y')
            ->orderBy('TCT_GOODS_ANWR.anwr_writng_dt', 'desc');
        return $model;
    }
}