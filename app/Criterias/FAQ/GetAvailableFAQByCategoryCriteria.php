<?php
namespace App\Criterias\FAQ;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAvailableFAQByCategoryCriteria implements CriteriaInterface
{
    private $faq_se_code;

    public function __construct($faq_se_code)
    {
        $this->faq_se_code = $faq_se_code;
    }


    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_FAQ.CNTNTS_NO as cntnts_no' ,'TCT_FAQ.SJ as title', 'TCT_FAQ.CN as content',
                'TSM_CODE.CODE_NM as category_name', 'TOM_CNTNTS_WDTB.EXPSR_AT as is_approved')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.CNTNTS_NO', '=', 'TCT_FAQ.CNTNTS_NO')
            ->join('TSM_CODE', 'TSM_CODE.CODE', '=', 'TCT_FAQ.FAQ_SE_CODE')
            ->where('TOM_CNTNTS_WDTB.DELETE_AT', '=', 'N')
            ->where('TOM_CNTNTS_WDTB.EXPSR_AT', '=', 'Y')
            ->where('TCT_FAQ.FAQ_SE_CODE', '=', $this->faq_se_code)
            ->orderBy('TCT_FAQ.CNTNTS_NO', 'desc');
        return $model;
    }
}