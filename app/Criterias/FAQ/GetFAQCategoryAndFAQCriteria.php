<?php
namespace App\Criterias\FAQ;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetFAQCategoryAndFAQCriteria implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_FAQ.CNTNTS_NO as cntnts_no' ,'TCT_FAQ.SJ as title', 'TCT_FAQ.CN as content',
                'TSM_CODE.CODE_NM as category_name', 'TSM_CODE.CODE as code', 'TOM_CNTNTS_WDTB.EXPSR_AT as is_approved')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.CNTNTS_NO', '=', 'TCT_FAQ.CNTNTS_NO')
            ->join('TSM_CODE', 'TSM_CODE.CODE', '=', 'TCT_FAQ.FAQ_SE_CODE')
            ->where('TOM_CNTNTS_WDTB.DELETE_AT', '=', 'N')
            ->orderBy('TCT_FAQ.CNTNTS_NO', 'desc');
        return $model;
    }
}