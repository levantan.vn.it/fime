<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 3/4/2019
 * Time: 1:45 PM
 */

namespace App\Criterias\Files;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetMainImageOfReviewsCriteria implements CriteriaInterface
{
    /** @var array */
    private $reviews_no;
    private $use_thumb;

    public function __construct(array $reviews_no = [], $use_thumb =  false)
    {
        $this->reviews_no = $reviews_no;
        $this->use_thumb = $use_thumb;

    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->use_thumb) {
            $model = $model
                ->select('file_cours', 'file_size', 'review_no', 'orginl_file_nm',
                    \DB::raw("
                        CASE 
                            WHEN TCT_REVIEW_FILE.thumb_file_nm is null  THEN TCT_REVIEW_FILE.stre_file_nm
                            WHEN TCT_REVIEW_FILE.thumb_file_nm = ''  THEN TCT_REVIEW_FILE.stre_file_nm                 
                            ELSE TCT_REVIEW_FILE.thumb_file_nm
                         END AS stre_file_nm"))
                ->whereIn('TCT_REVIEW_FILE.review_no', $this->reviews_no)
                ->where('file_sn', 1)
            ;
        } else {
            $model = $model
                ->select('file_cours', 'file_size', 'review_no', 'orginl_file_nm', 'stre_file_nm')
                ->whereIn('TCT_REVIEW_FILE.review_no', $this->reviews_no)
                ->where('file_sn', 1)
            ;
        }

        return $model;
    }
}
