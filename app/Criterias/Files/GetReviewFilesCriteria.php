<?php
namespace App\Criterias\Files;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewFilesCriteria implements CriteriaInterface
{
    private $review_no;
    private $is_thumb;

    public function __construct($review_no, $is_thumb = false)
    {
        $this->review_no = $review_no;
        $this->is_thumb = $is_thumb;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->is_thumb) {
            $model = $model
                ->select('TCT_REVIEW_FILE.file_se_code', 'TCT_REVIEW_FILE.file_sn',
                    'TCT_REVIEW_FILE.file_cours', 'TCT_REVIEW_FILE.review_no', 'TCT_REVIEW_FILE.thumb_file_nm2', 'TCT_REVIEW_FILE.se_code',
                    'TCT_REVIEW_FILE.image_dc',
                    \DB::raw("
                        CASE 
                            WHEN TCT_REVIEW_FILE.thumb_file_nm is null  THEN TCT_REVIEW_FILE.stre_file_nm
                            WHEN TCT_REVIEW_FILE.thumb_file_nm = ''  THEN TCT_REVIEW_FILE.stre_file_nm                 
                            ELSE TCT_REVIEW_FILE.thumb_file_nm
                         END AS stre_file_nm"))
                ->join('TCT_REVIEW', 'TCT_REVIEW.review_no', '=', 'TCT_REVIEW_FILE.review_no')
                ->where('TCT_REVIEW.review_no', '=', $this->review_no);
        } else {
            $model = $model
                ->select('TCT_REVIEW_FILE.*')
                ->join('TCT_REVIEW', 'TCT_REVIEW.review_no', '=', 'TCT_REVIEW_FILE.review_no')
                ->where('TCT_REVIEW.review_no', '=', $this->review_no);
        }
        return $model;
    }
}