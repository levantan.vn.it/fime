<?php
namespace App\Criterias\Hashtag;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTopHashtagsCriteria implements CriteriaInterface
{

    public function __construct()
    {
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('hash_tag as name')
            ->join('review_hashtag', 'TCT_POPHASH.hash_seq', '=', 'review_hashtag.hash_seq')
            ->orderBy('hash_cnt', 'desc')
            ->distinct()
            ->limit(40);

        return $model;
    }
}
