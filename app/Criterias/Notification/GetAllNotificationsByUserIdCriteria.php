<?php
namespace App\Criterias\Notification;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAllNotificationsByUserIdCriteria implements CriteriaInterface
{
    private $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('notifications.guid', DB::raw('max(id) as id'))
            ->where(function($query) {
                $query->where('notifications.user_id', '=', $this->user_id);
                $query->orWhereNull('notifications.user_id');
            })
            ->where('notifications.is_disabled', '=', 0)
            ->groupBy('notifications.guid')
            ->orderBy('posted_at', 'desc');
        return $model;
    }
}