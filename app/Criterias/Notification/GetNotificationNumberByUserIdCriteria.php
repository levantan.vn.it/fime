<?php

namespace App\Criterias\Notification;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNotificationNumberByUserIdCriteria implements CriteriaInterface
{
    private $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $now = Carbon::now();
        $model = $model
            ->select(DB::raw('count(*) as count'))
            ->whereIn('id', function ($query) use ($now) {
                $sub_query = 'max(id) as id 
                              from notifications 
                              where (notifications.user_id = "' . $this->user_id . '" or notifications.user_id is null)
                              and notifications.is_disabled = 0
                              and notifications.deleted_at is null
                              and notifications.posted_at <= "' . $now . '"
                              group by notifications.guid';
                $query->select(DB::raw($sub_query));
            })
            ->where('notifications.is_seen', '=', 0);
        return $model;
    }
}