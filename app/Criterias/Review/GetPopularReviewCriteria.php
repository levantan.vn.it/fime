<?php
namespace App\Criterias\Review;

use App\Actions\Constant;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetPopularReviewCriteria implements CriteriaInterface
{
    /**
     * GetRecentlyReviewCriteria constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW.cntnts_no',
                'TCT_REVIEW.updt_dt',
                'TCT_REVIEW.delete_at',
                'TCT_REVIEW.expsr_at',
                'TCT_REVIEW.pblonsip_at',
                'TCT_REVIEW.pblonsip_snts',
                'TCT_REVIEW.pblonsip_time',
                'TCT_REVIEW.review_no',
                'TCT_REVIEW.user_no',
                'TCT_REVIEW.goods_cl_code',
                'TCT_REVIEW.goods_nm',
                'TCT_REVIEW.m_cnt',
                'TCT_REVIEW.p_cnt',
                'TCT_REVIEW.slug',
                'TCT_REVIEW.review_short',
                'TCT_REVIEW.writng_dt',
                'TDM_USER.reg_name as author_name', 'TDM_USER.id as author_ds','TDM_USER.user_no as author_id', 'TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->join('TCT_MAIN_CONTS', 'TCT_MAIN_CONTS.ref_no', '=', 'TCT_REVIEW.review_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->where('TCT_MAIN_CONTS.cont_std', Constant::$POPULAR_CONT_STD_CODE)
            ->where('TCT_MAIN_CONTS.cont_type', Constant::$POPULAR_CONT_TYPE_CODE)
            ->orderBy('TCT_REVIEW.writng_dt', 'desc')
            ->limit(12);
        return $model;
    }
}