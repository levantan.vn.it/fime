<?php

namespace App\Criterias\Review;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewAfterPagingCriteria implements CriteriaInterface
{
    private $review_id;

    public function __construct($review_id)
    {
        $this->review_id = $review_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW.*', 'TDM_USER.reg_name as author', 'TDM_USER.id as author_ds', 'TDM_USER.user_no as author_id', 'TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->orderBy('TCT_REVIEW.review_no', 'desc');
        if ($this->review_id) {
            $model = $model->where('TCT_REVIEW.review_no', '<', $this->review_id);
        }
        return $model;
    }
}
