<?php

namespace App\Criterias\Review;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewAndAuthorPagingCriteria implements CriteriaInterface
{
    private $review_category_slug;
    private $search_value;
    private $reviewIds;

    public function __construct($review_category_slug = null, $search_value = null, $reviewIds = null)
    {
        $this->review_category_slug = $review_category_slug;
        $this->search_value = $search_value;
        $this->reviewIds = $reviewIds;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW.cntnts_no',
                'TCT_REVIEW.updt_dt',
                'TCT_REVIEW.delete_at',
                'TCT_REVIEW.expsr_at',
                'TCT_REVIEW.pblonsip_at',
                'TCT_REVIEW.pblonsip_snts',
                'TCT_REVIEW.pblonsip_time',
                'TCT_REVIEW.review_no',
                'TCT_REVIEW.user_no',
                'TCT_REVIEW.goods_cl_code',
                'TCT_REVIEW.goods_nm',
                'TCT_REVIEW.m_cnt',
                'TCT_REVIEW.p_cnt',
                'TCT_REVIEW.slug',
                'TCT_REVIEW.review_short',
                'TCT_REVIEW.writng_dt',
                'TDM_USER.reg_name as author', 'TDM_USER.user_no as author_id',
                'TDM_USER.pic as author_avatar', 'TDM_USER.id as author_ds', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->orderBy('TCT_REVIEW.review_no', 'desc');

        if ($this->reviewIds !== null && is_array($this->reviewIds) && count($this->reviewIds)) {
            $model = $model->whereIn('TCT_REVIEW.review_no', $this->reviewIds);
        }

        if ($this->review_category_slug != null && $this->review_category_slug !== 'all') {
            $model = $model
                ->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_REVIEW.goods_cl_code')
                ->where('TSM_CODE.slug', '=', $this->review_category_slug);
        }

        if ($this->search_value != null) {
            $model = $model->where(function ($query) {
                $query->where('TCT_REVIEW.review_dc', 'LIKE', '%' . $this->search_value . '%');
                $query->orWhere('TCT_REVIEW.goods_nm', 'LIKE', '%' . $this->search_value . '%');
            });
        }
        return $model;
    }
}
