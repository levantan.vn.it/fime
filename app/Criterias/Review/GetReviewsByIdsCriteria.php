<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/4/2019
 * Time: 11:08 AM
 */

namespace App\Criterias\Review;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewsByIdsCriteria implements CriteriaInterface
{
    public $ids;
    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.

        $model = $model
            ->select('TCT_REVIEW.*', 'TDM_USER.reg_name as author_name', 'TDM_USER.id as author_ds', 'TDM_USER.user_no as author_id', 'TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->orderBy('TCT_REVIEW.writng_dt', 'desc')
            ->whereIn('TCT_REVIEW.review_no', $this->ids);
        return $model;
    }
}
