<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/15/2019
 * Time: 5:21 PM
 */

namespace App\Criterias\Review;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewsByUserId implements CriteriaInterface
{
    /** @var int */
    private $userId;
    private $limit;

    public function __construct($userId = 1, $limit = 0)
    {
        $this->userId = $userId;
        $this->limit = $limit;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW.cntnts_no',
                'TCT_REVIEW.updt_dt',
                'TCT_REVIEW.delete_at',
                'TCT_REVIEW.expsr_at',
                'TCT_REVIEW.pblonsip_at',
                'TCT_REVIEW.pblonsip_snts',
                'TCT_REVIEW.pblonsip_time',
                'TCT_REVIEW.review_no',
                'TCT_REVIEW.user_no',
                'TCT_REVIEW.goods_cl_code',
                'TCT_REVIEW.goods_nm',
                'TCT_REVIEW.m_cnt',
                'TCT_REVIEW.p_cnt',
                'TCT_REVIEW.slug',
                'TCT_REVIEW.review_short',
                'TCT_REVIEW.writng_dt',
                'TDM_USER.reg_name as author', 'TDM_USER.user_no as author_id', 'TDM_USER.pic as author_avatar', 'TDM_USER.slug as author_slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW.user_no')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->where('TCT_REVIEW.user_no', $this->userId)
            ->orderBy('TCT_REVIEW.writng_dt', 'desc');

        if($this->limit != 0) {
            $model = $model->limit($this->limit);
        }

        return $model;
    }
}
