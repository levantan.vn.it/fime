<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/13/2019
 * Time: 4:45 PM
 */

namespace App\Criterias\Review;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewsOfListFimersCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    public $fimerIds = [];

    public function __construct($fimerIds)
    {
        $this->fimerIds = $fimerIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('id', 'name', 'slug', 'images', 'created_by')
            ->whereIn('created_by', $this->fimerIds)
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc');
        return $model;
    }
}
