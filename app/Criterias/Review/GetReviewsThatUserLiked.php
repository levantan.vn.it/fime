<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 10:13 AM
 */

namespace App\Criterias\Review;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetReviewsThatUserLiked implements CriteriaInterface
{
    /** @var int */
    private $reviewsIds;

    public function __construct(array $reviewsIds = [])
    {
        $this->reviewsIds = $reviewsIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_REVIEW.cntnts_no',
                'TCT_REVIEW.updt_dt',
                'TCT_REVIEW.delete_at',
                'TCT_REVIEW.expsr_at',
                'TCT_REVIEW.pblonsip_at',
                'TCT_REVIEW.pblonsip_snts',
                'TCT_REVIEW.pblonsip_time',
                'TCT_REVIEW.review_no',
                'TCT_REVIEW.user_no',
                'TCT_REVIEW.goods_cl_code',
                'TCT_REVIEW.goods_nm',
                'TCT_REVIEW.m_cnt',
                'TCT_REVIEW.p_cnt',
                'TCT_REVIEW.slug',
                'TCT_REVIEW.review_short',
                'TCT_REVIEW.writng_dt as created_at')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->whereIn('TCT_REVIEW.review_no', $this->reviewsIds)
            ->groupBy('TCT_REVIEW.review_no')
            ->orderBy('TCT_REVIEW.writng_dt', 'desc');
        return $model;
    }
}
