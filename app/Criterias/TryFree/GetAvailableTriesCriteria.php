<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 2:36 PM
 */

namespace App\Criterias\TryFree;


use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAvailableTriesCriteria implements CriteriaInterface
{
    public function __construct()
    {
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_GOODS.cntnts_no',
                'TCT_GOODS.modl_nombr',
                'TCT_GOODS.brnd_nm',
                'TCT_GOODS.event_knd_code',
                'TCT_GOODS.event_bgnde',
                'TCT_GOODS.event_endde',
                'TCT_GOODS.dlvy_bgnde',
                'TCT_GOODS.dlvy_endde',
                'TCT_GOODS.time_color_code',
                'TCT_GOODS.event_trgter_co',
                'TCT_GOODS.goods_pc',
                'TCT_GOODS.slctn_compt_at',
                'TCT_GOODS.link_url',
                'TCT_GOODS.brnd_code',
                'TCT_GOODS.goods_cl_code',
                'TCT_GOODS.goods_txt_code',
                'TCT_GOODS.goods_txt',
                'TCT_GOODS.resource_type',
                'TCT_GOODS.is_try_event',
                'TCT_GOODS.try_event_type',
                'TCT_GOODS.quantity_to_qualify',
                'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.slug', 'TOM_CNTNTS_WDTB.regist_dt AS created_at',
                'TSM_CODE.code_nm AS category_name', 'TOM_CNTNTS_WDTB.expsr_at AS is_disabled')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->join('TSM_CODE', 'TSM_CODE.code', '=', 'TCT_GOODS.goods_cl_code')
            ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
            ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')
            ->where('event_bgnde', '<=' , Carbon::now('UTC'))
            ->where('event_endde', '>=' , Carbon::now('UTC'))
            ->orderBy('event_endde', 'asc')
            ->take(5);

        return $model;
    }
}
