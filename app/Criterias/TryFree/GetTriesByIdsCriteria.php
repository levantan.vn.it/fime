<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/3/2019
 * Time: 5:23 PM
 */

namespace App\Criterias\TryFree;


use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTriesByIdsCriteria implements CriteriaInterface
{
    public $ids;
    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.
        $now = Carbon::now('UTC');

        $model = $model
            ->select('TCT_GOODS.*', 'AllTries.cntnts_nm', 'AllTries.slug')
            ->join(\DB::raw('(
                (SELECT 1 as sort_key, ROW_NUMBER() OVER (ORDER BY event_endde asc) as row_number, TCT_GOODS.cntnts_no,
                TOM_CNTNTS_WDTB.sj AS cntnts_nm, TOM_CNTNTS_WDTB.slug
                FROM TCT_GOODS INNER JOIN TOM_CNTNTS_WDTB ON TCT_GOODS.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no
                WHERE TOM_CNTNTS_WDTB.delete_at = \'N\' AND TOM_CNTNTS_WDTB.expsr_at = \'Y\' AND event_bgnde < \''. $now .'\'
                AND event_endde >= \'' . $now . '\' ORDER BY event_endde)
                UNION ALL
                (SELECT 2 as sort_key, ROW_NUMBER() OVER (ORDER BY event_bgnde asc) as row_number, TCT_GOODS.cntnts_no,
                TOM_CNTNTS_WDTB.sj AS cntnts_nm, TOM_CNTNTS_WDTB.slug
                FROM TCT_GOODS INNER JOIN TOM_CNTNTS_WDTB ON TCT_GOODS.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no
                WHERE TOM_CNTNTS_WDTB.delete_at = \'N\' AND TOM_CNTNTS_WDTB.expsr_at = \'Y\' AND event_bgnde >= \''. $now .'\'
                AND event_endde >= \'' . $now . '\' ORDER BY event_bgnde)
                 UNION ALL
                (SELECT 3 as sort_key, ROW_NUMBER() OVER (ORDER BY event_endde DESC) as row_number, TCT_GOODS.cntnts_no,
                TOM_CNTNTS_WDTB.sj AS cntnts_nm, TOM_CNTNTS_WDTB.slug
                FROM TCT_GOODS INNER JOIN TOM_CNTNTS_WDTB ON TCT_GOODS.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no
                WHERE TOM_CNTNTS_WDTB.delete_at = \'N\' AND TOM_CNTNTS_WDTB.expsr_at = \'Y\' AND event_endde < \'' . $now . '\' ORDER BY event_endde DESC)
                ) AS AllTries'),  function($join)
            {
                $join->on('TCT_GOODS.cntnts_no', '=', 'AllTries.cntnts_no');
            })
            ->whereIn('TCT_GOODS.cntnts_no', $this->ids)
            ->orderByRaw('sort_key, row_number');

        return $model;
    }
}
