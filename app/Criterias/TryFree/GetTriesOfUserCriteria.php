<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/22/2019
 * Time: 2:55 PM
 */

namespace App\Criterias\TryFree;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetTriesOfUserCriteria implements CriteriaInterface
{
    /** @var int */
    private $userId;

    /**
     * GetUsersFollowCriteria constructor.
     * @param int $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.
        $model = $model
            ->select('TCT_GOODS.cntnts_no',
                'TCT_GOODS.modl_nombr',
                'TCT_GOODS.brnd_nm',
                'TCT_GOODS.event_knd_code',
                'TCT_GOODS.event_bgnde',
                'TCT_GOODS.event_endde',
                'TCT_GOODS.dlvy_bgnde',
                'TCT_GOODS.dlvy_endde',
                'TCT_GOODS.time_color_code',
                'TCT_GOODS.event_trgter_co',
                'TCT_GOODS.goods_pc',
                'TCT_GOODS.slctn_compt_at',
                'TCT_GOODS.link_url',
                'TCT_GOODS.brnd_code',
                'TCT_GOODS.goods_cl_code',
                'TCT_GOODS.goods_txt_code',
                'TCT_GOODS.goods_txt',
                'TCT_GOODS.resource_type',
                'TCT_GOODS.is_try_event',
                'TCT_GOODS.try_event_type',
                'TCT_GOODS.quantity_to_qualify',
                'TOM_CNTNTS_WDTB.sj AS cntnts_nm', 'TOM_CNTNTS_WDTB.slug', 'TCT_DRWT.dlvy_dt',
                'TCT_DRWT.slctn_at',
                'TCT_DRWT.dlvy_addr', 'TCT_DRWT.dlvy_cmpny_code', 'TCT_DRWT.dlvy_mth_code', 'TCT_DRWT.invc_no')
            ->join('TCT_GOODS', 'TCT_DRWT.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
            ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')
            ->where('TCT_DRWT.user_no', $this->userId)
            ->orderBy('reqst_dt', 'desc');
        return $model;
    }
}
