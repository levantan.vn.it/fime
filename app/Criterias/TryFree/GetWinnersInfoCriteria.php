<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/22/2019
 * Time: 2:55 PM
 */

namespace App\Criterias\TryFree;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetWinnersInfoCriteria implements CriteriaInterface
{
    private $try_ids;

    /** @var int */
    private $userId;


    /**
     * GetUsersFollowCriteria constructor.
     * @param int $userId
     */
    public function __construct($try_ids, $userId)
    {
        $this->try_ids = $try_ids;
        $this->userId = $userId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.
        $model = $model
            ->select('TCT_GOODS.cntnts_no', 'TCT_DRWT.dlvy_dt', 'TCT_DRWT.slctn_dt',
                'TCT_DRWT.dlvy_addr', 'TCT_DRWT.dlvy_cmpny_code', 'TCT_DRWT.dlvy_mth_code', 'TCT_DRWT.invc_no')
            ->join('TCT_GOODS', 'TCT_DRWT.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->join('TOM_CNTNTS_WDTB', 'TOM_CNTNTS_WDTB.cntnts_no', '=', 'TCT_GOODS.cntnts_no')
            ->where('TOM_CNTNTS_WDTB.delete_at', 'N')
            ->where('TOM_CNTNTS_WDTB.expsr_at', 'Y')
            ->where('TCT_DRWT.user_no', $this->userId)
            ->whereIn('TCT_GOODS.cntnts_no', $this->try_ids)
            ->orderBy('reqst_dt', 'desc');
        return $model;
    }
}
