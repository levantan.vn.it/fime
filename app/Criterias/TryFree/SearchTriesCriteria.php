<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 3/6/2019
 * Time: 2:21 PM
 */

namespace App\Criterias\TryFree;


use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class SearchTriesCriteria implements CriteriaInterface
{
    /** @var string */
    private $searchValue;

    /**
     * SearchTriesCriteria constructor.
     * @param string|null $searchValue
     */
    public function __construct(string $searchValue = null)
    {
        $this->searchValue = $searchValue;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // TODO: Implement apply() method.
        $now = Carbon::now('UTC');

        $model = $model
            ->select('TCT_GOODS.cntnts_no',
                'TCT_GOODS.modl_nombr',
                'TCT_GOODS.brnd_nm',
                'TCT_GOODS.event_knd_code',
                'TCT_GOODS.event_bgnde',
                'TCT_GOODS.event_endde',
                'TCT_GOODS.dlvy_bgnde',
                'TCT_GOODS.dlvy_endde',
                'TCT_GOODS.time_color_code',
                'TCT_GOODS.event_trgter_co',
                'TCT_GOODS.goods_pc',
                'TCT_GOODS.slctn_compt_at',
                'TCT_GOODS.link_url',
                'TCT_GOODS.brnd_code',
                'TCT_GOODS.goods_cl_code',
                'TCT_GOODS.goods_txt_code',
                'TCT_GOODS.goods_txt',
                'TCT_GOODS.resource_type',
                'TCT_GOODS.is_try_event',
                'TCT_GOODS.try_event_type',
                'TCT_GOODS.quantity_to_qualify',
                'AllTries.cntnts_nm', 'AllTries.slug')
            ->join(\DB::raw('(
                (SELECT 1 as sort_key, ROW_NUMBER() OVER (ORDER BY event_endde asc) as row_number, TCT_GOODS.cntnts_no,
                TOM_CNTNTS_WDTB.sj AS cntnts_nm, TOM_CNTNTS_WDTB.slug
                FROM TCT_GOODS INNER JOIN TOM_CNTNTS_WDTB ON TCT_GOODS.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no
                WHERE TOM_CNTNTS_WDTB.delete_at = \'N\' AND TOM_CNTNTS_WDTB.expsr_at = \'Y\' AND event_bgnde < \''. $now .'\'
                AND event_endde >= \'' . $now . '\' ORDER BY event_endde)
                UNION ALL
                (SELECT 2 as sort_key, ROW_NUMBER() OVER (ORDER BY event_bgnde asc) as row_number, TCT_GOODS.cntnts_no,
                TOM_CNTNTS_WDTB.sj AS cntnts_nm, TOM_CNTNTS_WDTB.slug
                FROM TCT_GOODS INNER JOIN TOM_CNTNTS_WDTB ON TCT_GOODS.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no
                WHERE TOM_CNTNTS_WDTB.delete_at = \'N\' AND TOM_CNTNTS_WDTB.expsr_at = \'Y\' AND event_bgnde >= \''. $now .'\'
                AND event_endde >= \'' . $now . '\' ORDER BY event_bgnde)
                 UNION ALL
                (SELECT 3 as sort_key, ROW_NUMBER() OVER (ORDER BY event_endde DESC) as row_number, TCT_GOODS.cntnts_no,
                TOM_CNTNTS_WDTB.sj AS cntnts_nm, TOM_CNTNTS_WDTB.slug
                FROM TCT_GOODS INNER JOIN TOM_CNTNTS_WDTB ON TCT_GOODS.cntnts_no = TOM_CNTNTS_WDTB.cntnts_no
                WHERE TOM_CNTNTS_WDTB.delete_at = \'N\' AND TOM_CNTNTS_WDTB.expsr_at = \'Y\' AND event_endde < \'' . $now . '\' ORDER BY event_endde DESC)
                ) AS AllTries'),  function($join)
            {
                $join->on('TCT_GOODS.cntnts_no', '=', 'AllTries.cntnts_no');
            })
            ->orderByRaw('sort_key, row_number')
            ->skip((\Request::input('page') - 1) * 12)
            ->limit(12);

        if ($this->searchValue != null) {
            $model = $model->where(function ($query) {
                $query->where('AllTries.cntnts_nm', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_GOODS.modl_nombr', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_GOODS.hash_tag', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('TCT_GOODS.goods_dc', 'LIKE', '%' . $this->searchValue . '%');

                return $query;
            });
        }

        return $model;
    }
}
