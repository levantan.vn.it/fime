<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/13/2019
 * Time: 3:00 PM
 */

namespace App\Criterias\User;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetHotFimersCriteria implements CriteriaInterface
{
    /** @var int */
    private $days;

    /** @var string */
    private $searchValue;

    /**
     * GetHotFimersCriteria constructor.
     * @param int $days
     * @param null $searchValue
     */
    public function __construct(int $days = 30, $searchValue = null)
    {
        $this->days = $days;
        $this->searchValue = $searchValue;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(!in_array($this->days, [7, 30, 90])){
            $query = 'hot_fimer_1year';
        }else{
            $query = 'hot_fimer_' . $this->days . 'days';
        }

        $model = $model
            ->where('drmncy_at', 'N')
            ->where('delete_at', 'N')
            ->orderBy($query, 'desc')
            ->orderBy('reg_name', 'asc');

        if ($this->searchValue != null) {
            $model = $model->where(function ($query) {
                $query->where('reg_name', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('id', 'LIKE', '%' . $this->searchValue . '%');
                $query->orWhere('slug', 'LIKE', '%' . $this->searchValue . '%');
            });
        }


        return $model;
    }
}
