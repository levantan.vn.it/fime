<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class GetNewFimersCriteria.
 *
 * @package namespace App\Criterias;
 */
class GetNewFimersCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->where('drmncy_at', 'N')
            ->where('delete_at', 'N')
            ->orderBy('user_no', 'desc')
            ->orderBy('reg_name', 'asc');
        return $model;
    }
}
