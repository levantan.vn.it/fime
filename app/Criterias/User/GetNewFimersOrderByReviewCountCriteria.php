<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class GetNewFimersCriteria.
 *
 * @package namespace App\Criterias;
 */
class GetNewFimersOrderByReviewCountCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TDM_USER.pic', 'TDM_USER.slug', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.user_no', \DB::raw("count('TCT_REVIEW.REVIEW_NO') as reviews"))
            ->join('TCT_REVIEW', 'TCT_REVIEW.USER_NO', 'TDM_USER.USER_NO')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->groupBy('TDM_USER.USER_NO')
            ->orderBy('TDM_USER.created_at', 'desc')
            ->orderBy('reviews', 'desc');
        return $model;
    }
}
