<?php
namespace App\Criterias\UserLike;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfLikesByReviewIdsCriteria implements CriteriaInterface
{
    private $review_ids;
    public function __construct($review_ids)
    {
        $this->review_ids = $review_ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(DB::raw('count(*) as like_number'), 'TCT_REVIEW.review_no as object_id')
            ->join('TCT_REVIEW', 'TCT_REVIEW.review_no', '=', 'TCT_REVIEW_RECM.review_no')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_REVIEW_RECM.user_no')
            ->whereIn('TCT_REVIEW.review_no', $this->review_ids)
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_REVIEW.delete_at', 'N')
            ->where('TCT_REVIEW.expsr_at', 'Y')
            ->groupBy('TCT_REVIEW.review_no');
        return $model;
    }
}