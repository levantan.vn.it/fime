<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 9:48 AM
 */

namespace App\Criterias\UserLike;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfLikesByUserIdCriteria implements CriteriaInterface
{
    /** @var int */
    private $userIds;

    public function __construct($userIds)
    {
        $this->userIds = $userIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $ids = join('\',\'', $this->userIds);
        $model = $model
            ->select(\DB::raw('count(*) as number_of_likes'), 'TDM_USER.user_no as user_id')
            ->join(\DB::raw('((SELECT distinct(gr.cntnts_no) as id , u.user_no FROM TDM_USER u 
                INNER JOIN TCT_GOODS_RECM gr on gr.user_no = u.user_no
                INNER JOIN TOM_CNTNTS_WDTB g on g.cntnts_no = gr.cntnts_no
                WHERE g.delete_at = "N" AND g.expsr_at = "Y" 
                AND gr.user_no IN (\'' . $ids .'\')) UNION ALL
                (SELECT distinct(rr.review_no) as id , u.user_no FROM TDM_USER u 
                INNER JOIN TCT_REVIEW_RECM rr on rr.user_no = u.user_no
                INNER JOIN TCT_REVIEW r on r.review_no = rr.review_no
                WHERE r.delete_at = "N" AND r.expsr_at = "Y" 
                AND rr.user_no IN (\'' . $ids .'\'))) AS TotalLikes'),
                function($join)
                {
                    $join->on('TDM_USER.user_no', '=', 'TotalLikes.user_no');
                })
                ->groupBy('TDM_USER.user_no');

        return $model;
    }
}
