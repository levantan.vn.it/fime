<?php
namespace App\Criterias\UserLike;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetUserLikeInfoCriteria implements CriteriaInterface
{
    private $object_id;
    public function __construct($object_id)
    {
        $this->object_id = $object_id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TDM_USER.user_no', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.pic','TDM_USER.slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_GOODS_RECM.user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('cntnts_no', $this->object_id)
            ->orderBy('recomend_dt', 'desc')
            ->limit(3);
        return $model;
    }
}
