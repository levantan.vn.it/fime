<?php
namespace App\Criterias\UserTry;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetAllCriteria implements CriteriaInterface
{
    private $try_id;
    private $name;
    private $is_selected;

    public function __construct($try_id, $name = null, $is_selected = -1)
    {
        $this->try_id = $try_id;
        $this->name = $name;
        $this->is_selected =  $is_selected;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_DRWT.*',
                'TDM_USER.created_at',
                'TDM_USER.reg_name',
                'TDM_USER.email',
                'TDM_USER.home_addr1',
                'TDM_USER.cellphone')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_DRWT.user_no')
            ->where('cntnts_no', $this->try_id);

        if ($this->name != null && $this->name != 'null' && $this->name != '') {
            $model = $model->where(function ($query) {
                $query->where('TDM_USER.user_no', 'LIKE', '%' . $this->name . '%');
                $query->orWhere('TDM_USER.reg_name', 'LIKE', '%' . $this->name . '%');
            });
        }

        if ($this->is_selected != -1) {
            if ($this->is_selected) {
                $model = $model->where('TCT_DRWT.SLCTN_AT',  'Y');
            } else {
                $model = $model->where('TCT_DRWT.SLCTN_AT', 'N');
            }
        }

        return $model;
    }
}
