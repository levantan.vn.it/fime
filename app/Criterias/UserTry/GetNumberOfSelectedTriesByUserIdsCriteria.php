<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 9:48 AM
 */

namespace App\Criterias\UserTry;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfSelectedTriesByUserIdsCriteria implements CriteriaInterface
{
    /** @var int */
    private $userIds;

    public function __construct($userIds)
    {
        $this->userIds = $userIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(\DB::raw('count(*) as number_of_tries'), 'user_no as user_id')
            ->where('SLCTN_AT', '<>', 'N')
            ->groupBy('user_no');

        if(!empty($this->userIds))
            $model->whereIn('user_no', $this->userIds);

        return $model;
    }
}
