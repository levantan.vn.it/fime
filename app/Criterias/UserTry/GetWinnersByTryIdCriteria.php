<?php
namespace App\Criterias\UserTry;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetWinnersByTryIdCriteria implements CriteriaInterface
{
    private $try_id;
    private $name;

    public function __construct($try_id, $name = null)
    {
        $this->try_id = $try_id;
        $this->name = $name;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('TCT_DRWT.*', 'TDM_USER.reg_name', 'TDM_USER.id', 'TDM_USER.pic', 'TDM_USER.slug')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_DRWT.user_no')
            ->where('cntnts_no', $this->try_id)
            ->where('slctn_at', 'Y')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N');

        return $model;
    }
}