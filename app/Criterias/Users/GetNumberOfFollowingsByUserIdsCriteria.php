<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 2:08 PM
 */

namespace App\Criterias\Users;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetNumberOfFollowingsByUserIdsCriteria implements CriteriaInterface
{
    /** @var array */
    private $userIds;

    public function __construct(array $userIds = [])
    {
        $this->userIds = $userIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(\DB::raw('count(*) as number_of_followings'), 'TCT_FLLW.fllwr_user_no as user_id')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_FLLW.fllwr_user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->whereIn('TCT_FLLW.fllwr_user_no', $this->userIds)
            ->groupBy('TCT_FLLW.fllwr_user_no');
        return $model;
    }
}
