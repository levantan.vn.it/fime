<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/19/2019
 * Time: 2:08 PM
 */

namespace App\Criterias\Users;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetUsersByIdsCriteria implements CriteriaInterface
{
    /** @var array */
    private $userIds;

    public function __construct(array $userIds = [])
    {
        $this->userIds = $userIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('user_no', 'reg_name', 'id', 'slug', 'pic')
            ->whereIn('user_no', $this->userIds)
            ->where('drmncy_at', 'N')
            ->where('delete_at', 'N')
            ->orderBy('user_no', 'desc');
        return $model;
    }
}
