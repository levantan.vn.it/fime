<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 2/22/2019
 * Time: 1:43 PM
 */

namespace App\Criterias\Users;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class GetUsersFollowCriteria implements CriteriaInterface
{
    /** @var string */
    private $userId;
    private $fllwr_user_no;

    /**
     * GetUsersFollowingCriteria constructor.
     * @param string $userId
     * @param array $fllwr_user_no
     */
    public function __construct(string $userId = '', $fllwr_user_no = [])
    {
        $this->userId = $userId;
        $this->fllwr_user_no = $fllwr_user_no;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select('fllwr_user_no as user_id')
            ->join('TDM_USER', 'TDM_USER.user_no', '=', 'TCT_FLLW.fllwr_user_no')
            ->where('TDM_USER.delete_at', 'N')
            ->where('TDM_USER.drmncy_at', 'N')
            ->where('TCT_FLLW.user_no', $this->userId)
            ->orderBy('TCT_FLLW.regist_dt', 'desc');

        if(count($this->fllwr_user_no) > 0){
            $model = $model->whereIn('fllwr_user_no', $this->fllwr_user_no);
        }

        return $model;
    }
}
