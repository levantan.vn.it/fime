<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/1/2019
 * Time: 2:09 PM
 */

namespace App\Export;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class Tries implements FromArray, WithEvents, WithTitle, WithCustomStartCell, WithColumnFormatting
{
    use Exportable;

    /** @var array */
    public $arr_data;

    /** @var int */
    public $startRow = 2;

    public $length = 0;

    /** @var string  */
    public $title;

    public function __construct($arr_data, string $title)
    {
        $this->arr_data = $arr_data;
        $this->length = count($arr_data);
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function array(): array
    {
        // TODO: Implement array() method.
        return $this->arr_data;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        // TODO: Implement startCell() method.
        return 'A' . ($this->startRow);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        // TODO: Implement registerEvents() method.
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // Set heading
                $sheet = $event->sheet->getDelegate();
                $sheet->getCell('A' . ($this->startRow - 1))->setValue('ID');
                $sheet->getCell('B' . ($this->startRow - 1))->setValue('Product Name');
                $sheet->getCell('C' . ($this->startRow - 1))->setValue('Brand Number');
                $sheet->getCell('D' . ($this->startRow - 1))->setValue('Brand Name');
                $sheet->getCell('E' . ($this->startRow - 1))->setValue('Category');
                $sheet->getCell('F' . ($this->startRow - 1))->setValue('Model Number');
                $sheet->getCell('G' . ($this->startRow - 1))->setValue('Product URL');
                $sheet->getCell('H' . ($this->startRow - 1))->setValue('Free/Paid');
                $sheet->getCell('I' . ($this->startRow - 1))->setValue('Price');
                $sheet->getCell('J' . ($this->startRow - 1))->setValue('Start Date');
                $sheet->getCell('K' . ($this->startRow - 1))->setValue('End Date');
                $sheet->getCell('L' . ($this->startRow - 1))->setValue('Max-Participants');
                $sheet->getCell('M' . ($this->startRow - 1))->setValue('Delivery Start Date');
                $sheet->getCell('N' . ($this->startRow - 1))->setValue('Delivery End Date');
                $sheet->getCell('O' . ($this->startRow - 1))->setValue('Apply Status');


                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(30);
                $sheet->getColumnDimension('C')->setAutoSize(true);
                $sheet->getColumnDimension('D')->setAutoSize(true);
                $sheet->getColumnDimension('E')->setAutoSize(true);
                $sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(20);
                $sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(30);
                $sheet->getColumnDimension('H')->setAutoSize(true);
                $sheet->getColumnDimension('I')->setAutoSize(true);
                $sheet->getColumnDimension('J')->setAutoSize(true);
                $sheet->getColumnDimension('K')->setAutoSize(true);
                $sheet->getColumnDimension('L')->setAutoSize(true);
                $sheet->getColumnDimension('M')->setAutoSize(true);
                $sheet->getColumnDimension('N')->setAutoSize(true);
                $sheet->getColumnDimension('O')->setAutoSize(true);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'O' . ($this->startRow - 1))->getFont()->getColor()->setARGB(Color::COLOR_WHITE);

                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'O' . ($this->startRow - 1))->getFill()->setFillType('solid')->getStartColor()->setARGB('366092');

                $horizontal_left = array(
                    'alignment' => array(
                        'horizontal' => 'left',
                    )
                );
                $horizontal_center = array(
                    'alignment' => array(
                        'horizontal' => 'center',
                    )
                );

                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'O' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_left);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'O' . ($this->length + ($this->startRow - 1)))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $sheet->getStyle('C' . ($this->startRow - 1) . ':' . 'C' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_center);
                $sheet->getStyle('H' . ($this->startRow - 1) . ':' . 'O' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_center);
                $sheet->getStyle('A' . ($this->startRow - 1) . ':' . 'A' . ($this->length + ($this->startRow - 1)))->applyFromArray($horizontal_center);
            },
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        // TODO: Implement title() method.
        return $this->title;
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        // TODO: Implement columnFormats() method.
        return [
            'I' => NumberFormat::FORMAT_NUMBER,
            'L' => NumberFormat::FORMAT_NUMBER,
            'A' => NumberFormat::FORMAT_NUMBER,
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
