<?php

namespace App\Http\Controllers;

use App\Actions\Ad\AddAdAction;
use App\Actions\Ad\DeleteAdAction;
use App\Actions\Ad\GetAdByIDAction;
use App\Actions\Ad\GetAllAdsAction;
use App\Actions\Ad\GetAvailableAdsAction;
use App\Actions\Ad\ToggleAdAction;
use App\Actions\Ad\UpdateAdAction;
use App\Http\Requests\Ad\AddAdRequest;
use App\Http\Requests\Ad\DeleteAdRequest;
use App\Http\Requests\Ad\GeAdByIDRequest;
use App\Http\Requests\Ad\ToggleAdRequest;
use App\Http\Requests\Ad\UpdateAdRequest;

class AdsController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'available', 'getBy']]);
    }

    /**
     * @param GetAllAdsAction $action
     * @return array
     */
    public function index(GetAllAdsAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param GetAvailableAdsAction $action
     * @return array
     */
    public function available(GetAvailableAdsAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param GeAdByIDRequest $request
     * @param GetAdByIDAction $action
     * @return array
     */
    public function getBy(GeAdByIDRequest $request, GetAdByIDAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @param AddAdRequest $request
     * @param AddAdAction $action
     * @return array
     */
    public function store(AddAdRequest $request, AddAdAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateAdRequest $request
     * @param UpdateAdAction $action
     * @return array
     */
    public function update(UpdateAdRequest $request, UpdateAdAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param ToggleAdRequest $request
     * @param ToggleAdAction $action
     * @return array
     */
    public function toggle(ToggleAdRequest $request, ToggleAdAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }


    /**
     * @param DeleteAdRequest $request
     * @param DeleteAdAction $action
     * @return array
     */
    public function delete(DeleteAdRequest $request, DeleteAdAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }
}
