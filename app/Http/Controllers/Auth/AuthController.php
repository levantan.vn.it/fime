<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Auth\ForgotPassword;
use App\Actions\Auth\Login;
use App\Actions\Auth\LoginByFacebook;
use App\Actions\Auth\Logout;
use App\Actions\Auth\Register;
use App\Actions\Auth\ResetPassword;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\LoginByFacebookRequest;
use App\Http\Requests\Auth\LoginUserRequest;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\CoreRequest;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['login', 'register', 'loginByFacebook', 'resetPassword', 'forgotPassword']]);
    }
    /**
     * @SWG\Post(
     *      path="/auth/login",
     *      operationId="login",
     *      tags={"Auth"},
     *      summary="Login",
     *      description="Login",
     *      @SWG\Parameter(
     *          name="email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * @param LoginUserRequest $request
     * @param Login $action
     * @return array
     */
    public function login(LoginUserRequest $request, Login $action)
    {
        $data = $action->run($request->all(['email', 'password']));
        return $this->response($data);
    }

    /**
     * @SWG\Post(
     *      path="/auth/loginByFacebook",
     *      operationId="loginByFacebook",
     *      tags={"Auth"},
     *      summary="Login By Facebook",
     *      description="Login By Facebook",
     *      @SWG\Parameter(
     *          name="accessToken",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="userID",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * @param LoginByFacebookRequest $request
     * @param LoginByFacebook $action
     * @return array
     */
    public function loginByFacebook(LoginByFacebookRequest $request, LoginByFacebook $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param CoreRequest $request
     * @param Logout $action
     * @return array
     */
    public function logout(CoreRequest $request, Logout $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @SWG\Post(
     *      path="/auth/resetPassword",
     *      operationId="resetPassword",
     *      tags={"Auth"},
     *      summary="Reset password",
     *      description="Reset password",
     *      @SWG\Parameter(
     *          name="token",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * @param ResetPasswordRequest $request
     * @param ResetPassword $action
     * @return array
     */
    public function resetPassword(ResetPasswordRequest $request, ResetPassword $action)
    {
        $data = $action->run($request);
        return $this->response($data);
    }

    /**
     *  @SWG\Post(
     *      path="/auth/forgotPassword",
     *      operationId="forgotPassword",
     *      tags={"Auth"},
     *      summary="Forgot password",
     *      description="Forgot password",
     *      @SWG\Parameter(
     *          name="email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     * @param ForgotPasswordRequest $request
     * @param ForgotPassword $action
     * @return array
     */
    public function forgotPassword(ForgotPasswordRequest $request, ForgotPassword $action)
    {
        $data = $action->run($request);
        return $this->response($data);
    }

    /**
     * @SWG\Post(
     *      path="/auth/register",
     *      operationId="register",
     *      tags={"Auth"},
     *      summary="Register User",
     *      description="Register User",
     *      @SWG\Parameter(
     *          name="email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
 *          @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     * @param RegisterUserRequest $request
     * @param Register $action
     * @return array
     */
    public function register(RegisterUserRequest $request, Register $action)
    {
        $data = $action->run($request);
        return $this->response($data);
    }
}
