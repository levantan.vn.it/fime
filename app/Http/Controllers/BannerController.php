<?php

namespace App\Http\Controllers;

use App\Actions\Banner\AddBannerAction;
use App\Actions\Banner\DeleteBannerAction;
use App\Actions\Banner\GetAllAction;
use App\Actions\Banner\GetByAction;
use App\Actions\Banner\GetForFrontEnd;
use App\Actions\Banner\GetVisibleBannerAction;
use App\Actions\Banner\ToggleBannerAction;
use App\Actions\Banner\UpdateBannerAction;
use App\Http\Requests\Banner\AddBannerRequest;
use App\Http\Requests\Banner\DeleteBannerRequest;
use App\Http\Requests\Banner\GetByRequest;
use App\Http\Requests\Banner\ToggleBannerRequest;
use App\Http\Requests\Banner\UpdateBannerRequest;

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     schemes={"http", "https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Fime API",
 *         description="Fime API description",
 *         @SWG\Contact(
 *             email="nhan@netpower.no"
 *         ),
 *     )
 * )
 */
class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'available']]);
    }

    /**
     * @SWG\Get(
     *      path="/banners",
     *      operationId="index",
     *      tags={"Banners"},
     *      summary="Get all banners",
     *      description="Returns list of banners",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Returns list of banners
     * @param GetAllAction $action
     * @return array
     */
    public function index(GetVisibleBannerAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    public function get(GetForFrontEnd $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    public function getAll(GetAllAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }


    /**
     * Returns banner by id
     * @param GetByRequest $request
     * @param GetByAction $action
     * @return array
     */
    public function getBy(GetByRequest $request, GetByAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    public function store(AddBannerRequest $request, AddBannerAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateBannerRequest $request
     * @param UpdateBannerAction $action
     * @return array
     */
    public function update(UpdateBannerRequest $request, UpdateBannerAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param ToggleBannerRequest $request
     * @param ToggleBannerAction $action
     * @return array
     */
    public function toggle(ToggleBannerRequest $request, ToggleBannerAction $action)
    {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }


    /**
     * @param DeleteBannerRequest $request
     * @param DeleteBannerAction $action
     * @return array
     */
    public function deleteMulti(DeleteBannerRequest $request, DeleteBannerAction $action)
    {
        $data = $action->run($request->ids);
        return $this->response($data);
    }
}
