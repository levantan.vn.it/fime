<?php

namespace App\Http\Controllers;

use App\Actions\Blog\AddBlogAction;
use App\Actions\Blog\DeleteBlogAction;
use App\Actions\Blog\GetAllAction;
use App\Actions\Blog\GetAvailableAction;
use App\Actions\Blog\GetByAction;
use App\Actions\Blog\GetByPaginationAction;
use App\Actions\Blog\GetBySlugAction;
use App\Actions\Blog\GetLatestAction;
use App\Actions\Blog\GetTopViewAction;
use App\Actions\Blog\ToggleBlogAction;
use App\Actions\Blog\UpdateBlogAction;
use App\Http\Requests\Blog\AddBlogRequest;
use App\Http\Requests\Blog\DeleteBlogRequest;
use App\Http\Requests\Blog\GetByPaginationRequest;
use App\Http\Requests\Blog\GetByRequest;
use App\Http\Requests\Blog\GetBySlugRequest;
use App\Http\Requests\Blog\GetLatestRequest;
use App\Http\Requests\Blog\GetTopViewRequest;
use App\Http\Requests\Blog\ToggleBlogRequest;
use App\Http\Requests\Blog\UpdateBlogRequest;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'available', 'latest', 'getTopView', 'getBySlug']]);
    }
    /**
     * @param GetAllAction $action
     * @return array
     */
    public function index(GetAllAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param GetByPaginationRequest $request
     * @param GetByPaginationAction $action
     * @return array
     */
    public function getByPagination(GetByPaginationRequest $request, GetByPaginationAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }


    /**
     * @param GetByRequest $request
     * @param GetByAction $action
     * @return array
     */
    public function getBy(GetByRequest $request, GetByAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @param AddBlogRequest $request
     * @param AddBlogAction $action
     * @return array
     */
    public function store(AddBlogRequest $request, AddBlogAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateBlogRequest $request
     * @param UpdateBlogAction $action
     * @return array
     */
    public function update(UpdateBlogRequest $request, UpdateBlogAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param ToggleBlogRequest $request
     * @param ToggleBlogAction $action
     * @return array
     */
    public function toggle(ToggleBlogRequest $request, ToggleBlogAction $action)
    {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }


    /**
     * @param DeleteBlogRequest $request
     * @param DeleteBlogAction $action
     * @return array
     */
    public function deleteMulti(DeleteBlogRequest $request, DeleteBlogAction $action)
    {
        $data = $action->run($request->ids);
        return $this->response($data);
    }

    /**
     * @param GetAvailableAction $action
     * @return array
     */
    public function available(GetAvailableAction $action) {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param GetLatestRequest $request
     * @param GetLatestAction $action
     * @return array
     */
    public function latest(GetLatestRequest $request, GetLatestAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param GetTopViewRequest $request
     * @param GetTopViewAction $action
     * @return array
     */
    public function getTopView(GetTopViewRequest $request, GetTopViewAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param GetBySlugRequest $request
     * @param GetBySlugAction $action
     * @return array
     */
    public function getBySlug(GetBySlugRequest $request, GetBySlugAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }
}
