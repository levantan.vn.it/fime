<?php

namespace App\Http\Controllers;

use App\Actions\Brand\AddBrandAction;
use App\Actions\Brand\DeleteBrandAction;
use App\Actions\Brand\GetAllAction;
use App\Actions\Brand\GetByAction;
use App\Actions\Brand\UpdateBrandAction;
use App\Http\Requests\Brand\AddBrandRequest;
use App\Http\Requests\Brand\GetByRequest;
use App\Http\Requests\Brand\DeleteBrandRequest;
use App\Http\Requests\Brand\UpdateBrandRequest;

class BrandController extends Controller
{
    public function __construct()
    {
//        $this->middleware('jwt.auth', ['except' => ['index']]);
    }
    /**
     * @param GetAllAction $action
     * @return array
     */
    public function index(GetAllAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param GetByRequest $request
     * @param GetByAction $action
     * @return array
     */
    public function getBy(GetByRequest $request, GetByAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @param AddBrandRequest $request
     * @param AddBrandAction $action
     * @return array
     */
    public function store(AddBrandRequest $request, AddBrandAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateBrandRequest $request
     * @param UpdateBrandAction $action
     * @return array
     */
    public function update(UpdateBrandRequest $request, UpdateBrandAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param DeleteBrandRequest $request
     * @param DeleteBrandAction $action
     * @return array
     */
    public function delete(DeleteBrandRequest $request, DeleteBrandAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }
}
