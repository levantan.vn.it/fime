<?php

namespace App\Http\Controllers;

use App\Actions\Category\AddCategoryAction;
use App\Actions\Category\DeleteCategoryAction;
use App\Actions\Category\GetAllAction;
use App\Actions\Category\UpdateCategoryAction;

use App\Http\Requests\Category\AddCategoryRequest;
use App\Http\Requests\Category\DeleteCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;

class CategoryController extends Controller
{
    public function __construct()
    {
//        $this->middleware('jwt.auth', ['except' => ['index']]);
    }
    /**
     * @param GetAllAction $action
     * @return array
     */
    public function index(GetAllAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param AddCategoryRequest $request
     * @param AddCategoryAction $action
     * @return array
     */
    public function store(AddCategoryRequest $request, AddCategoryAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateCategoryRequest $request
     * @param UpdateCategoryAction $action
     * @return array
     */
    public function update(UpdateCategoryRequest $request, UpdateCategoryAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param DeleteCategoryRequest $request
     * @param DeleteCategoryAction $action
     * @return array
     */
    public function delete(DeleteCategoryRequest $request, DeleteCategoryAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }
}
