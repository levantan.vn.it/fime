<?php

namespace App\Http\Controllers;

use App\Actions\Ad\AddAdAction;
use App\Actions\Ad\DeleteAdAction;
use App\Actions\Ad\GetAdByIDAction;
use App\Actions\Ad\GetAllAdsAction;
use App\Actions\Ad\GetAvailableAdsAction;
use App\Actions\Ad\ToggleAdAction;
use App\Actions\Ad\UpdateAdAction;
use App\Actions\Hashtag\GetHashtagsAction;
use App\Http\Requests\Ad\AddAdRequest;
use App\Http\Requests\Ad\DeleteAdRequest;
use App\Http\Requests\Ad\GeAdByIDRequest;
use App\Http\Requests\Ad\ToggleAdRequest;
use App\Http\Requests\Ad\UpdateAdRequest;

class HashtagController extends Controller
{
    public function __construct()
    {
    }

    public function getTopHashtags(GetHashtagsAction $action) {
        $data = $action->run();
        return $this->response($data);
    }
}
