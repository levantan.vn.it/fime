<?php

namespace App\Http\Controllers;

use App\Actions\Notification\GetNotificationNumberByUserIdAction;
use App\Actions\Notification\GetNotificationsByUserIdAction;
use App\Actions\Notification\MarkAllNotificationSeenAction;
use App\Actions\Notification\MarkNotificationSeenAction;
use App\Http\Requests\Notification\GetNotificationByUserId;
use App\Http\Requests\Notification\MarkNotificationAsSeenRequest;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => []]);
    }

    /**
     * @param GetNotificationByUserId $request
     * @param GetNotificationsByUserIdAction $action
     * @return array
     */
    public function getByUserId(GetNotificationByUserId $request, GetNotificationsByUserIdAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param GetNotificationNumberByUserIdAction $action
     * @return array
     */
    public function getNotificationNumber(GetNotificationNumberByUserIdAction $action) {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param MarkNotificationAsSeenRequest $request
     * @param MarkNotificationSeenAction $action
     * @return array
     */
    public function markNotificationAsSeen(MarkNotificationAsSeenRequest $request, MarkNotificationSeenAction $action) {
        $data = $action->run($request->ids);
        return $this->response($data);
    }

    public function markAllAsSeen(MarkAllNotificationSeenAction $action) {
        $data = $action->run();
        return $this->response($data);
    }
}
