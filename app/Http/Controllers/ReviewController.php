<?php

namespace App\Http\Controllers;

use App\Actions\Review\CreateReviewAction;
use App\Actions\Review\DeleteReviewAction;
use App\Actions\Review\GetAllReviewAction;
use App\Actions\Review\GetPaginateAfterReviewAction;
use App\Actions\Review\GetPaginateReviewAction;
use App\Actions\Review\GetReviewByIDAction;
use App\Actions\Review\GetReviewByTypeAction;
use App\Actions\Review\GetReviewDetailAction;
use App\Actions\Review\TogglePopularReviewAction;
use App\Actions\Review\ToggleReviewAction;
use App\Actions\Review\UpdateReviewAction;
use App\Http\Requests\CoreRequest;
use App\Http\Requests\Review\CreateReviewRequest;
use App\Http\Requests\Review\DeleteReviewRequest;
use App\Http\Requests\Review\GetReviewByIDRequest;
use App\Http\Requests\Review\GetReviewByTypeRequest;
use App\Http\Requests\Review\GetReviewPagingRequest;
use App\Http\Requests\Review\ToggleReviewRequest;
use App\Http\Requests\Review\UpdateReviewRequest;

class ReviewController extends Controller
{

    /**
     * ReviewController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'get', 'getNew', 'getReviewByType', 'getDetail', 'getByHashtag', 'getNewAfter']]);
    }

    public function index(CoreRequest $request, GetAllReviewAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param CreateReviewRequest $request
     * @param CreateReviewAction $action
     * @return array
     */
    public function store(CreateReviewRequest $request, CreateReviewAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param ToggleReviewRequest $request
     * @param ToggleReviewAction $action
     * @return array
     */
    public function toggle(ToggleReviewRequest $request, ToggleReviewAction $action) {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }

    public function togglePopular(ToggleReviewRequest $request, TogglePopularReviewAction $action) {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }

    /**
     * @param DeleteReviewRequest $request
     * @param DeleteReviewAction $action
     * @return array
     */
    public function delete(DeleteReviewRequest $request, DeleteReviewAction $action) {
        $data = $action->run($request->ids);
        return $this->response($data);
    }

    /**
     * @param GetReviewByIDRequest $request
     * @param GetReviewByIDAction $action
     * @return array
     */
    public function get(GetReviewByIDRequest $request, GetReviewByIDAction $action) {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @param GetReviewByTypeRequest $request
     * @param GetReviewByTypeAction $action
     * @return array
     */
    public function getReviewByType(GetReviewByTypeRequest $request, GetReviewByTypeAction $action) {
        $data = $action->run($request->type);
        return $this->response($data);
    }

    /**
     * @param GetReviewPagingRequest $request
     * @param GetPaginateReviewAction $action
     * @return array
     */
    public function getNew(GetReviewPagingRequest $request, GetPaginateReviewAction $action) {
        $data = $action->run($request);
        return $this->response($data);
    }

    public function getNewAfter(GetReviewPagingRequest $request, GetPaginateAfterReviewAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param GetReviewByIDRequest $request
     * @param GetReviewDetailAction $action
     * @return array
     */
    public function getDetail(GetReviewByIDRequest $request, GetReviewDetailAction $action) {
        $data = $action->run($request->slug);
        return $this->response($data);
    }

    /**
     * @param UpdateReviewRequest $request
     * @param UpdateReviewAction $action
     * @return array
     */
    public function update(UpdateReviewRequest $request, UpdateReviewAction $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }
}
