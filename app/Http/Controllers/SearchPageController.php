<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 3/6/2019
 * Time: 2:17 PM
 */

namespace App\Http\Controllers;


use App\Actions\TryFree\SearchTries;
use App\Actions\Users\SearchFimers;
use App\Http\Requests\CoreRequest;

class SearchPageController extends Controller
{
    public function getTries(CoreRequest $request, SearchTries $action)
    {
        return $this->response($action->run($request));
    }

    public function getFimers(CoreRequest $request, SearchFimers $action)
    {
        return $this->response($action->run($request));
    }
}
