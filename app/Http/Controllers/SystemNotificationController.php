<?php

namespace App\Http\Controllers;

use App\Actions\SystemNotification\AddSystemNotificationAction;
use App\Actions\SystemNotification\DeleteSystemNotificationAction;
use App\Actions\SystemNotification\GetAllSystemNotificationAction;
use App\Actions\SystemNotification\GetSystemNotificationByIDAction;
use App\Actions\SystemNotification\ToggleSystemNotificationAction;
use App\Actions\SystemNotification\UpdateSystemNotificationAction;
use App\Http\Requests\SystemNotification\AddSystemNotificationRequest;
use App\Http\Requests\SystemNotification\DeleteSystemNotificationRequest;
use App\Http\Requests\SystemNotification\GetSystemNotificationByIDRequest;
use App\Http\Requests\SystemNotification\ToggleSystemNotificationRequest;
use App\Http\Requests\SystemNotification\UpdateSystemNotificationRequest;

class SystemNotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'getById']]);
    }

    /**
     * @param AddSystemNotificationRequest $request
     * @param AddSystemNotificationAction $action
     * @return array
     */
    public function store(AddSystemNotificationRequest $request, AddSystemNotificationAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param GetAllSystemNotificationAction $action
     * @return array
     */
    public function index(GetAllSystemNotificationAction $action) {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @param ToggleSystemNotificationRequest $request
     * @param ToggleSystemNotificationAction $action
     * @return array
     */
    public function toggle(ToggleSystemNotificationRequest $request, ToggleSystemNotificationAction $action) {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }

    /**
     * @param DeleteSystemNotificationRequest $request
     * @param DeleteSystemNotificationAction $action
     * @return array
     */
    public function delete(DeleteSystemNotificationRequest $request, DeleteSystemNotificationAction $action) {
        $data = $action->run($request->ids);
        return $this->response($data);
    }

    /**
     * @param GetSystemNotificationByIDRequest $request
     * @param GetSystemNotificationByIDAction $action
     * @return array
     */
    public function getById(GetSystemNotificationByIDRequest $request, GetSystemNotificationByIDAction $action) {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @param UpdateSystemNotificationRequest $request
     * @param UpdateSystemNotificationAction $action
     * @return array
     */
    public function update(UpdateSystemNotificationRequest $request, UpdateSystemNotificationAction  $action) {
        $data = $action->run($request->all());
        return $this->response($data);
    }
}
