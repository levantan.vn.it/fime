<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 4:39 PM
 */

namespace App\Http\Controllers;


use App\Actions\Tip\GetAllTipsAction;
use App\Actions\Tip\GetByIDAction;
use App\Actions\Tip\GetTopViewAction;
use App\Http\Requests\CoreRequest;

class TipController extends Controller
{
    public function getAll(GetAllTipsAction $action)
    {
        return $this->response($action->run());
    }

    public function getByID(CoreRequest $coreRequest, GetByIDAction $action)
    {
        return $this->response($action->run($coreRequest->id));
    }

    public function getTopView(CoreRequest $coreRequest, GetTopViewAction $action)
    {
        return $this->response($action->run());
    }
}
