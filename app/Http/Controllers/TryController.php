<?php

namespace App\Http\Controllers;

use App\Actions\TryFree\AddTryAction;
use App\Actions\TryFree\DeleteTryAction;
use App\Actions\TryFree\GetAllAction;
use App\Actions\TryFree\GetAvailableTryAction;
use App\Actions\TryFree\GetByAction;
use App\Actions\TryFree\GetByPaginationAction;
use App\Actions\TryFree\GetBySlugAction;
use App\Actions\TryFree\GetTriesAction;
use App\Actions\TryFree\ToggleTryAction;
use App\Actions\TryFree\UpdateTryAction;
use App\Http\Requests\TryFree\AddTryRequest;
use App\Http\Requests\TryFree\DeleteTryRequest;
use App\Http\Requests\TryFree\GetByPaginationRequest;
use App\Http\Requests\TryFree\GetByRequest;
use App\Http\Requests\TryFree\GetBySlugRequest;
use App\Http\Requests\TryFree\GetTriesRequest;
use App\Http\Requests\TryFree\ToggleTryRequest;
use App\Http\Requests\TryFree\UpdateTryRequest;

class TryController extends Controller
{
    public function __construct()
    {
        //$this->middleware('jwt.auth', ['except' => ['getAvailableTries', 'getAllTries', 'getBySlug']]);
    }

    public function index(GetAllAction $action)
    {
        $data = $action->run();
        
        return $this->response($data);
    }

    public function getByPagination(GetByPaginationRequest $request, GetByPaginationAction $action)
    {
        $data = $action->run($request->all());

        return $this->response($data);
    }

    public function store(AddTryRequest $request, AddTryAction $action)
    {
        $data = $action->run($request->all());

        return $this->response($data);
    }

    /**
     * @param GetByRequest $request
     * @param GetByAction $action
     * @return array
     */
    public function getBy(GetByRequest $request, GetByAction $action)
    {
        $data = $action->run($request->id);
        return $this->response($data);
    }

    /**
     * @SWG\Get(
     *      path="/tries/slug?slug={slug}",
     *      operationId="getBySlug",
     *      tags={"Try"},
     *      summary="get try by slug",
     *      description="Returns try",
     *      @SWG\Parameter(
     *          name="slug",
     *          description="Slug",
     *          required=true,
     *          type="string",
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"jwt": {}}
     *       }
     *     )
     *
     * @param GetBySlugRequest $request
     * @param GetBySlugAction $action
     * @return array
     */
    public function getBySlug(GetBySlugRequest $request, GetBySlugAction $action)
    {
        $data = $action->run($request->slug);
        return $this->response($data);
    }

    /**
     * @SWG\Get(
     *      path="/tries/getAvailableTries",
     *      operationId="getAvailableTries",
     *      tags={"Try"},
     *      summary="get Available Tries",
     *      description="Returns try",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"jwt": {}}
     *       }
     *     )
     *
     * @param GetAvailableTryAction $action
     * @return array
     */
    public function getAvailableTries(GetAvailableTryAction $action)
    {
        $data = $action->run();
        return $this->response($data);
    }

    /**
     * @SWG\Get(
     *      path="/tries/all?page={page}&period={period}",
     *      operationId="getAllTries",
     *      tags={"Try"},
     *      summary="Get tries",
     *      description="Returns tries",
     *      @SWG\Parameter(
     *          name="period",
     *          description="Period: 0 - All, 1 - Inprogress, 2 - Coming soon , 3 - Done",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *     @SWG\Parameter(
     *          name="page",
     *          description="Page Index for pagination",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"jwt": {}}
     *       }
     *     )
     *
     * @param GetTriesRequest $request
     * @param GetTriesAction $action
     * @return array
     */
    public function getAllTries(GetTriesRequest $request,GetTriesAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    /**
     * @param UpdateTryRequest $request
     * @param UpdateTryAction $action
     * @return array
     */
    public function update(UpdateTryRequest $request, UpdateTryAction $action)
    {
        $data = $action->run($request->all());
        return $this->response($data);
    }

    public function toggle(ToggleTryRequest $request, ToggleTryAction $action)
    {
        $data = $action->run($request->ids, $request->toggle);
        return $this->response($data);
    }

    public function deleteMulti(DeleteTryRequest $request, DeleteTryAction $action)
    {
        $data = $action->run($request->ids);
        return $this->response($data);
    }
}
