<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Image::configure(array('driver' => public_path('data')));


class UploadController extends Controller
{
    private $MAX_UPLOAD_SIZE;

    public function __construct()
    {
        $this->MAX_UPLOAD_SIZE = config('MAX_UPLOAD_SIZE', 5) * 1024 * 1024;
    }

    public function store(Request $request)
    {
        try {
            $now = date('Ymd');
            if ($request->hasFile('video')) {
                $video = $request->file('video');
                $file_name = $this->generateRandomString() . '.' . $video->extension();
                $video->move('/data/videos', $file_name);
                return ['result' => ['name' => $file_name, 'url' => '/data/videos']];
            } else if ($request->input('files')) {
                $files = $request->input('files');
                $images = [];
                foreach ($files as $key => $file) {
                    if (($file['base64'] == '' || $file['type'] == '') && $file['url'] != '') {
                        $path_names = explode('/', $file['url']);
                        $file_name = $path_names[count($path_names) - 1];
                        $file_path = '';
                        for ($i = 3; $i < count($path_names) - 1; $i++) {
                            $file_path = $file_path . '/' .$path_names[$i];
                        }
                        $file_thumb_name = isset($file['thumb_name']) ? $file['thumb_name'] : '';
                        array_push($images, ['name' => $file_name, 'url' => $file_path, 'thumb_name' => $file_thumb_name]);
                    } else {
                        $file_data = $file['base64'];
                        @list($type, $file_data) = explode(';base64,', $file_data);
                        @list(, $extension) = explode('/', $type);
                        $randomName = $this->generateRandomString();
                        $file_name = $randomName . '.' . $extension;
                        $file_thumb_name = $randomName . '_TMB' . '.' . $extension;
                        $file_original_name = $randomName . '_ORIGINAL' . '.' . $extension;
                        \Storage::disk('data')->put('images/upload/' .$now . '/' . $file_original_name, base64_decode($file_data));
                        $uri = 'data://application/octet-stream;base64,'  . $file_data;
                        $size_info = getimagesize($uri);
                        $width = $size_info[0];
                        $heigth = $size_info[1];
                        \Image::make($file_data)->resize(400, 400 * $heigth / $width)->save((public_path().'/data/images/upload/' .$now . '/' . $file_thumb_name));
                        \Image::make($file_data)->resize(800, 800 * $heigth / $width)->save((public_path().'/data/images/upload/' .$now . '/' . $file_name));
                        array_push($images, ['name' => $file_name, 'url' => '/data/images/upload/' .$now, 'thumb_name' => $file_thumb_name]);
                    }
                }
                return ['result' => ['images' => $images]];
            } else if ($request->hasFile('images')) {
                $image = $request->file('images');
                $file_name = $this->generateRandomString() . '.' . $image->extension();
                $image->move('/data/images/upload' .$now, $file_name);
                return ['result' => '/data/images/upload' .$now . '/' . $file_name];
            } else {
                $file_data = $request->input('file');
                @list($type, $file_data) = explode(';base64,', $file_data);
                @list(, $extension) = explode('/', $type);
                $randomName = $this->generateRandomString();
                $file_name = $randomName . '.' . $extension;
                $file_thumb_name = $randomName . '_TMB' . '.' . $extension;
                $file_original_name = $randomName . '_ORIGINAL' . '.' . $extension;
                \Storage::disk('data')->put('images/upload/' .$now . '/' . $file_original_name,  base64_decode($file_data));
                $uri = 'data://application/octet-stream;base64,'  . $file_data;
                $size_info = getimagesize($uri);
                $width = $size_info[0];
                $heigth = $size_info[1];
                \Image::make($file_data)->resize(400, 400 * $heigth / $width)->save((public_path().'/data/images/upload/' .$now . '/' . $file_thumb_name));
                \Image::make($file_data)->resize(800, 800 * $heigth / $width)->save((public_path().'/data/images/upload/' .$now . '/' . $file_name));
                return ['result' => ['name' => $file_name, 'url' => '/data/images/upload/' .$now, 'thumb_name' => 'data/images/upload/' .$now . '/' . $file_thumb_name]];
            }

        } catch (\Exception $ex) {
            \Log::error($ex);

            return ['result' => [
                'error' => true,
                'message' => $ex->getMessage()
            ]];
        }
    }

    private function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

}
