<?php

namespace App\Http\Controllers;

use App\Actions\User\ChangePasswordAction;
use App\Actions\User\User;
use App\Actions\Users\GetByIdsAction;
use App\Actions\Users\SearchUserByNameAction;
use App\Http\Requests\CoreRequest;
use App\Http\Requests\User\ChangePasswordRequest;
use App\Http\Requests\User\GetByIdsRequest;
use App\Http\Requests\User\SearchByNameRequest;

class UserController extends Controller
{
    /**
     * @param CoreRequest $request
     * @param User $action
     * @return array
     */
    public function get(CoreRequest $request, User $action)
    {
        return $this->response($action->getUserById((string)$request->user_no));
    }

    /**
     * @param CoreRequest $request
     * @param User $action
     * @return array
     */
    public function getUserByEmail(CoreRequest $request, User $action)
    {
        return $this->response($action->getUserByEmail((string)$request->get('email')));
    }

    /**
     * @param CoreRequest $request
     * @param User $action
     * @return array
     */
    public function delete(CoreRequest $request, User $action)
    {
        return $this->response($action->deleteUser((string)$request->user_no));
    }

    /**
     * @param CoreRequest $request
     * @param User $action
     * @return array
     */
    public function updateStatus(CoreRequest $request, User $action){
        return $this->response($action->updateStatus($request->all()));
    }

    /**
     * @param CoreRequest $request
     * @param User $action
     * @return array
     */
    public function update(CoreRequest $request, User $action){
        return $this->response($action->update($request->all()));
    }

    public function store(CoreRequest $request, User $action)
    {
        return $this->response($action->add($request->all()));
    }


    public function searchByName(SearchByNameRequest $request, SearchUserByNameAction $action){
        return $this->response($action->run($request->term));
    }

    public function getByIds(GetByIdsRequest $request, GetByIdsAction $action){
        return $this->response($action->run($request->ids));
    }

    public function changePassword(ChangePasswordRequest $request, ChangePasswordAction $action) {
        return $this->response($action->run($request->all()));
    }
}
