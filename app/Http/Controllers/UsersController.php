<?php

namespace App\Http\Controllers;

use App\Actions\Users\Fimers;
use App\Actions\Users\GetUserProfileAction;
use App\Actions\Users\HotFimers;
use App\Actions\Users\Users;
use App\Http\Requests\CoreRequest;

class UsersController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('jwt.auth', ['except' => ['getFimers', 'getHotFimers', 'getList']]);
//    }

    public function getUserProfile(GetUserProfileAction $action){
        return $this->response($action->run());
    }

    public function getList(CoreRequest $request, Users $action){
        return $this->response($action->getList($request));
    }

    public function getFimers(CoreRequest $request, Fimers $action){
        $data = $action->run($request->filterType);
        return $this->response($data);
    }

    public function getHotFimers(CoreRequest $request, HotFimers $action){
        $data = $action->run($request->type);
        return $this->response($data);
    }
}
