<?php

namespace App\Models;

class Ads extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ads';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'url', 'target_url', 'order', 'resource_type', 'show_on_frontend',
        'target_type', 'is_disabled', 'created_by', 'updated_by', 'deleted_by'];
}
