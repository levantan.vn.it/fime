<?php

namespace App\Models;


class FAQ extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TCT_FAQ';

    protected $connection = 'aws';

    protected  $primaryKey = 'cntnts_no';

    public $timestamps = false;

    public $incrementing = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cntnts_no', 'sj', 'cn', 'faq_se_code'];

}
