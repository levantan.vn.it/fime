<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
    protected $connection = 'aws';

    protected $table = 'TCT_POPHASH';

    public $timestamps = false;

//    public $incrementing = false;

    public $primaryKey = 'hash_seq';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['hash_seq', 'hash_tag', 'hash_cnt', 'hash_dt'];
}
