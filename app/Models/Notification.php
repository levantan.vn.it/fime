<?php

namespace App\Models;

class Notification extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'url', 'user_id', 'reference_user_id', 'is_seen', 'guid', 'created_at',
        'updated_at', 'deleted_at', 'type', 'is_system', 'is_disabled', 'posted_at', 'object_id', 'object_type'];
}
