<?php

namespace App\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Review extends BaseModel
{
    protected $connection = 'aws';

    use HasSlug;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TCT_REVIEW';

    /**
     * @var string
     */
    protected  $primaryKey = 'review_no';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['review_no', 'cntnts_no', 'slug', 'review_dc', 'review_short', 'writng_dt', 'updt_dt', 'delete_at', 'expsr_at', 'pblonsip_at', 'pblonsip_snts', 'pblonsip_time', 'review_no', 'user_no', 'goods_cl_code', 'goods_nm', 'm_cnt', 'p_cnt'];

    /**
     * @return SlugOptions
     */
    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom('goods_nm')
            ->saveSlugsTo('slug');
    }
}
