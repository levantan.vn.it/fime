<?php

namespace App\Models;

class ReviewComment extends BaseModel
{
    protected $connection = 'aws';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TCT_REVIEW_ANWR';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     * object_type is 'Try' or 'Review'
     * object_type is id of 'Try' or 'Review'
     * @var array
     */
    protected $fillable = [ 'review_no', 'parent_id', 'user_no', 'anwr_sn', 'anwr_writng_dt', 'anwr_cn', 'expsr_at', 'delete_at'];

}
