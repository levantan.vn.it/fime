<?php

namespace App\Models;

class ReviewLike extends BaseModel
{
    protected $connection = 'aws';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TCT_REVIEW_RECM';


    protected $primaryKey = ['review_no', 'user_no', 'recomend_dt'];

    public $incrementing = false;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     * object_type is 'Try' or 'Review'
     * object_type is id of 'Try' or 'Review'
     * @var array
     */
    protected $fillable = ['review_no', 'recomend_dt', 'user_no'];

}
