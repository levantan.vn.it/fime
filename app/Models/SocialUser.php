<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class SocialUser extends BaseModel
{
    use SoftDeletes;

    protected $table = 'social_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'provider', 'provider_user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
