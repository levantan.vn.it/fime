<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 3:58 PM
 */

namespace App\Models;


class Tips extends BaseModel
{
    protected $connection = 'aws';

    protected $table = 'TCT_TIPS';

    protected  $primaryKey = 'ti_id';

    protected $fillable = ["ti_id", "subject", "display_yn", "cover_img1", "cover_img2", "cover_img3", "cover_img4", "tips_desc_head", "tips_desc_body", "cont1_type", "cont1_photo", "cont1_movie", "cont1_link_type", "cont1_link_url", "caption_text", "source_site_name", "source_site_url", "cont1_head", "cont1_body", "cont2_type", "cont2_photo", "cont2_movie", "cont2_link_type", "cont2_link_url", "caption_text2", "source_site_name2", "source_site_url2", "cont2_head", "cont2_body", "hot_fimer1", "hot_fimer2", "hot_fimer3", "hot_fimer4", "hot_fimer5", "hot_fimer6", "img_url", "mov_url", "created_at", "updated_at"];
}
