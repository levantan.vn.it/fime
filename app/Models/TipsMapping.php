<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/3/2019
 * Time: 4:50 PM
 */

namespace App\Models;


class TipsMapping extends BaseModel
{
    protected $connection = 'aws';

    protected $table = 'TCT_TIPS_MAPPING';

    protected  $primaryKey = 'tm_id';

    public $timestamps = false;

    protected $fillable = ['tm_id', 'content_gb', 'ti_id', 'content_id', 'tm_no'];
}
