<?php

namespace App\Models;

class TryFree extends BaseModel
{
    protected $connection = 'aws';

    protected  $primaryKey = 'cntnts_no';

    protected $table = 'TCT_GOODS';

    public $timestamps = false;

    public $incrementing = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cntnts_no', 'modl_nombr', 'brnd_nm', 'brnd_code', 'event_knd_code', 'event_bgnde',
        'event_endde', 'dlvy_bgnde', 'dlvy_endde', 'event_trgter_co', 'goods_dc', 'goods_pc', 'event_pc',
        'time_color_code', 'slctn_compt_at', 'link_url', 'border_at', 'hash_tag', 'brnd_code', 'goods_cl_code',
        'goods_txt_code', 'goods_txt', 'view_cnt', 'p_cnt', 'm_cnt', 'sale_price', 'short_desc', 'resource_type',
        'is_try_event', 'try_event_type', 'quantity_to_qualify'];

}
