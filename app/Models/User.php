<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
//    use SoftDeletes;

    use HasSlug;
    use HasRoles;
    protected $connection = 'aws';

    public $enableCreated =  false;

    public $guard_name = 'api';

	protected $table = 'TDM_USER';

	protected $primaryKey = 'user_no';

	protected $dates = [];

    public $incrementing = false;

    public  $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_no',
        'type',
        'status',
        'reg_name',
        'nick_name',
        'id',
        'hint_qst',
        'hint_qst_answer',
        'email',
        'home_zip',
        'home_addr1',
        'home_tel',
        'cellphone',
        'sms',
        'real_name',
        'self_intro',
        'pic',
        'open',
        'reg_dt',
        'mod_dt',
        'password_change_de',
        'password_initl_at',
        'sns_knd_code',
        'fllwr_co',
        'sns_id',
        'accml_pntt',
        'drmncy_at',
        'delete_at',
        'login_co',
        'last_login_dt',
        'link_url',
        'try_click_dt',
        'review_click_dt',
        'birthday',
        'gender',
        'p_cnt',
        'm_cnt',
        'push_get_yn',
        'slug',
        'allow_review',
        'allow_comment',
        'role_id',
        'password',
        'deleted_at',
        'updated_at',
        'hot_fimer_7days',
        'hot_fimer_30days',
        'hot_fimer_90days',
        'hot_fimer_1year',
        'reviews'
    ];

    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom('id')
            ->saveSlugsTo('slug');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
