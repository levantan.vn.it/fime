<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class UserFollow extends BaseModel
{
    protected $connection = 'aws';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TCT_FLLW';

    public $primaryKey = ['user_no', 'fllwr_user_no'];

    public $incrementing = false;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_no', 'fllwr_user_no', 'regist_dt'];
}
