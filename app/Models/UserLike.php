<?php

namespace App\Models;

class UserLike extends BaseModel
{
    protected $connection = 'aws';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TCT_GOODS_RECM';


    protected $primaryKey = ['cntnts_no', 'user_no'];

    public $incrementing = false;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     * object_type is 'Try' or 'Review'
     * object_type is id of 'Try' or 'Review'
     * @var array
     */
    protected $fillable = ['cntnts_no', 'recomend_dt', 'user_no'];

}
