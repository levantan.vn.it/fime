<?php

namespace App\Providers;

use App\Contracts\AdsRepositoryInterface;
use App\Contracts\BannerRepositoryInterface;
use App\Contracts\BlogRepositoryInterface;
use App\Contracts\CodeRepositoryInterface;
use App\Contracts\CommentRepositoryInterface;
use App\Contracts\FAQCategoryRepositoryInterface;
use App\Contracts\FAQRepositoryInterface;
use App\Contracts\FilesRepositoryInterface;
use App\Contracts\HashtagRepositoryInterface;
use App\Contracts\MainContRepositoryInterface;
use App\Contracts\NotificationRepositoryInterface;
use App\Contracts\PasswordResetRepositoryInterface;
use App\Contracts\ReviewCommentRepositoryInterface;
use App\Contracts\ReviewFilesRepositoryInterface;
use App\Contracts\ReviewHashTagsRepositoryInterface;
use App\Contracts\ReviewLikeRepositoryInterface;
use App\Contracts\ReviewRepositoryInterface;
use App\Contracts\RoleRepositoryInterface;
use App\Contracts\SettingRepositoryInterface;
use App\Contracts\SocialUserRepositoryInterface;
use App\Contracts\SystemNotificationRepositoryInterface;
use App\Contracts\TipsMappingRepositoryInterface;
use App\Contracts\TipsRepositoryInterface;
use App\Contracts\TipViewsRepositoryInterface;
use App\Contracts\TryExtRepositoryInterface;
use App\Contracts\TryFilesRepositoryInterface;
use App\Contracts\TryRepositoryInterface;
use App\Contracts\UserFollowRepositoryInterface;
use App\Contracts\UserLikeRepositoryInterface;
use App\Contracts\UserPointRepositoryInterface;
use App\Contracts\UserRepositoryInterface;
use App\Contracts\UserTryRepositoryInterface;
use App\Repositories\AdsRepository;
use App\Repositories\BannerRepository;
use App\Repositories\BlogRepository;
use App\Repositories\CodeRepository;
use App\Repositories\CommentRepository;
use App\Repositories\FAQCategoryRepository;
use App\Repositories\FAQRepository;
use App\Repositories\FilesRepository;
use App\Repositories\HashtagRepository;
use App\Repositories\MainContRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\PasswordResetRepository;
use App\Repositories\ReviewCommentRepository;
use App\Repositories\ReviewFilesRepository;
use App\Repositories\ReviewHashTagsRepository;
use App\Repositories\ReviewLikeRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\RoleRepository;
use App\Repositories\SettingRepository;
use App\Repositories\SocialUserRepository;
use App\Repositories\SystemNotificationRepository;
use App\Repositories\TipsMappingRepository;
use App\Repositories\TipsRepository;
use App\Repositories\TipViewsRepository;
use App\Repositories\TryExtRepository;
use App\Repositories\TryFilesRepository;
use App\Repositories\TryRepository;
use App\Repositories\UserFollowRepository;
use App\Repositories\UserLikeRepository;
use App\Repositories\UserPointRepository;
use App\Repositories\UserRepository;
use App\Repositories\UserTryRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind(TryRepositoryInterface::class, TryRepository::class);
        App::bind(UserRepositoryInterface::class, UserRepository::class);
        App::bind(BannerRepositoryInterface::class, BannerRepository::class);
        App::bind(PasswordResetRepositoryInterface::class, PasswordResetRepository::class);
        App::bind(SocialUserRepositoryInterface::class, SocialUserRepository::class);
        App::bind(BlogRepositoryInterface::class, BlogRepository::class);
        App::bind(AdsRepositoryInterface::class, AdsRepository::class);
        App::bind(ReviewRepositoryInterface::class, ReviewRepository::class);
        App::bind(RoleRepositoryInterface::class, RoleRepository::class);
        App::bind(FAQRepositoryInterface::class, FAQRepository::class);
        App::bind(FAQCategoryRepositoryInterface::class, FAQCategoryRepository::class);
        App::bind(CommentRepositoryInterface::class, CommentRepository::class);
        App::bind(ReviewCommentRepositoryInterface::class, ReviewCommentRepository::class);
        App::bind(HashtagRepositoryInterface::class, HashtagRepository::class);
        App::bind(SettingRepositoryInterface::class, SettingRepository::class);
        App::bind(UserLikeRepositoryInterface::class, UserLikeRepository::class);
        App::bind(UserFollowRepositoryInterface::class, UserFollowRepository::class);
        App::bind(FilesRepositoryInterface::class, FilesRepository::class);
        App::bind(TryFilesRepositoryInterface::class, TryFilesRepository::class);
        App::bind(UserTryRepositoryInterface::class, UserTryRepository::class);
        App::bind(ReviewFilesRepositoryInterface::class, ReviewFilesRepository::class);
        App::bind(SystemNotificationRepositoryInterface::class, SystemNotificationRepository::class);
        App::bind(NotificationRepositoryInterface::class, NotificationRepository::class);
        App::bind(CodeRepositoryInterface::class, CodeRepository::class);
        App::bind(TryExtRepositoryInterface::class, TryExtRepository::class);
        App::bind(ReviewLikeRepositoryInterface::class, ReviewLikeRepository::class);
        App::bind(MainContRepositoryInterface::class, MainContRepository::class);
        App::bind(UserPointRepositoryInterface::class, UserPointRepository::class);
        App::bind(ReviewHashTagsRepositoryInterface::class, ReviewHashTagsRepository::class);
        App::bind(TipsRepositoryInterface::class, TipsRepository::class);
        App::bind(TipViewsRepositoryInterface::class, TipViewsRepository::class);
        App::bind(TipsMappingRepositoryInterface::class, TipsMappingRepository::class);
    }
}
