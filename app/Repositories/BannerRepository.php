<?php

namespace App\Repositories;

use App\Contracts\BannerRepositoryInterface;
use App\Models\Banner;

/**
 * Class BannerRepository.
 *
 */
class BannerRepository extends Repository implements BannerRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Banner::class;
    }
}
