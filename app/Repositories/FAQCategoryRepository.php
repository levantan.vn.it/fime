<?php

namespace App\Repositories;

use App\Contracts\FAQCategoryRepositoryInterface;
use App\Models\FAQCategory;

/**
 * Class FAQRepository.
 *
 */
class FAQCategoryRepository extends Repository implements FAQCategoryRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return FAQCategory::class;
    }
}
