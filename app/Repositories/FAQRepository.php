<?php

namespace App\Repositories;

use App\Contracts\FAQRepositoryInterface;
use App\Models\FAQ;

/**
 * Class FAQRepository.
 *
 */
class FAQRepository extends Repository implements FAQRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return FAQ::class;
    }
}
