<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/31/2019
 * Time: 3:07 PM
 */

namespace App\Repositories;


use App\Contracts\FilesRepositoryInterface;
use App\Models\Files;

class FilesRepository extends Repository implements FilesRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Files::class;
    }
}
