<?php

namespace App\Repositories;

use App\Contracts\MainContRepositoryInterface;
use App\Models\MainCont;

/**
 * Class MainContRepository.
 *
 */
class MainContRepository extends Repository implements MainContRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return MainCont::class;
    }
}
