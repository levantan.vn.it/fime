<?php

namespace App\Repositories;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class Repository.
 *
 */
abstract class Repository  extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes) {
        if($this->model->usesTimestamps() && $this->model->enableCreated == true) {
            $user_id = \Auth::id();
            $attributes['created_by'] = $user_id;
        }
        return parent::create($attributes);
    }

    public function count($wheres)
    {
        $this->applyCriteria();
        $this->applyScope();

        $this->applyConditions($wheres);

        $result = $this->model->count();

        $this->resetModel();

        return $result;
    }
}
