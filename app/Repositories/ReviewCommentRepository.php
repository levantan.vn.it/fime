<?php

namespace App\Repositories;

use App\Contracts\ReviewCommentRepositoryInterface;
use App\Models\ReviewComment;

/**
 * Class ReviewCommentRepository.
 *
 */
class ReviewCommentRepository extends Repository implements ReviewCommentRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return ReviewComment::class;
    }
}
