<?php

namespace App\Repositories;

use App\Contracts\ReviewFilesRepositoryInterface;
use App\Models\ReviewFiles;

class ReviewFilesRepository extends Repository implements ReviewFilesRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ReviewFiles::class;
    }
}
