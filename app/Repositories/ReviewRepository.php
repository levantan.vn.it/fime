<?php

namespace App\Repositories;

use App\Contracts\ReviewRepositoryInterface;
use App\Models\Review;

/**
 * Class ReviewRepository.
 *
 */
class ReviewRepository extends Repository implements ReviewRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Review::class;
    }
}
