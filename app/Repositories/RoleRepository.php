<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 1/22/2019
 * Time: 3:22 PM
 */

namespace App\Repositories;


use App\Contracts\RoleRepositoryInterface;
use App\Models\Role;

class RoleRepository extends Repository implements RoleRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Role::class;
    }
}