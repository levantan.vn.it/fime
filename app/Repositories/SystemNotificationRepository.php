<?php

namespace App\Repositories;

use App\Contracts\SystemNotificationRepositoryInterface;
use App\Models\SystemNotification;

/**
 * Class BannerRepository.
 *
 */
class SystemNotificationRepository extends Repository implements SystemNotificationRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return SystemNotification::class;
    }
}
