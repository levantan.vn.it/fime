<?php
/**
 * Created by PhpStorm.
 * User: hung.dao
 * Date: 4/2/2019
 * Time: 4:34 PM
 */

namespace App\Repositories;


use App\Contracts\TipsRepositoryInterface;
use App\Models\Tips;

class TipsRepository extends Repository implements TipsRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
        return Tips::class;
    }
}
