<?php

namespace App\Repositories;

use App\Contracts\TryExtRepositoryInterface;
use App\Models\TryExt;

/**
 * Class TryExtRepository.
 *
 */
class TryExtRepository extends Repository implements TryExtRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return TryExt::class;
    }
}
