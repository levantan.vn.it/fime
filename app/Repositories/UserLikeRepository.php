<?php

namespace App\Repositories;

use App\Contracts\UserLikeRepositoryInterface;
use App\Models\UserLike;

/**
 * Class UserLikeRepository.
 *
 */
class UserLikeRepository extends Repository implements UserLikeRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserLike::class;
    }
}
