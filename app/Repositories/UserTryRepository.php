<?php

namespace App\Repositories;

use App\Contracts\UserTryRepositoryInterface;
use App\Models\UserTry;

/**
 * Class UserTryRepository.
 *
 */
class UserTryRepository extends Repository implements UserTryRepositoryInterface
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return UserTry::class;
    }
}
