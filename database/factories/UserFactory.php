<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'slug' => $faker->slug,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make(123456),
        'recent_selected_at' => now(),
        'remember_token' => ''
    ];
});
