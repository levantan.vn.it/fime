<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTryEventToTctGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('TCT_GOODS', 'is_try_event')) {

            Schema::table('TCT_GOODS', function (Blueprint $table) {
                $table->tinyInteger('is_try_event')->default(0);
                $table->string('try_event_type')->nullable();
                $table->integer('quantity_to_qualify')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TCT_GOODS', function(Blueprint $table) {
            $table->dropColumn('is_try_event');
            $table->dropColumn('try_event_type');
            $table->dropColumn('quantity_to_qualify');
        });
    }
}
