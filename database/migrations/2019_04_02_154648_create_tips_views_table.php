<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipsViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tip_views')) {

            Schema::create('tip_views', function (Blueprint $table) {
                $table->integer('ti_id')->primary();;
                $table->unsignedInteger('views');
                $table->foreign('ti_id')
                    ->references('ti_id')
                    ->on('TCT_TIPS')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tip_views');
    }
}
