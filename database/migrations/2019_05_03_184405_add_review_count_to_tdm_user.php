<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReviewCountToTdmUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('TDM_USER', 'reviews')) {
            Schema::table('TDM_USER', function (Blueprint $table) {
                $table->integer('reviews')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TDM_USER', function($table) {
            $table->dropColumn('reviews');
        });
    }
}
