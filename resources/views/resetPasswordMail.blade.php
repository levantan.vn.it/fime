<!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Fime</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 20px 0; font-family: 'Helvetica', sans-serif; font-weight: 400; background: #eeeeee;">
<table align='center' border='0' cellpadding='0' cellspacing='0' width='600'
       style="background: #ffffff; padding: 30px; margin-top: 20px;">
    <tr>
        <td style="padding: 30px 0;">
            <img src="https://api-np.fime.vn/data/images/site/logos/logo.png" alt="fime" width="144" height="33"
                 style="display: block; margin: 0 auto;">
        </td>
    </tr>
    <tr>
        <td>
            <h1 style="font-style: bold; font-size: 16px; margin: 0; line-height: 1.5;">Đặt lại mật khẩu</h1>
            <p style="font-weight: 400; font-size: 14px; margin: 0; line-height: 1.5;">Chào bạn!</p>
            <p style="font-weight: 400; font-size: 14px; margin: 0; line-height: 1.5;">
                Dường như bạn đã quên mật khẩu tài khoản Fime. Chúng tôi rất tiếc vì điều đó.
            </p>
            <p style="font-weight: 400; font-size: 14px; margin: 0; line-height: 1.5;">
                Nhưng đừng lo lắng! Bạn có thể sử dụng đường dẫn bên dưới để đặt lại mật khẩu cho tài khoản của mình.
            </p>
        </td>
    </tr>
    <tr>
        <td style="
            padding: 25px;
            text-align: center;">
            <a href="{{$url}}" style="
                padding: 10px 20px;
                background-color: #4267b2;
                text-decoration: none;
                font-size: 14px;
                color: #ffffff;
                ">Đặt lại mật khẩu</a>
        </td>
    </tr>
    <tr>
        <td>
            <p style="font-size: 14px; margin: 0; line-height: 1.5;">
                Nếu bạn không sử dụng đường dẫn này, nó sẽ hết hạn trong vòng 3 giờ. Để nhận lại đường dẫn mới vui lòng
                ghé thăm
                <a href="{{$forgot_password_url}}" style="
                        text-decoration: none;
                        font-weight: 500;
                        color: #0099ff;
                        display: inline-block;
                        ">Fime</a>.
            </p>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px;">
            <p style="font-style: italic; font-size: 14px; margin: 0; line-height: 1.5;">Trân trọng,</p>
            <p style="font-style: italic; font-size: 14px; margin: 0; line-height: 1.5;">Đội ngũ Fime</p>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 20px; text-align: center; padding-bottom: 20px; border-bottom: 1px solid #dddddd;">
            <a href="https://www.facebook.com/Fime.vn/" style="margin-right: 15px;"><img
                        src="https://api-np.fime.vn/data/images/site/logos/facebook.png" alt="facebook" width="24"
                        height="24"></a>
            <a href="http://youtube.com/" style="margin-right: 15px;"><img
                        src="https://api-np.fime.vn/data/images/site/logos/youtube.png" alt="youtube" width="24"
                        height="24"></a>
            <a href="https://www.instagram.com/fime.vn/" style="margin-right: 15px;"><img
                        src="https://api-np.fime.vn/data/images/site/logos/instagram.png" alt="instagram" width="24"
                        height="24"></a>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; padding-top: 20px;">
            <p style="font-size: 12px; margin: 0; line-height: 1.5;">
                Công ty | TNHH DM&C
            </p>
            <p style="font-size: 12px; margin: 0; line-height: 1.5;">
                Mã số thuế | 0314724148
            </p>
            <p style="font-size: 12px; margin: 0; line-height: 1.5;">
                Địa chỉ | Tầng 10, 194 Golden Building, 473 Điện Biên Phủ, Phường 25, Quận Bình Thạnh. Tp.HCM
            </p>
            <p style="font-size: 12px; margin: 0; line-height: 1.5;">
                Email | fime.info@dmnc.vn
            </p>
            <p style="font-size: 12px; margin: 0; line-height: 1.5;">
                SĐT | 028.6258.3681
            </p>
        </td>
    </tr>
</table>
</body>

</html>