<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('loginByFacebook', 'Auth\AuthController@loginByFacebook');
    Route::post('logout', 'Auth\AuthController@logout');
    Route::post('register', 'Auth\AuthController@register');
    Route::post('resetPassword', 'Auth\AuthController@resetPassword');
    Route::post('forgotPassword', 'Auth\AuthController@forgotPassword');
    Route::post('getUserProfile', function () {
        return auth()->user();
    });
});

/*BANNER API*/
Route::post('uploads', 'UploadController@store');

/*End BANNER API*/

/*TRY API*/
Route::get('tries/getAvailableTries', 'TryController@getAvailableTries');
Route::get('tries/slug', 'TryController@getBySlug');
Route::get('tries/all', 'TryController@getAllTries');
Route::get('tries/write-review/{id}', 'TryController@getBy');

Route::get('user-tries/getShippings', 'UserTryController@getShippings');
Route::get('user-tries/getShippingStatuses', 'UserTryController@getShippingStatuses');
Route::post('user-tries', 'UserTryController@apply');
Route::post('user-tries/updateDeliveryInfo', 'UserTryController@updateDeliveryInfo');
/*End TRY API*/

/*BANNER API*/
Route::get('banners', 'BannerController@index');
Route::get('banners/get', 'BannerController@get');
Route::get('banners/all', 'BannerController@getAll');

/*End BANNER API*/


/*BLOG API*/
Route::get('blogs', 'BlogController@index');
Route::get('blogs/getByPagination', 'BlogController@getByPagination');
Route::get('blogs/available', 'BlogController@available');
Route::get('blogs/latest', 'BlogController@latest');
Route::get('blogs/top-view', 'BlogController@getTopView');
Route::get('blogs/getBySlug', 'BlogController@getBySlug');
Route::get('blogs/{id}', 'BlogController@getBy');
Route::post('blogs', 'BlogController@store');
Route::post('blogs/deleteMulti', 'BlogController@deleteMulti');
Route::put('blogs/toggle', 'BlogController@toggle');
Route::put('blogs/{id}', 'BlogController@update');

/*End BLOG API*/

/*End Review Category API*/


/*Try Category API*/

Route::get('categories', 'CategoryController@index');
Route::get('categories/{id}', 'CategoryController@getBy');
Route::post('categories', 'CategoryController@store');
Route::put('categories/{id}', 'CategoryController@update');
Route::delete('categories/{id}', 'CategoryController@delete');

/*End Try Category API*/

Route::prefix('admin')->group(function () {
    Route::get('users', 'UsersController@getUsers');
    Route::post('user/change-password', 'UserController@changePassword');
});

/* SOCIAL USER API */
Route::post('social-user/update', 'SocialUserController@updateUserID');

/* End SOCIAL USER API */

/*ADS API*/
Route::get('ads', 'AdsController@index');
Route::get('ads/available', 'AdsController@available');
Route::get('ads/{id}', 'AdsController@getBy');
Route::post('ads', 'AdsController@store');
Route::put('ads/{id}/toggle', 'AdsController@toggle');
Route::put('ads/{id}', 'AdsController@update');
Route::delete('ads/{id}', 'AdsController@delete');

/*End ADS API*/

/* Review API*/
Route::get('reviews', 'ReviewController@index');
Route::post('reviews', 'ReviewController@store');
Route::get('reviews/getNew', 'ReviewController@getNew');
Route::get('reviews/getNewAfter', 'ReviewController@getNewAfter');
Route::get('reviews/getByType/{type}', 'ReviewController@getReviewByType');
Route::get('reviews/{id}', 'ReviewController@get');
Route::get('reviews/detail/{slug}', 'ReviewController@getDetail');
Route::post('reviews/{id}', 'ReviewController@delete');
Route::put('reviews/toggle', 'ReviewController@toggle');
Route::put('reviews/togglePopular', 'ReviewController@togglePopular');
Route::put('reviews/edit', 'ReviewController@update');
Route::post('reviews/delete', 'ReviewController@delete');

/*End Review API*/


/* FAQ API*/
Route::get('faqs', 'FAQController@index');
Route::get('faqs/{code}', 'FAQController@get');
Route::get('faqs/getByCategory/{code}', 'FAQController@available');
Route::post('faqs/delete', 'FAQController@delete');
Route::put('faqs/toggle', 'FAQController@toggle');
Route::put('faqs/{id}', 'FAQController@update');
Route::post('faqs', 'FAQController@store');

/*End FAQ API*/

/* FAQ Category API*/
Route::get('faq-categories', 'FAQCategoryController@index');
Route::get('faq-categories/{id}', 'FAQCategoryController@getBy');
Route::post('faq-categories', 'FAQCategoryController@store');
Route::put('faq-categories/{id}', 'FAQCategoryController@update');
Route::delete('faq-categories/{id}', 'FAQCategoryController@delete');

/* END FAQ Category API*/

/* Comment API*/
Route::get('comments/all/{id}', 'CommentController@getCommentsOfReview');
Route::get('comments/list', 'CommentController@getListComments');
Route::post('comments/delete', 'CommentController@delete');
Route::post('comments/create', 'CommentController@store');
//Route::get('comments/{id}', 'CommentController@getById');
Route::put('comments/{id}/toggle', 'CommentController@toggle');
/* End Comment API*/


/* Setting */
Route::get('settings', 'SettingController@index');
Route::get('settings/getByGroup', 'SettingController@getByGroup');
/* End Setting */

/* Follow user */
Route::post('user-follows', 'UserFollowController@toggle');
Route::get('user-follows/topInteractive', 'UserFollowController@getTopInteractive');
/* End follow user */

/* Fimers */
Route::get('fimers/profile', 'UsersController@getUserProfile');
Route::get('fimers/get/{filterType}', 'UsersController@getFimers');
Route::get('fimers/getHot/{type}', 'UsersController@getHotFimers');
/* End Fimers */

/* Like action */
Route::post('user-likes', 'UserLikeController@toggle');
Route::get('user-likes/get-list', 'UserLikeController@getList');
/* End like action */

/* Like action */
Route::get('hashtags', 'HashtagController@getTopHashtags');
/* End like action */

/* My page */
Route::get('usr/{slug}', 'MyPageController@get');
Route::get('usr/get/{user_no}', 'UserController@get');
Route::get('usr-info/reviews', 'MyPageController@getReviews');
Route::get('usr-info/likes', 'MyPageController@getLikeReviews');
Route::get('usr-info/followers', 'MyPageController@getFollowers');
Route::get('usr-info/followings', 'MyPageController@getFollowings');
Route::get('usr-info/tries', 'MyPageController@getTries');
Route::post('usr-info/follow', 'MyPageController@follow');
Route::post('my-page/edit', 'MyPageController@edit');
Route::post('my-page/linkFacebook', 'MyPageController@linkFacebook');
/* End my page */

/* Search page */
Route::get('search/tries', 'SearchPageController@getTries');
Route::get('search/fimers', 'SearchPageController@getFimers');
/* End search page */

/* System notification API*/
Route::post('system-notification', 'SystemNotificationController@store');
Route::get('system-notification', 'SystemNotificationController@index');
Route::put('system-notification/toggle', 'SystemNotificationController@toggle');
Route::put('system-notification/update', 'SystemNotificationController@update');
Route::post('system-notification/delete', 'SystemNotificationController@delete');
Route::get('system-notification/{id}', 'SystemNotificationController@getById');
/* End System notification API*/

/* System notification API*/
Route::get('notifications', 'NotificationController@getByUserId');
Route::get('notifications/number', 'NotificationController@getNotificationNumber');
Route::post('notifications/mark-as-seen', 'NotificationController@markNotificationAsSeen');
Route::put('notifications/mark-all-as-seen', 'NotificationController@markAllAsSeen');
/* End System notification API*/


/* Tips API */
Route::get('tips', 'TipController@getAll');
Route::get('tip/{id}', 'TipController@getByID');
Route::get('tips/top-view', 'TipController@getTopView');
Route::get('text-colors', 'TextColorController@index');
/* End Tips API */


Route::group(['middleware' => ['role:admin']], function () {
    Route::get('banners/{id}', 'BannerController@getBy');
    Route::post('banners', 'BannerController@store');
    Route::post('banners/deleteMulti', 'BannerController@deleteMulti');
    Route::put('banners/toggle', 'BannerController@toggle');
    Route::put('banners/{id}', 'BannerController@update');


    Route::get('tries', 'TryController@index');
    Route::get('tries/getByPagination', 'TryController@getByPagination');
    Route::post('tries', 'TryController@store');
    Route::post('tries/deleteMulti', 'TryController@deleteMulti');
    Route::put('tries/toggle', 'TryController@toggle');
    Route::put('tries/{id}', 'TryController@update');
    Route::get('tries/{id}', 'TryController@getBy');


    Route::get('user-tries', 'UserTryController@getAll');
    Route::post('user-tries/toggle', 'UserTryController@toggle');


    Route::prefix('admin')->group(function () {
        Route::prefix('users')->group(function () {
            Route::post('get-list', 'UsersController@getList');
            Route::get('get-list', 'UsersController@getList');
        });

        Route::prefix('user')->group(function () {
            Route::get('searchByName', 'UserController@searchByName');
            Route::get('get/{user_no}', 'UserController@get');
            Route::delete('{user_no}', 'UserController@delete');
            Route::post('getByIds', 'UserController@getByIds');
            Route::post('{user_no}/updateStatus', 'UserController@updateStatus');
            Route::post('{user_no}/update', 'UserController@update');
            Route::post('/add', 'UserController@store');
        });

        Route::prefix('roles')->group(function () {
            Route::get('get-list', 'RoleController@getList');
        });

        Route::prefix('export')->group(function () {
            Route::get('tries', 'Report\TriesReportController@export');
            Route::get('winner', 'Report\WinnerReportController@export');
        });
        Route::get('points', 'PointController@index');
        Route::put('points', 'PointController@update');
        Route::get('text-colors', 'TextColorController@index');
        Route::put('text-colors', 'TextColorController@update');
        Route::post('text-colors', 'TextColorController@store');
    });


    /*BRAND API*/
    Route::get('brands', 'BrandController@index');
    Route::get('brands/{id}', 'BrandController@getBy');
    Route::post('brands', 'BrandController@store');
    Route::put('brands/{id}', 'BrandController@update');
    Route::delete('brands/{id}', 'BrandController@delete');

    /*End BRAND API*/

    Route::put('settings', 'SettingController@update');


});
